import Foundation
import UIKit

public enum DependencyContextError: LocalizedError {

    /// Cannot register, key already taken
    case keyAlreadyTaken(key: String)

    /// No service found
    case keyNotFound(key: String)

    /// Key found but the provider was unresolvable
    case incompatibleInjectionProvider
}

extension DependencyContextError {
    public var errorDescription: String? {
        switch self {
        case .incompatibleInjectionProvider: return "DependencyContextError: Incompatible injection provider"
        case .keyNotFound(let key): return "DependencyContextError: Key not found: \(key)"
        case .keyAlreadyTaken(let key): return "DependencyContextError: Key already taken: \(key)"
        }
    }
}

public class DependencyContext {
    private static var shared = DependencyContext()
    private var registeredProviders: [String: Any] = [:]
    internal static let removeNotification = Notification.Name.init("dependencyContext.onRemoveNotification")
    internal static let setNotification = Notification.Name.init("dependencyContext.onSetNotification")

    static func provide<T, Provider>(_ keyPath: WritableKeyPath<DependencyContext, Provider>) throws -> T where Provider: InjectionProvider, Provider.T == T {
        try shared[keyPath: keyPath].provide()
    }

    public static subscript<T, Provider>(_ keyPath: WritableKeyPath<DependencyContext, Provider>) -> Provider where Provider: InjectionProvider, Provider.T == T {
        get { shared[keyPath: keyPath] }
        set {
            shared[keyPath: keyPath] = newValue
            NotificationCenter.default.post(setNotification(for: keyPath))
        }
    }

    public static func register<ProviderType, ValueType>(_ provider: ProviderType) throws where ProviderType: InjectionProvider, ProviderType.T == ValueType {
        let typeHash = String(describing: ValueType.self)
        try shared.register(key: typeHash, value: provider)
        NotificationCenter.default.post(setNotification(for: typeHash))
    }

    public static func register<ProviderType, ValueType>(_ provider: ProviderType, keyIdentifier: String) throws where ProviderType: InjectionProvider, ProviderType.T == ValueType {
        try shared.register(key: keyIdentifier, value: provider)
        NotificationCenter.default.post(setNotification(for: keyIdentifier))
    }

    public static func remove(for keyIdentifier: String) {
        _ = shared.registeredProviders.removeValue(forKey: keyIdentifier)
        NotificationCenter.default.post(removalNotification(for: keyIdentifier))
    }

    public static func remove<T>(for type: T.Type) {
        _ = shared.registeredProviders.removeValue(forKey: key(for: T.self))
        NotificationCenter.default.post(removalNotification(for: key(for: T.self)))
    }

    public static func removeAllDynamic() {
        shared.registeredProviders.removeAll()
    }

    static func provide<ValueType>(for keyIdentifier: String? = nil) throws -> ValueType {
        let key = keyIdentifier ?? key(for: ValueType.self)
        return try shared.retrieve(key: key)
    }

    static func provideAll<ValueType>() -> [ValueType] {
        retrieveProviders()
            .compactMap { (provider: BaseInjectionProvider<ValueType>) -> ValueType? in
                let providedValue = try? provider.provide() // Ignore errors? Bad, but to abort whole operation because of one error is not good neither...
                return providedValue
            }
    }

    static func retrieveProviders<ValueType>() -> [BaseInjectionProvider<ValueType>] {
        shared.registeredProviders
            .compactMap { $0.value as? SharedProvider<ValueType> } // Take only singletons.
    }

    static func retrieveProvider<ValueType>() throws -> BaseInjectionProvider<ValueType> {
        try retrieveProvider(key: Self.key(for: ValueType.self))
    }

    static func retrieveProvider<ValueType>(key: String) throws -> BaseInjectionProvider<ValueType> {
        guard let anyRegisteredObject = shared.registeredProviders[key] else {
            throw DependencyContextError.keyNotFound(key: key)
        }

        guard let provider = anyRegisteredObject as? BaseInjectionProvider<ValueType> else {
            throw DependencyContextError.incompatibleInjectionProvider
        }
        return provider
    }

    // MARK: - Private instance helpers
    private func retrieve<ValueType>(key: String) throws -> ValueType {
        return try Self.retrieveProvider(key: key).provide()
    }

    private func register<ProviderType>(key: String, value: ProviderType) throws where ProviderType: InjectionProvider {
        if registeredProviders[key] != nil {
            throw DependencyContextError.keyAlreadyTaken(key: key)
        }
        registeredProviders[key] = value
    }

    static internal func key<T>(for: T.Type) -> String {
        String(describing: T.self)
    }

    static private func removalNotification(for key: Any) -> Notification {
        Notification(name: removeNotification, object: nil, userInfo: ["key": key])
    }

    static private func setNotification(for key: Any) -> Notification {
        Notification(name: setNotification, object: nil, userInfo: ["key": key])
    }
}
