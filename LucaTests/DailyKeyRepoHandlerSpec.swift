import Quick
import Nimble
import Cuckoo
import Foundation
import RxNimble
import RxSwift
import RxBlocking
import DependencyInjection

@testable import Luca_Auto_Tests

class DailyKeyRepoHandlerSpec: QuickSpec {
    // swiftlint:disable:next function_body_length
    override func spec() {
        describe("the DailyKeyRepoHandler") {
            var keyValueRepoMock: MockKeyValueRepoProtocol!
            var repoHandler: MockDailyKeyRepoHandler!
            let dailyKeyRepository = MockDailyPubKeyHistoryRepository()
            let backendAddressV3 = BackendAddressV3()
            let matcher = anyClosure() as ParameterMatcher<(Date) -> Void>
            let matcherEmpty = anyClosure() as ParameterMatcher<() -> Void>
            let matcherLocalizedTitledError = anyClosure() as ParameterMatcher<(LocalizedTitledError) -> Void>
            let matcherDate = any() as ParameterMatcher<Date>
            let matcherPublicKeyFetchResultV3 = any() as ParameterMatcher<PublicKeyFetchResultV3>
            let matcherDailyKeyRepoHandlerError = anyClosure() as ParameterMatcher<(DailyKeyRepoHandlerError) -> Void>
            var backendDailyKeyV3: MockDefaultBackendDailyKeyV3!
            var reachabilityService: ReachabilityService = {
                let rs = ReachabilityService()
                //        rs.enable()
                return rs
            }()

            beforeEach {
                keyValueRepoMock = MockKeyValueRepoProtocol()
                DependencyContext[\.dailyKeyRepository] = SharedProvider(value: dailyKeyRepository)
                DependencyContext[\.keyValueRepo] = SharedProvider(value: keyValueRepoMock)
                backendDailyKeyV3 = MockDefaultBackendDailyKeyV3()
                repoHandler = MockDailyKeyRepoHandler(reachabilityService: reachabilityService)
            }

            it("should report valid key age") {
                expect(DailyKeyRepoHandler.keyAgeIsValid(for: Date().timeIntervalSince1970)).to(equal(true))
                expect(DailyKeyRepoHandler.keyAgeIsValid(for: Date().timeIntervalSince1970 - TimeUnit.day(amount: 3).timeInterval)).to(equal(true))
                expect(DailyKeyRepoHandler.keyAgeIsValid(for: Date().timeIntervalSince1970 - TimeUnit.day(amount: 8).timeInterval)).to(equal(false))
            }

            context("keyAgeIsValidRx") {
                it("should report valid key age") {
                    expect(DailyKeyRepoHandler.keyAgeIsValidRx(for: Date().timeIntervalSince1970)).array.to(beEmpty())
                    expect(DailyKeyRepoHandler.keyAgeIsValidRx(for: Date().timeIntervalSince1970 - TimeUnit.day(amount: 3).timeInterval)).array.to(beEmpty())
                    expect(DailyKeyRepoHandler.keyAgeIsValidRx(for: Date().timeIntervalSince1970 - TimeUnit.day(amount: 8).timeInterval)).first.to(throwError())
                }
            }

            context("fetching new keys via Rx") {
                beforeEach {
                    stub(repoHandler) { m in
                        when(m.shouldUpdateKeysRx()).thenReturn(Single.just(true))
                        when(m.isSystemTimeGenuine()).thenReturn(Single.just(true))
                        when(m.fetchRx()).thenCallRealImplementation()
                        when(m.processFetchResult(result: any())).thenReturn(Completable.empty())
                        when(m.retrieveDailyPubKey())
                            .thenReturn(
                                Single.just(PublicKeyFetchResultV3(
                                    keyId: 0,
                                    issuerId: "",
                                    publicKey: "",
                                    createdAt: 0,
                                    signature: ""))
                            )
                    }
                }
                it("should complete if keys are still valid or is already updating") {
                    stub(repoHandler) { m in
                        when(m.shouldUpdateKeysRx()).thenReturn(Single.just(false))
                    }
                    expect(repoHandler.fetchRx()).array.to(beEmpty())
                    verify(repoHandler, never()).isSystemTimeGenuine()
                }

                it("should complete if keys are still valid or is already updating") {
                    stub(repoHandler) { m in
                        when(m.isSystemTimeGenuine()).thenReturn(Single.just(false))
                    }
                    expect(repoHandler.fetchRx()).array.to(beEmpty())
                    verify(repoHandler, never()).retrieveDailyPubKey()
                }

                it("should complete if retrieveDailyPubKey throws an error") {
                    stub(repoHandler) { m in
                        when(m.retrieveDailyPubKey()).thenReturn(Single.error(RetrieveDailyKeyError.notFound))
                    }
                    expect(repoHandler.fetchRx()).first.to(throwError())
                    verify(repoHandler, never()).processFetchResult(result: any())
                }
                it("should process the fetch result") {
                    let publicKeyFetchResultV3 = PublicKeyFetchResultV3(keyId: 0, issuerId: "", publicKey: "", createdAt: 0, signature: "")
                    stub(repoHandler) { m in
                        when(m.retrieveDailyPubKey()).thenReturn(Single.just(publicKeyFetchResultV3))
                        when(m.processFetchResult(result: any())).thenReturn(.empty())
                        when(m.updateDailyKeyRepoRx(with: any())).thenReturn(.empty())
                        when(m.emitDailyPublicKeyValidity(valid: any())).thenDoNothing()
                        when(m.verifyDailyKeyNotOutdated()).thenReturn(true)
                    }
                    stub(keyValueRepoMock) { mock in
                        when(mock)
                            .store(any(), value: matcherDate, completion: matcherEmpty, failure: matcherLocalizedTitledError)
                            .then { _, _, completion, _ in
                                completion()
                            }
                    }
                    expect(repoHandler.fetchRx()).array.to(beEmpty())
                    verify(repoHandler).processFetchResult(result: any())
                    verify(repoHandler).updateDailyKeyRepoRx(with: any())
                    verify(repoHandler).emitDailyPublicKeyValidity(valid: any())
                    verify(repoHandler).verifyDailyKeyNotOutdated()
                    verify(keyValueRepoMock).store(any(), value: matcherDate, completion: anyClosure(), failure: matcherLocalizedTitledError)
                }

            }

            context("verify daily key") {
                beforeEach {
                    stub(repoHandler) { m in
                        when(m.verifyDailyKeyNotOutdated()).thenCallRealImplementation()
                    }
                }
                it("should not be valid if there is no latest key") {
                    stub(dailyKeyRepository) { m in
                        when(m.newestId).get.thenReturn(nil)
                    }
                    expect(repoHandler.verifyDailyKeyNotOutdated()).to(equal(false))
                }
                it("should be valid if latest key is 6 days old") {
                    let date = Date().timeIntervalSince1970 - TimeUnit.day(amount: 6).timeInterval
                    stub(dailyKeyRepository) { m in
                        when(m.newestId).get.thenReturn(DailyKeyIndex(keyId: 1, createdAt: Date(timeIntervalSince1970: date)))
                    }
                    expect(repoHandler.verifyDailyKeyNotOutdated()).to(equal(true))
                }
                it("should not be valid if latest key is 8 days old") {
                    let date = Date().timeIntervalSince1970 - TimeUnit.day(amount: 8).timeInterval
                    stub(dailyKeyRepository) { m in
                        when(m.newestId).get.thenReturn(DailyKeyIndex(keyId: 1, createdAt: Date(timeIntervalSince1970: date)))
                    }
                    expect(repoHandler.verifyDailyKeyNotOutdated()).to(equal(false))
                }

            }

            context("isSystemTimeGenuine Rx") {
                beforeEach {
                    stub(repoHandler) { m in
                        when(m.isSystemTimeGenuine()).thenCallRealImplementation()
                    }
                }

                it("should throw error if timesync couln't be fetched") {
                    stub(repoHandler) { m in
                        when(m.fetchTimeSync()).thenReturn(.error(RetrieveDailyKeyError.notFound))
                    }
                    expect(repoHandler.isSystemTimeGenuine()).first.to(throwError())
                }
                it("should report result of systemTimeEqualsServerTime true") {
                    stub(repoHandler) { m in
                        when(m.fetchTimeSync()).thenReturn(.just(Timesync(unix: Int(Date().timeIntervalSince1970))))
                    }
                    expect(repoHandler.isSystemTimeGenuine()).first == true
                }
                it("should report result of systemTimeEqualsServerTime false") {
                    stub(repoHandler) { m in
                        let currentDate = Date(timeIntervalSince1970: Date().timeIntervalSince1970 - TimeUnit.hour(amount: 23).timeInterval)
                        when(m.fetchTimeSync()).thenReturn(.just(Timesync(unix: Int(currentDate.timeIntervalSince1970))))
                    }
                    expect(repoHandler.isSystemTimeGenuine()).first == false
                }
            }

            context("updateDailyKeyRepoRx") {
                let publicKeyFetchResultV3 = PublicKeyFetchResultV3(keyId: 0, issuerId: "", publicKey: "", createdAt: 0, signature: "")
                beforeEach {
                    stub(repoHandler) { m in
                        when(m.updateDailyKeyRepoRx(with: any())).thenCallRealImplementation()
                    }
                }
                it("should throw an error") {
                    let error = NSError(domain: "Invalid daily public key", code: 0, userInfo: nil)
                    stub(repoHandler) { m in
                        when(m.updateDailyKeyRepo(with: matcherPublicKeyFetchResultV3)).thenThrow(DailyKeyRepoHandlerError.keyNotSaved(error: error))
                    }
                    expect(repoHandler.updateDailyKeyRepoRx(with: publicKeyFetchResultV3)).first.to(throwError())
                }
                it("should finish successfully") {
                    stub(repoHandler) { m in
                        when(m.updateDailyKeyRepo(with: matcherPublicKeyFetchResultV3)).thenDoNothing()
                    }
                    expect(repoHandler.updateDailyKeyRepoRx(with: publicKeyFetchResultV3)).array.to(beEmpty())
                }
            }

            context("shouldUpdateKeys") {

                beforeEach {
                    stub(repoHandler) { m in
                        when(m.shouldUpdateKeysRx()).thenCallRealImplementation()
                    }
                }

                it("should error out if loading fails") {
                    stub(keyValueRepoMock) { m in
                        when(m)
                            .load(anyString(), defaultValue: any(), completion: matcher, failure: matcherLocalizedTitledError)
                            .then { _, _, _, failure in
                                failure(DailyKeyRepoHandlerError.timeOutOfSyncFailed)
                            }
                    }
                    expect(repoHandler.shouldUpdateKeysRx()).first.toNot(throwError())
                }

                it("should not update when updated within last 24 hours") {
                    let currentDate = Date(timeIntervalSince1970: Date().timeIntervalSince1970 - TimeUnit.hour(amount: 23).timeInterval)

                    stub(keyValueRepoMock) { stub in
                        when(stub.load(any(), defaultValue: any(), completion: matcher, failure: anyClosure()))
                            .then({ tuple in
                                tuple.2(currentDate)
                            })
                    }

                    expect(repoHandler.shouldUpdateKeysRx()).first == false
                }

                it("should be true if not fetching yet when updated more than 24 hours ago") {
                    let currentDate = Date(timeIntervalSince1970: Date().timeIntervalSince1970 - TimeUnit.hour(amount: 25).timeInterval)

                    stub(keyValueRepoMock) { stub in
                        when(stub.load(any(), defaultValue: any(), completion: matcher, failure: anyClosure()))
                            .then({ tuple in
                                tuple.2(currentDate)
                            })
                    }
                    expect(repoHandler.shouldUpdateKeysRx()).first == true
                }

            }

            context("process fetch result") {
                let now: Int = Int(Date().timeIntervalSince1970)
                let publicKeyFetchResultV3 = PublicKeyFetchResultV3(keyId: 123, issuerId: "", publicKey: "publicKey1234", createdAt: now, signature: "sig_123")

                beforeEach {
                    let result = IssuerKeysFetchResultV3(issuerId: "", name: "", publicHDEKP: "", publicHDSKP: "publicHDSKP_123")
                    stub(repoHandler) { m in
                        when(m.processFetchResult(result: matcherPublicKeyFetchResultV3)).thenCallRealImplementation()
                        when(m.fetchIssuerKeysRx(for: anyString())).thenReturn(.just(result))
                        when(m.verify(thatSignature: anyString(),
                                      matchesIssuerWithPublicHDSKP: anyString(),
                                      withKeyId: anyInt(),
                                      createdAt: anyInt(),
                                      publicKey: anyString())).thenReturn(true)
                    }
                }
                it("should succeed if valid and verified") {
                    expect(repoHandler.processFetchResult(result: publicKeyFetchResultV3)).array.to(beEmpty())
                    verify(repoHandler).verify(thatSignature: "sig_123",
                                               matchesIssuerWithPublicHDSKP: "publicHDSKP_123",
                                               withKeyId: 123,
                                               createdAt: now,
                                               publicKey: "publicKey1234")
                }

                it("should error out if fetching issuers key fails") {
                    stub(repoHandler) { m in
                        when(m.fetchIssuerKeysRx(for: anyString())).thenReturn(.error(RetrieveDailyKeyError.notFound))
                    }
                    expect(repoHandler.processFetchResult(result: publicKeyFetchResultV3)).first.to(throwError())
                }

                it("should should throw error if key too old") {
                    let eightDaysAgo: Int = Int(Date().timeIntervalSince1970 - TimeUnit.day(amount: 8).timeInterval)
                    let publicKeyFetchResultV3 = PublicKeyFetchResultV3(keyId: 123, issuerId: "", publicKey: "publicKey1234", createdAt: eightDaysAgo, signature: "sig_123")
                    expect(repoHandler.processFetchResult(result: publicKeyFetchResultV3)).first.to(throwError())
                }

                it("should should throw error if verification fails") {
                    stub(repoHandler) { m in
                        when(m.verify(thatSignature: anyString(),
                                      matchesIssuerWithPublicHDSKP: anyString(),
                                      withKeyId: anyInt(),
                                      createdAt: anyInt(),
                                      publicKey: anyString())).thenReturn(false)
                    }
                    expect(repoHandler.processFetchResult(result: publicKeyFetchResultV3)).first.to(throwError())
                }

            }
        }
    }
}
