import Foundation
import RxSwift
import RxCocoa
import RxBlocking

extension ObservableType {
    func materializeAndBlock() throws -> [Event<Element>]? {
        return try self
                .materialize()
                .toBlocking()
                .toArray()
    }
}

extension PrimitiveSequence where Trait == SingleTrait {

    func materializeAndBlock() throws -> [Event<Element>]? {
        return try asObservable()
                .materialize()
                .toBlocking()
                .toArray()
    }

}

extension PrimitiveSequence where Trait == MaybeTrait {
    func materializeAndBlock() throws -> [Event<Element>]? {
        return try asObservable()
            .materialize()
            .toBlocking()
            .toArray()
    }
}
