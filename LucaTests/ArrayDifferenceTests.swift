import XCTest
@testable import Luca_Auto_Tests

class ArrayDifferenceTests: XCTestCase {

    func testHashable() throws {

        let first = [0, 1, 2, 3]
        let second = [2, 3, 4, 5]

        XCTAssertEqual(
            Set(first.difference(from: second)),
            Set(second.difference(from: first))
        )
        XCTAssertEqual(
            Set(first.difference(from: second)),
            Set([0, 1, 4, 5])
        )
    }

    func testCustomChecker() {

        let first = [0, 1, 2, 3]
        let second = [2, 3, 4, 5]

        XCTAssertEqual(
            Set(first.difference(from: second, equalityChecker: { $0 == $1 })),
            Set(second.difference(from: first, equalityChecker: { $0 == $1 }))
        )

        XCTAssertEqual(
            Set(first.difference(from: second, equalityChecker: { $0 == $1 })),
            Set([0, 1, 4, 5])
        )

    }

}
