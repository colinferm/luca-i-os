import XCTest
import RxSwift
@testable import Luca_Auto_Tests
import Cuckoo
import simd
import DependencyInjection

class DailyKeyRepoHandlerTests: XCTestCase {
    var dailyKeyRepoHandler: DailyKeyRepoHandler!
    let dailyKeyRepository = DailyPubKeyHistoryRepository()
    let backendAddressV3 = BackendAddressV3()
    let keyValueRepo: RealmKeyValueRepo = RealmKeyValueRepo(key: nil, filenameSalt: "asdf")
    lazy var backendDailyKeyV3: DefaultBackendDailyKeyV3 = {
        return DefaultBackendDailyKeyV3()
    }()
    var keyValueRepoMock: MockKeyValueRepoProtocol!
    var reachabilityService: ReachabilityService = {
        let rs = ReachabilityService()
//        rs.enable()
        return rs
    }()

    override func setUpWithError() throws {
        DependencyContext[\.keyValueRepo] = SharedProvider(value: keyValueRepo)
        dailyKeyRepoHandler = DailyKeyRepoHandler(reachabilityService: reachabilityService)
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testKeyAgeIsValid() throws {
        XCTAssertTrue(DailyKeyRepoHandler.keyAgeIsValid(for: Date().timeIntervalSince1970))
        XCTAssertTrue(DailyKeyRepoHandler.keyAgeIsValid(for: Date().timeIntervalSince1970 - TimeUnit.day(amount: 3).timeInterval))
        XCTAssertFalse(DailyKeyRepoHandler.keyAgeIsValid(for: Date().timeIntervalSince1970 - TimeUnit.day(amount: 8).timeInterval))
    }

//    func testVerifyDailyKeyNotOutdated() throws {
//        var date = Date().timeIntervalSince1970 - TimeUnit.day(amount: 6).timeInterval
//        dailyKeyRepository.resultKey = DailyKeyIndex(keyId: 1, createdAt: Date(timeIntervalSince1970: date))
//        XCTAssertTrue(dailyKeyRepoHandler.verifyDailyKeyNotOutdated())
//
//        date = Date().timeIntervalSince1970 - TimeUnit.day(amount: 8).timeInterval
//        dailyKeyRepository.resultKey = DailyKeyIndex(keyId: 1, createdAt: Date(timeIntervalSince1970: date))
//        XCTAssertFalse(dailyKeyRepoHandler.verifyDailyKeyNotOutdated())
//
//        dailyKeyRepository.resultKey = nil
//        XCTAssertFalse(dailyKeyRepoHandler.verifyDailyKeyNotOutdated())
//    }

    func testSystemTimeCheck() throws {
        XCTAssertTrue(DailyKeyRepoHandler.systemTimeEqualsServerTime(servertime: Timesync(unix: Int(Date().timeIntervalSince1970))))
        let SixMinutesAgo = Date(timeIntervalSince1970: Date().timeIntervalSince1970 - TimeUnit.minute(amount: 6).timeInterval)
        XCTAssertFalse(DailyKeyRepoHandler.systemTimeEqualsServerTime(servertime: Timesync(unix: Int(SixMinutesAgo.timeIntervalSince1970))))
        let FourMinutesAgo = Date(timeIntervalSince1970: Date().timeIntervalSince1970 - TimeUnit.minute(amount: 4).timeInterval)
        XCTAssertTrue(DailyKeyRepoHandler.systemTimeEqualsServerTime(servertime: Timesync(unix: Int(FourMinutesAgo.timeIntervalSince1970))))
        let SixMinutesInTheFuture = Date(timeIntervalSince1970: Date().timeIntervalSince1970 + TimeUnit.minute(amount: 6).timeInterval)
        XCTAssertFalse(DailyKeyRepoHandler.systemTimeEqualsServerTime(servertime: Timesync(unix: Int(SixMinutesInTheFuture.timeIntervalSince1970))))
    }
 }

 class DailyPubKeyHistoryRepositoryTest: SecKeyHistoryRepository<DailyKeyIndex>, DailyPubKeyHistoryRepositoryKind {
    var resultKey: DailyKeyIndex?

    var newestId: IndexType? {
        return resultKey
    }

    func newest(withId: Int) -> IndexType? {
        return nil
    }

 }

// class ReachabilityServiceTest: NSObject, ReachabilityServiceKind {
//    private var disposeBag: DisposeBag?
//    private var reachability: Reachability?
//    var isEnabled: Bool { disposeBag != nil }
//
//    private var isReachableRelay = BehaviorRelay<Bool>(value: false)
//    var isReachableDriver: Driver<Bool> {
//        return isReachableRelay.distinctUntilChanged().asDriver(onErrorJustReturn: false)
//    }
//
//    /// It enables only the listeners.
//    func enable() {}
//
//    /// Disables the listeners; it doesn't turn off the feature though
//    func disable() {}
// }

 func equal(to value: Date) -> ParameterMatcher<Date> {
  return ParameterMatcher { tested in
    // Implementation of equality test for your custom type.
      tested == value
  }
}
