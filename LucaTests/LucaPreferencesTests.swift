import XCTest
import DependencyInjection
import RxSwift
@testable import Luca_Auto_Tests

class LucaPreferencesTests: XCTestCase {
    var keyValueRepo: RealmKeyValueRepo!
    var preferences: LucaPreferences!

    override func setUpWithError() throws {
        keyValueRepo = RealmKeyValueRepo(key: nil, filenameSalt: "TestRepo")
        DependencyContext[\.importantDataKeyValueRepo] = SharedProvider(value: keyValueRepo)
        preferences = LucaPreferences()
    }

    override func tearDownWithError() throws {
        try keyValueRepo.removeFile().blockingWait()
    }

    var propertyTest_hasUUID: Bool {
        let uuid = preferences.blocking.get(\.uuid)
        let hasUUID = uuid != nil
        return hasUUID
    }

    func test_saveToPropertyWithDefaultValue() throws {
        try preferences.set(\.welcomePresented, value: true).blockingWait()
        let retrieved = try preferences.get(\.welcomePresented).retrieveBlocking()
        XCTAssertEqual(retrieved, true)
    }

    func test_saveToPropertyWithoutDefaultValue() throws {
        var data = UserRegistrationData()
        data.firstName = "Temp"

        try preferences.set(\.userRegistrationData, value: data).blockingWait()
        let retrieved = try preferences.get(\.userRegistrationData).retrieveBlocking()
        XCTAssertEqual(retrieved?.firstName, data.firstName)
        XCTAssertNotNil(retrieved?.firstName)
    }

    func test_saveToUserDataProperty() throws {
        try preferences.set(\.firstName, value: "Something").blockingWait()
        let retrieved = try preferences.get(\.firstName).retrieveBlocking()
        XCTAssertEqual(retrieved, "Something")
    }

    func test_allProperties() throws {
        let data = UserRegistrationData()
        data.firstName = "Something"
        let uuid = UUID(uuidString: "3d0a4bdd-bb83-4243-9eca-e3cb89303553")!
        let requests = [PhoneNumberVerificationRequest.init(challengeId: "123", date: Date(timeIntervalSince1970: 123), phoneNumber: "123", verified: true)]
        try preferences.set(\.userRegistrationData, value: data)
            .andThen(preferences.set(\.uuid, value: uuid))
            .andThen(preferences.set(\.currentOnboardingPage, value: 2))
            .andThen(preferences.set(\.onboardingComplete, value: true))
            .andThen(preferences.set(\.welcomePresented, value: true))
            .andThen(preferences.set(\.donePresented, value: true))
            .andThen(preferences.set(\.dataPrivacyPresented, value: true))
            .andThen(preferences.set(\.phoneNumberVerified, value: true))
            .andThen(preferences.set(\.termsAcceptedVersion, value: 100))
            .andThen(preferences.set(\.verificationRequests, value: requests))
            .andThen(preferences.set(\.checkoutNotificationScheduled, value: true))
            .andThen(preferences.set(\.appStoreReviewCheckoutCounter, value: 2))
            .blockingWait()

        let retrievedData = try preferences.get(\.userRegistrationData).retrieveBlocking()
        XCTAssertEqual(retrievedData?.firstName, data.firstName)
        XCTAssertNotNil(retrievedData?.firstName)
        XCTAssertEqual(try preferences.get(\.uuid).retrieveBlocking(), uuid)
        XCTAssertEqual(try preferences.get(\.currentOnboardingPage).retrieveBlocking(), 2)
        XCTAssertEqual(try preferences.get(\.onboardingComplete).retrieveBlocking(), true)
        XCTAssertEqual(try preferences.get(\.welcomePresented).retrieveBlocking(), true)
        XCTAssertEqual(try preferences.get(\.donePresented).retrieveBlocking(), true)
        XCTAssertEqual(try preferences.get(\.dataPrivacyPresented).retrieveBlocking(), true)
        XCTAssertEqual(try preferences.get(\.phoneNumberVerified).retrieveBlocking(), true)
        XCTAssertEqual(try preferences.get(\.termsAcceptedVersion).retrieveBlocking(), 100)
        XCTAssertEqual(try preferences.get(\.verificationRequests).retrieveBlocking(), requests)
        XCTAssertEqual(try preferences.get(\.checkoutNotificationScheduled).retrieveBlocking(), true)
        XCTAssertEqual(try preferences.get(\.appStoreReviewCheckoutCounter).retrieveBlocking(), 2)
    }

    func test_remove_withDefaultValue() {
        preferences.blocking.set(\.welcomePresented, value: true)
        XCTAssertTrue(preferences.blocking.get(\.welcomePresented))
        preferences.blocking.remove(\.welcomePresented)
        XCTAssertFalse(preferences.blocking.get(\.welcomePresented))
    }

    func test_remove_withoutDefaultValue() {
        let uuid = UUID(uuidString: "3d0a4bdd-bb83-4243-9eca-e3cb89303553")!
        preferences.blocking.set(\.uuid, value: uuid)
        XCTAssertEqual(preferences.blocking.get(\.uuid), uuid)
        preferences.blocking.remove(\.uuid)
        XCTAssertNil(preferences.blocking.get(\.uuid))
    }

    func test_changes_withDefaultValue() {
        var values: [Bool] = []
        var disposeBag = DisposeBag()
        preferences.currentAndChanges(\.welcomePresented)
            .do(onNext: { values.append($0) })
            .subscribe()
            .disposed(by: disposeBag)

        preferences.blocking.set(\.welcomePresented, value: true)
        preferences.blocking.set(\.welcomePresented, value: false)
        preferences.blocking.set(\.welcomePresented, value: true)
        preferences.blocking.remove(\.welcomePresented)
        XCTAssertEqual(preferences.blocking.get(\.welcomePresented), false)
        disposeBag = DisposeBag()
        XCTAssertEqual(values, [false, true, false, true, false])
    }

    func test_changes_inUserDataShortcuts() {
        var values: [String?] = []
        var disposeBag = DisposeBag()
        preferences.currentAndChanges(\.firstName)
            .do(onNext: { values.append($0) })
            .subscribe()
            .disposed(by: disposeBag)

        preferences.blocking.set(\.firstName, value: "true")
        preferences.blocking.set(\.firstName, value: "false")
        XCTAssertEqual(preferences.blocking.get(\.firstName), "false")
        disposeBag = DisposeBag()
        XCTAssertEqual(values, [nil, "true", "false"])
    }

    func test_changes_withoutDefaultValue() {
        let uuid = UUID(uuidString: "3d0a4bdd-bb83-4243-9eca-e3cb89303553")!
        var values: [UUID?] = []
        var disposeBag = DisposeBag()
        preferences.currentAndChanges(\.uuid)
            .do(onNext: { values.append($0) })
            .subscribe()
            .disposed(by: disposeBag)

        preferences.blocking.set(\.uuid, value: uuid)
        preferences.blocking.remove(\.uuid)
        disposeBag = DisposeBag()
        XCTAssertEqual(values.count, 3)
        XCTAssertEqual(values.filter { $0 == nil }.count, 2)
        XCTAssertEqual(values.filter { $0 != nil }.count, 1)
    }

    func test_migration() throws {
        let data = UserRegistrationData()
        data.firstName = "Something"
        data.lastName = "ALksdlakjs"
        data.city = "asdas"
        OldLucaPreferences.shared.currentOnboardingPage = 123
        OldLucaPreferences.shared.userRegistrationData = data
        OldLucaPreferences.shared.uuid = UUID(uuidString: "3d0a4bdd-bb83-4243-9eca-e3cb89303553")!
        OldLucaPreferences.shared.onboardingComplete = true
        OldLucaPreferences.shared.welcomePresented = true
        OldLucaPreferences.shared.donePresented = true
        OldLucaPreferences.shared.termsAcceptedVersion = 1233
        OldLucaPreferences.shared.dataPrivacyPresented = true
        OldLucaPreferences.shared.phoneNumberVerified = true
        OldLucaPreferences.shared.autoCheckout = true
        OldLucaPreferences.shared.verificationRequests = [PhoneNumberVerificationRequest.init(challengeId: "1231", date: Date(), phoneNumber: "123", verified: true)]
        OldLucaPreferences.shared.checkoutNotificationScheduled = true
        OldLucaPreferences.shared.appStoreReviewCheckoutCounter = 123

        try preferences.migrate(from: OldLucaPreferences.shared).blockingWait()

        XCTAssertEqual(preferences.blocking.get(\.currentOnboardingPage), OldLucaPreferences.shared.currentOnboardingPage)
        XCTAssertEqual(preferences.blocking.get(\.userRegistrationData), OldLucaPreferences.shared.userRegistrationData)
        XCTAssertEqual(preferences.blocking.get(\.uuid), OldLucaPreferences.shared.uuid)
        XCTAssertEqual(preferences.blocking.get(\.onboardingComplete), OldLucaPreferences.shared.onboardingComplete)
        XCTAssertEqual(preferences.blocking.get(\.welcomePresented), OldLucaPreferences.shared.welcomePresented)
        XCTAssertEqual(preferences.blocking.get(\.donePresented), OldLucaPreferences.shared.donePresented)
        XCTAssertEqual(preferences.blocking.get(\.termsAcceptedVersion), OldLucaPreferences.shared.termsAcceptedVersion)
        XCTAssertEqual(preferences.blocking.get(\.dataPrivacyPresented), OldLucaPreferences.shared.dataPrivacyPresented)
        XCTAssertEqual(preferences.blocking.get(\.phoneNumberVerified), OldLucaPreferences.shared.phoneNumberVerified)
        XCTAssertEqual(preferences.blocking.get(\.verificationRequests), OldLucaPreferences.shared.verificationRequests)
        XCTAssertEqual(preferences.blocking.get(\.checkoutNotificationScheduled), OldLucaPreferences.shared.checkoutNotificationScheduled)
        XCTAssertEqual(preferences.blocking.get(\.appStoreReviewCheckoutCounter), OldLucaPreferences.shared.appStoreReviewCheckoutCounter)

        OldLucaPreferences.shared.appStoreReviewCheckoutCounter = -1

        try preferences.migrate(from: OldLucaPreferences.shared).blockingWait(nil)
        // Check if the migration won't be run for the second time
        XCTAssertNotEqual(OldLucaPreferences.shared.appStoreReviewCheckoutCounter, preferences.blocking.get(\.appStoreReviewCheckoutCounter))
    }

    func test_blockingInProperty() {
        preferences.blocking.set(\.uuid, value: UUID())
        XCTAssertTrue(propertyTest_hasUUID)
        XCTAssertTrue(preferences.blocking.get(\.uuid) != nil)
    }

    func test_oldPerformance() {
        let uuid = UUID()
        OldLucaPreferences.shared.uuid = uuid
        var date = Date()
        var retrieval = OldLucaPreferences.shared.uuid
        print("First retrieval duration: \(Date().timeIntervalSince1970 - date.timeIntervalSince1970)")
        date = Date()
        retrieval = OldLucaPreferences.shared.uuid
        print("Second retrieval duration: \(Date().timeIntervalSince1970 - date.timeIntervalSince1970)")

        measure {
            OldLucaPreferences.shared.uuid = uuid
            print(OldLucaPreferences.shared.uuid)
            print(OldLucaPreferences.shared.uuid)
        }
    }

    func test_newPerformance() {
        let uuid = UUID()
        OldLucaPreferences.shared.uuid = uuid
        var date = Date()
        var retrieval = self.preferences.blocking.get(\.uuid)
        print("First retrieval duration: \(Date().timeIntervalSince1970 - date.timeIntervalSince1970)")
        date = Date()
        retrieval = self.preferences.blocking.get(\.uuid)
        print("Second retrieval duration: \(Date().timeIntervalSince1970 - date.timeIntervalSince1970)")
        measure {
            self.preferences.blocking.set(\.uuid, value: uuid)
            print(self.preferences.blocking.get(\.uuid))
            print(self.preferences.blocking.get(\.uuid))
        }
    }

}
