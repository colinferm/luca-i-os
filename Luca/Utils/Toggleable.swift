protocol Toggleable {
    var isEnabled: Bool { get }
    func enable()
    func disable()
}
