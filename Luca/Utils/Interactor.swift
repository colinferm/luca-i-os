import Foundation
import RxSwift

protocol Interactor {
    func interact() -> Completable
}
