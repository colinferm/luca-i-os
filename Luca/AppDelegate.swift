import UIKit
import IQKeyboardManagerSwift
import BackgroundTasks
import RxSwift
import DependencyInjection

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    @InjectStatic(\.appServicesInitialiser) private var appServicesInitialiser
    @InjectStatic(\.documentProcessingService) private var documentProcessingService
    @InjectStatic(\.traceIdService) private var traceIdService
    @InjectStatic(\.accessTraceIdChecker) private var accessTraceIdChecker
    @InjectStatic(\.selfCheckin) private var selfCheckin
    @InjectStatic(\.notificationService) private var notificationService
    @InjectStatic(\.accessTraceIdNotifier) private var accessTraceIdNotifier
    @InjectStatic(\.baerCodeKeyService) private var baerCodeKeyService

    // swiftlint:disable:next function_body_length
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .dark
        }
        Styling.applyStyling()

        initialiseServices()

        // It enables intelligent text field behavior when the keyboard is covering the text field.
        IQKeyboardManager.shared.enable = true

        if #available(iOS 13, *) {
            BGTaskScheduler.shared.register(forTaskWithIdentifier: "de.culture4life.matchTraces",
                                            using: nil) { (task) in
                guard let appRefreshTask = task as? BGAppRefreshTask else {
                    fatalError("Expected task to be of type BGAppRefreshTask")
                }
                self.handleAppRefresh(task: appRefreshTask)
            }
            BGTaskScheduler.shared.cancelAllTaskRequests()
        } else {
            // Fetch data once an hour
            UIApplication.shared.setMinimumBackgroundFetchInterval(3600)
        }

        #if DEBUG
        // Test BGTask using https://developer.apple.com/documentation/backgroundtasks/starting_and_terminating_tasks_during_development
        DispatchQueue.main.async { NotificationScheduler.shared.scheduleNotification(title: "App delegate", message: "for iOS 13+") }
        #endif

        _ = notificationService.removePendingNotificationsIfNotCheckedIn().subscribe()

        return true
    }

    // Background fetch for iOS 12 and under
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {

        initialiseServices()

        _ = traceIdService.fetchOldTraces().subscribe()
        _ = traceIdService.fetchTraceStatus().subscribe()
        accessTraceIdNotifier.sendNotificationOnMatch(completionHandler: completionHandler)
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        if #available(iOS 13, *) {
            scheduleAppRefresh()
        }
    }

    @available(iOS 13.0, *)
    func handleAppRefresh(task: BGAppRefreshTask) {
        scheduleAppRefresh()

        task.expirationHandler = {
            self.accessTraceIdNotifier.disposeNotificationOnMatch()
            task.setTaskCompleted(success: false)
        }

        initialiseServices()

        _ = traceIdService.fetchTraceStatus().subscribe()
        _ = traceIdService.fetchOldTraces().subscribe()
        accessTraceIdNotifier.sendNotificationOnMatch(task: task)
    }

    @available(iOS 13.0, *)
    // TEST in debug console: e -l objc -- (void)[[BGTaskScheduler sharedScheduler] _simulateLaunchForTaskWithIdentifier:@"de.culture4life.matchTraces"]
    func scheduleAppRefresh() {
        let request = BGAppRefreshTaskRequest(identifier: "de.culture4life.matchTraces")
        request.earliestBeginDate = Date(timeIntervalSinceNow: 3600)
        do {
            try BGTaskScheduler.shared.submit(request)
        } catch {
            log("Could not schedule app refresh: \(error.localizedDescription)")
        }
    }

    private var fetchAccessedTraceIdsDisposeBag = DisposeBag()
    func applicationWillEnterForeground(_ application: UIApplication) {

        // Pull keys when app enters foreground
        _ = baerCodeKeyService.getKeys().subscribe()
        _ = notificationService.removePendingNotificationsIfNotCheckedIn().subscribe()

        fetchAccessedTraceIdsDisposeBag = DisposeBag()
        accessTraceIdChecker
            .fetch()
            .subscribe()
            .disposed(by: fetchAccessedTraceIdsDisposeBag)

        // Fetch old traces and dispose them
        traceIdService
            .fetchOldTraces()
            .subscribe()
            .disposed(by: fetchAccessedTraceIdsDisposeBag)
    }

    func application(_ application: UIApplication,
                     continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        guard userActivity.activityType == NSUserActivityTypeBrowsingWeb,
              let incomingURL = userActivity.webpageURL else {
            return false
        }

        if let selfCheckin = CheckInURLParser.parse(url: incomingURL) {
            self.selfCheckin.add(selfCheckinPayload: selfCheckin)
            return true
        } else {
            for deeplinkPrefix in documentProcessingService.deeplinkPrefixArray {
                if incomingURL.absoluteString.hasPrefix(deeplinkPrefix) {
                    documentProcessingService
                        .deeplinkStore
                        .onNext(incomingURL.absoluteString)
                    return true
                }
            }
        }

        return false
    }

    private func initialiseServices() {
        do {
            try appServicesInitialiser.initialise()
        } catch let error {
            fatalError("Failure at initialising services!")
        }
    }

}

extension AppDelegate: UnsafeAddress, LogUtil {}
