import Foundation
import UIKit
import LucaUIComponents

internal struct Styling {
    static func applyStyling() {
        // Alerts
        UIView.appearance(whenContainedInInstancesOf: [UIAlertController.self]).tintColor = Asset.lucaBlue.color

        configureDefaultNavigationBar()
        configureEmbeddedNavigationBar()
        configureLabels()
        configureButtons()

        LucaDefaultTextField.appearance().backgroundColor = .clear
        LucaDefaultTextField.appearance().font = FontFamily.Montserrat.medium.font(size: 14)
        LucaDefaultTextField.appearance().textColor = UIColor.white
        LucaDefaultTextField.appearance().borderColor = Asset.luca747480.color

        self.styleEmbeddedChildrenViewControllers()
    }

    private static func configureLabels() {
        Luca14PtLabel.appearance().font = Styling.font(font: FontFamily.Montserrat.medium.font(size: 14))
        Luca14PtLabel.appearance().textColor = UIColor.white

        Luca14PtBlackLabel.appearance().font = Styling.font(font: FontFamily.Montserrat.medium.font(size: 14))
        Luca14PtBlackLabel.appearance().textColor = UIColor.black

        Luca14PtBoldBlackLabel.appearance().font = Styling.font(font: FontFamily.Montserrat.bold.font(size: 14))
        Luca14PtBoldBlackLabel.appearance().textColor = UIColor.black

		Luca14PtBoldLabel.appearance().font = Styling.font(font: FontFamily.Montserrat.bold.font(size: 14))
		Luca14PtBoldLabel.appearance().textColor = UIColor.white

        Luca20PtBoldLabel.appearance().font = Styling.font(font: FontFamily.Montserrat.bold.font(size: 20))
        Luca20PtBoldLabel.appearance().textColor = UIColor.white

        TANLabel.appearance().font = Styling.font(font: UIFont.monospacedDigitSystemFont(ofSize: 20, weight: .bold))
        TANLabel.appearance().textColor = UIColor.white

        Luca16PtLabel.appearance().font = Styling.font(font: FontFamily.Montserrat.medium.font(size: 16))
        Luca16PtLabel.appearance().textColor = Asset.lucaBlack87Percent.color

        Luca16PtBoldLabel.appearance().font = Styling.font(font: FontFamily.Montserrat.bold.font(size: 16))
        Luca16PtBoldLabel.appearance().textColor = UIColor.white

        Luca16PtBoldBlackLabel.appearance().font = Styling.font(font: FontFamily.Montserrat.bold.font(size: 16))
        Luca16PtBoldBlackLabel.appearance().textColor = UIColor.black

        Luca60PtLabel.appearance().font = Styling.font(font: UIFont.monospacedDigitSystemFont(ofSize: 60, weight: .semibold))
        Luca60PtLabel.appearance().textColor = Asset.lucaBlack.color

        Luca12PtLabel.appearance().font = Styling.font(font: FontFamily.Montserrat.medium.font(size: 12))
        Luca12PtLabel.appearance().textColor = Asset.lucaBlack.color

        // NewPrivateMeetingViewController

        Luca14PtLabel.appearance(whenContainedInInstancesOf: [ActiveCheckinNavigationController.self]).textColor = Asset.lucaBlack87Percent.color
        Luca14PtBoldLabel.appearance(whenContainedInInstancesOf: [ActiveCheckinNavigationController.self]).textColor = Asset.lucaBlack.color

        Luca16PtBoldLabel.appearance(whenContainedInInstancesOf: [NewPrivateMeetingViewController.self, ActiveCheckinNavigationController.self]).textColor = Asset.lucaBlack.color

        PrivateMeetingTimerLabel.appearance(whenContainedInInstancesOf: [NewPrivateMeetingViewController.self]).font = Styling.font(font: FontFamily.Montserrat.bold.font(size: 16))

        PrivateMeetingTimerLabel.appearance(whenContainedInInstancesOf: [NewPrivateMeetingDetailsViewController.self]).font = Styling.font(font: FontFamily.Montserrat.regular.font(size: 16))

        PrivateMeetingTimerLabel.appearance(whenContainedInInstancesOf: [ActiveCheckinNavigationController.self]).textColor = Asset.lucaBlack.color

        // DocumentListViewController

        Luca16PtLabel.appearance(whenContainedInInstancesOf: [DocumentListViewController.self]).textColor = UIColor.white
    }

    private static func configureButtons() {
        DarkStandardButton.appearance().titleLabelFont = Styling.font(font: FontFamily.Montserrat.bold.font(size: 14))
        DarkStandardButton.appearance().titleLabelColor = UIColor.white
        DarkStandardButton.appearance().backgroundColor = UIColor.clear
        DarkStandardButton.appearance().borderColor = UIColor.white

        LightStandardButton.appearance().titleLabelFont = Styling.font(font: FontFamily.Montserrat.bold.font(size: 14))
        LightStandardButton.appearance().titleLabelColor = UIColor.black
        LightStandardButton.appearance().backgroundColor = Asset.lucaBlue.color

        WhiteButton.appearance().titleLabelColor = UIColor.black
        WhiteButton.appearance().backgroundColor = .white

        BlueButton.appearance().titleLabelFont = Styling.font(font: FontFamily.Montserrat.bold.font(size: 14))
        BlueButton.appearance().titleLabelColor = Asset.lucaBlue.color
    }

    private static func configureDefaultNavigationBar() {
        let attrsTitle = [
            NSAttributedString.Key.font: FontFamily.Montserrat.bold.font(size: 20),
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]

        let attrsButton = [
            NSAttributedString.Key.font: FontFamily.Montserrat.bold.font(size: 14),
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]

        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor.black
            appearance.titleTextAttributes = attrsTitle
            appearance.buttonAppearance.normal.titleTextAttributes = attrsButton
            appearance.buttonAppearance.highlighted.titleTextAttributes = attrsButton

            UINavigationBar.appearance().standardAppearance = appearance
            UINavigationBar.appearance().scrollEdgeAppearance = appearance
            UINavigationBar.appearance().tintColor = UIColor.white
        } else {
            UINavigationBar.appearance().titleTextAttributes = attrsTitle
            UINavigationBar.appearance().barTintColor = UIColor.black
            UINavigationBar.appearance().tintColor = UIColor.white
            UINavigationBar.appearance().isTranslucent = false
            UINavigationBar.appearance().shadowImage = UIImage()

            UIBarButtonItem.appearance(whenContainedInInstancesOf: [UINavigationBar.self]).setTitleTextAttributes(attrsButton, for: .normal)
            UIBarButtonItem.appearance(whenContainedInInstancesOf: [UINavigationBar.self]).setTitleTextAttributes(attrsButton, for: .highlighted)
        }
    }

    private static func configureEmbeddedNavigationBar() {
        let attrsTitleEmbedded = [
            NSAttributedString.Key.font: FontFamily.Montserrat.bold.font(size: 20),
            NSAttributedString.Key.foregroundColor: UIColor.black]

        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = Asset.lucaCheckinGradientBottom.color
            appearance.titleTextAttributes = attrsTitleEmbedded
            appearance.shadowImage = UIImage()
            appearance.shadowColor = .clear

            UINavigationBar.appearance(whenContainedInInstancesOf: [ ActiveCheckinNavigationController.self]).standardAppearance = appearance
            UINavigationBar.appearance(whenContainedInInstancesOf: [ ActiveCheckinNavigationController.self]).scrollEdgeAppearance = appearance
            UINavigationBar.appearance(whenContainedInInstancesOf: [ ActiveCheckinNavigationController.self]).tintColor = UIColor.black
        } else {
            UINavigationBar.appearance(whenContainedInInstancesOf: [ ActiveCheckinNavigationController.self]).titleTextAttributes = attrsTitleEmbedded
            UINavigationBar.appearance(whenContainedInInstancesOf: [ ActiveCheckinNavigationController.self]).barTintColor = Asset.lucaCheckinGradientBottom.color
            UINavigationBar.appearance(whenContainedInInstancesOf: [ ActiveCheckinNavigationController.self]).tintColor = UIColor.black
            UINavigationBar.appearance(whenContainedInInstancesOf: [ ActiveCheckinNavigationController.self]).isTranslucent = false
            UINavigationBar.appearance(whenContainedInInstancesOf: [ ActiveCheckinNavigationController.self]).shadowImage = UIImage()
        }

        let attrsButtonEmbedded = [
            NSAttributedString.Key.font: FontFamily.Montserrat.bold.font(size: 14),
            NSAttributedString.Key.foregroundColor: UIColor.black
        ]
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UINavigationBar.self, ActiveCheckinNavigationController.self]).setTitleTextAttributes(attrsButtonEmbedded, for: .normal)
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UINavigationBar.self, ActiveCheckinNavigationController.self]).setTitleTextAttributes(attrsButtonEmbedded, for: .highlighted)
    }

    private static func font(font: UIFont, textStyle: UIFont.TextStyle = UIFont.TextStyle.body, maximumFontSize: CGFloat = 60) -> UIFont {
        let fontMetrics = UIFontMetrics(forTextStyle: textStyle)
        return fontMetrics.scaledFont(for: font, maximumPointSize: maximumFontSize)
    }

    private static func styleEmbeddedChildrenViewControllers() {
        let luca14ptLabel = Luca14PtLabel.appearance(whenContainedInInstancesOf: [EmbeddedChildrenListViewController.self,
                                                                                  ActiveCheckinNavigationController.self])
        luca14ptLabel.textColor = Asset.lucaBlack.color
        Luca14PtBoldLabel.appearance(whenContainedInInstancesOf: [EmbeddedChildrenListViewController.self,
                                                                  ActiveCheckinNavigationController.self]).textColor = Asset.lucaBlack.color

        Luca16PtBoldLabel.appearance(whenContainedInInstancesOf: [EmbeddedChildrenListViewController.self,
                                                                  ActiveCheckinNavigationController.self]).textColor = Asset.lucaBlack.color

        UITableView.appearance(whenContainedInInstancesOf: [EmbeddedChildrenListViewController.self]).separatorColor = .yellow
    }
}

protocol LucaModalAppearance {
    func applyColors()
}

extension LucaModalAppearance where Self: UIViewController {
    func applyColors() {
        self.view.backgroundColor = Asset.luca1d1d1d.color

        let attrsButton = [
            NSAttributedString.Key.font: FontFamily.Montserrat.bold.font(size: 14),
            NSAttributedString.Key.foregroundColor: Asset.lucaBlue.color
        ]

        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = Asset.luca1d1d1d.color
            appearance.shadowImage = UIImage()
            appearance.shadowColor = .clear
            appearance.buttonAppearance.normal.titleTextAttributes = attrsButton
            appearance.buttonAppearance.highlighted.titleTextAttributes = attrsButton

            self.navigationController?.navigationBar.standardAppearance = appearance
            self.navigationController?.navigationBar.scrollEdgeAppearance = appearance

        } else {
            self.navigationController?.navigationBar.barTintColor = Asset.luca1d1d1d.color
            self.navigationController?.navigationBar.shadowImage = UIImage()

            self.navigationItem.rightBarButtonItem?.setTitleTextAttributes(attrsButton, for: .normal)
            self.navigationItem.rightBarButtonItem?.setTitleTextAttributes(attrsButton, for: .highlighted)
            self.navigationItem.leftBarButtonItem?.setTitleTextAttributes(attrsButton, for: .normal)
            self.navigationItem.leftBarButtonItem?.setTitleTextAttributes(attrsButton, for: .highlighted)
        }
    }
}
