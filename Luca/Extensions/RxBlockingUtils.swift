import RxSwift
import Foundation

extension PrimitiveSequence where Trait == SingleTrait {

    func retrieveBlocking(_ timeout: TimeInterval? = 5.0) throws -> Element {
        let group = DispatchGroup()
        group.enter()
        var error: Error?
        var retVal: Element?
        _ = self
            .do(onSuccess: { retVal = $0; group.leave() }, onError: { error = $0; group.leave() })
            .subscribe()
        if let timeout = timeout {
            let result = group.wait(timeout: .now() + DispatchTimeInterval.microseconds(Int(timeout * 1000.0 * 1000.0)))
            if result == .timedOut {
                throw NSError.init(domain: "Subscription timed out", code: 0)
            }
        } else {
            group.wait()
        }
        if let error = error {
            throw error
        }
        // Unwrapping optional is OK here as this is single. It will throw an error anyways if it completes before emitting a value
        return retVal!
    }
}

extension Completable {
    func blockingWait(_ timeout: TimeInterval? = 5.0) throws {
        let group = DispatchGroup()
        group.enter()
        var error: Error?
        _ = self
            .do(onError: { error = $0; group.leave() }, onCompleted: { group.leave() })
            .subscribe()
        if let timeout = timeout {
            let result = group.wait(timeout: .now() + DispatchTimeInterval.microseconds(Int(timeout * 1000.0 * 1000.0)))
            if result == .timedOut {
                throw NSError.init(domain: "Subscription timed out", code: 0)
            }
        } else {
            group.wait()
        }
        if let error = error {
            throw error
        }
    }
}
