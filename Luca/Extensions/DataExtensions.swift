import Foundation

extension Data {

    /// CRC32 loaded as Int32
    var crc32: Int32 {
        let crc = self.crc32()
        return crc.withUnsafeBytes({ $0.load(as: Int32.self) })
    }

    /// Produces base64 string and replaces `+/=` occurrences with percent encoded equivalents
    var base64URLPercentageEscaped: String {
        base64EncodedString().addingPercentEncoding(withAllowedCharacters: CharacterSet(charactersIn: "+/=").inverted) ?? ""
    }
}
