import UIKit

protocol DisplaysProgress {
	var progressIndicator: UIActivityIndicatorView! {get}
	func displayLoadingIndicator()
	func hideLoadingIndicator()
}

extension DisplaysProgress where Self: UIViewController {
	func displayLoadingIndicator() {
		self.progressIndicator.isHidden = false
		self.progressIndicator.startAnimating()
	}
	func hideLoadingIndicator() {
		self.progressIndicator.stopAnimating()
		self.progressIndicator.isHidden = true
	}
}
