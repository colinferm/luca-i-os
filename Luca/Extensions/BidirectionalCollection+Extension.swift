extension BidirectionalCollection where Element: StringProtocol {
		var asSentence: String {
				count <= 2 ?
					joined(separator: L10n.List.and.string) :
					dropLast().joined(separator: ", ") + L10n.List.lastElement.string + last!
		}
}
