import UIKit
import RxSwift

class DataAccessAlertViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!

    var sectionedData: [SectionedAccessedTraceId] = []

    var allAccessesPressed: (() -> Void)?

    private let tableViewHeightConstraint: CGFloat = 100.0

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self

        tableView.reloadData()
        setupTableViewHeight()
        tableView.register(NewDataAccessHeaderView.self, forHeaderFooterViewReuseIdentifier: "NewDataAccessHeaderView")

    }

    func setupTableViewHeight() {
        let rowCount = sectionedData.map { $0.accesses.count }.reduce(0, +)
        tableViewHeight.constant = CGFloat(rowCount) * tableViewHeightConstraint +
                                   CGFloat(sectionedData.count) * tableView.sectionHeaderHeight +
                                   CGFloat(sectionedData.count) * tableView.sectionFooterHeight
    }

    @IBAction func closePressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func allDataAccessesPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: allAccessesPressed ?? nil)
    }

}
extension DataAccessAlertViewController: UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionedData.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sectionedData[section].accesses.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // swiftlint:disable:next force_cast
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewDataAccessTableViewCell", for: indexPath) as! NewDataAccessTableViewCell
        let newDataAccess = sectionedData[indexPath.section].accesses[indexPath.row]

        cell.locationLabel.text = newDataAccess.location.name
        let checkin = newDataAccess.traceInfo.checkInDate
        if let checkout = newDataAccess.traceInfo.checkOutDate {
            cell.dateLabel.text = "\(checkin.formattedDateTime) - \(checkout.formattedDateTime)"
            cell.dateLabel.accessibilityLabel = L10n.History.Checkin.Checkout.time(checkin.accessibilityDate, checkout.accessibilityDate)
        } else {
            cell.dateLabel.text = "\(checkin.formattedDateTime)"
            cell.dateLabel.accessibilityLabel = checkin.accessibilityDate
        }

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableViewHeightConstraint
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // swiftlint:disable:next force_cast
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: "NewDataAccessHeaderView") as! NewDataAccessHeaderView
        view.departmentLabel.text = sectionedData[section].healthDepartment.name
        return view
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }

}
