// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
internal enum L10n {
  /// Licenses
  internal static let acknowledgements = L10n.tr("Localizable", "Acknowledgements")

  internal enum AccessedTraceIdDetailViewController {
    /// Notification
    internal static let title = L10n.tr("Localizable", "accessedTraceIdDetailViewController.title")
  }

  internal enum AppVersion {
    /// Version Details
    internal static let button = L10n.tr("Localizable", "appVersion.button")
    internal enum Alert {
      /// You are currently using the iOS app version %@ (%@) %@\n\nThe source code can be found on GitLab using the commit hash %@
      internal static func message(_ p1: Any, _ p2: Any, _ p3: Any, _ p4: Any) -> String {
        return L10n.tr("Localizable", "appVersion.alert.message", String(describing: p1), String(describing: p2), String(describing: p3), String(describing: p4))
      }
      /// App Version
      internal static let title = L10n.tr("Localizable", "appVersion.alert.title")
    }
  }

  internal enum Button {
    internal enum Private {
      /// Create private meeting
      internal static let meeting = L10n.tr("Localizable", "button.private.meeting")
    }
    internal enum Scanner {
      /// Self check-in
      internal static let checkin = L10n.tr("Localizable", "button.scanner.checkin")
    }
    internal enum Show {
      /// Show my QR code
      internal static let qr = L10n.tr("Localizable", "button.show.qr")
    }
  }

  internal enum Camera {
    internal enum Access {
      /// Once you've granted access, you can scan QR codes within your luca app.
      internal static let description = L10n.tr("Localizable", "camera.access.description")
      /// luca needs to access your camera
      internal static let title = L10n.tr("Localizable", "camera.access.title")
      internal enum Activate {
        /// Press to\nactivate camera
        internal static let label = L10n.tr("Localizable", "camera.access.activate.label")
      }
    }
    internal enum Error {
      /// Your device cannot be used to scan.
      internal static let scanningFailed = L10n.tr("Localizable", "camera.error.scanningFailed")
    }
    internal enum Warning {
      internal enum Checkin {
        /// Hey, you're scanning the QR code of a document and not of a luca location. If you want to add a document, click "CONTINUE".
        internal static let description = L10n.tr("Localizable", "camera.warning.checkin.description")
        /// Continue with import?
        internal static let title = L10n.tr("Localizable", "camera.warning.checkin.title")
      }
      internal enum Document {
        /// Hey, you're scanning the QR code of a luca location and not of a document. Click "CONTINUE" to check in.
        internal static let description = L10n.tr("Localizable", "camera.warning.document.description")
        /// Proceed with check-in?
        internal static let title = L10n.tr("Localizable", "camera.warning.document.title")
      }
      internal enum Incompatible {
        /// The QR code could not be scanned and the data could not be processed. This could be because this QR code is not compatible with luca.
        internal static let description = L10n.tr("Localizable", "camera.warning.incompatible.description")
        /// Operation failed
        internal static let title = L10n.tr("Localizable", "camera.warning.incompatible.title")
      }
    }
  }

  internal enum Checkin {
    /// Check-in
    internal static let noun = L10n.tr("Localizable", "checkin.noun")
    internal enum Consent {
      /// You want to check in at %@?
      internal static func description(_ p1: Any) -> String {
        return L10n.tr("Localizable", "checkin.consent.description", String(describing: p1))
      }
      /// Confirm & Don't ask again
      internal static let dontAskAgain = L10n.tr("Localizable", "checkin.consent.dontAskAgain")
    }
    internal enum Failure {
      internal enum AlreadyCheckedIn {
        /// You are already checked in.
        internal static let message = L10n.tr("Localizable", "checkin.failure.alreadyCheckedIn.message")
      }
      internal enum MissingIsPrivateFlag {
        /// Check-in not possible. Apparently, this QR-Code is not valid.
        internal static let message = L10n.tr("Localizable", "checkin.failure.missingIsPrivateFlag.message")
      }
      internal enum NotAvailableAnymore {
        /// It is not possible to check in here any more.
        internal static let message = L10n.tr("Localizable", "checkin.failure.notAvailableAnymore.message")
      }
      internal enum PrivateMeetingRunning {
        /// You can't check in while you are hosting a private meeting. End the meeting and try again.
        internal static let message = L10n.tr("Localizable", "checkin.failure.privateMeetingRunning.message")
      }
    }
    internal enum Kids {
      /// Add kid
      internal static let add = L10n.tr("Localizable", "checkin.kids.add")
      /// Add another kid
      internal static let addAnother = L10n.tr("Localizable", "checkin.kids.addAnother")
      /// Checkin kids
      internal static let checkin = L10n.tr("Localizable", "checkin.kids.checkin")
      /// Select the kids that shall be checked in with you. Your selection will not be saved und needs to be re-submitted on every checkin.
      internal static let description = L10n.tr("Localizable", "checkin.kids.description")
    }
    internal enum Qr {
      /// This is your QR code. To check in, have it scanned at a luca location.
      internal static let description = L10n.tr("Localizable", "checkin.qr.description")
      /// Check-in
      internal static let title = L10n.tr("Localizable", "checkin.qr.title")
      internal enum Title {
        /// My QR-Code
        internal static let myQRCode = L10n.tr("Localizable", "checkin.qr.title.myQRCode")
      }
    }
    internal enum Scanner {
      /// Scan the QR code of a place that uses luca or a private meeting to check yourself in.
      internal static let description = L10n.tr("Localizable", "checkin.scanner.description")
      /// You are currently unable to check in. Check your connection and try again. Please contact us if your check-in still does not work.
      internal static let publicKeyOutdatedDescription = L10n.tr("Localizable", "checkin.scanner.publicKeyOutdatedDescription")
      /// Check-in not possible
      internal static let publicKeyOutdatedTitle = L10n.tr("Localizable", "checkin.scanner.publicKeyOutdatedTitle")
      /// Self check-in
      internal static let title = L10n.tr("Localizable", "checkin.scanner.title")
    }
    internal enum Slider {
      /// %@
      internal static func date(_ p1: Any) -> String {
        return L10n.tr("Localizable", "checkin.slider.date", String(describing: p1))
      }
      /// dd.MM.yyyy HH:mm
      internal static let dateFormat = L10n.tr("Localizable", "checkin.slider.dateFormat")
    }
  }

  internal enum Checkout {
    /// Checkout
    internal static let noun = L10n.tr("Localizable", "checkout.noun")
  }

  internal enum Children {
    internal enum Add {
      /// Add
      internal static let button = L10n.tr("Localizable", "children.add.button")
      /// Please provide your child's full name. Documents can only be added if the names match.
      internal static let description = L10n.tr("Localizable", "children.add.description")
      /// Add Child
      internal static let title = L10n.tr("Localizable", "children.add.title")
      internal enum Placeholder {
        /// Firstname
        internal static let firstname = L10n.tr("Localizable", "children.add.placeholder.firstname")
        /// Lastname
        internal static let lastname = L10n.tr("Localizable", "children.add.placeholder.lastname")
      }
    }
    internal enum List {
      /// You can add children (up to 14 years) to your app to import their documents and check them in with you. The information will not be shared with the health department, it is for your convenience only.
      internal static let description = L10n.tr("Localizable", "children.list.description")
      /// You can add children (up to 14 years) to your app to import their documents and check them in with you.\n\nPlease make sure that the provided data is correct. The information will not be shared with the health department, it is for your convenience only.
      internal static let emptyDescription = L10n.tr("Localizable", "children.list.emptyDescription")
      /// Add Children
      internal static let title = L10n.tr("Localizable", "children.list.title")
      internal enum Add {
        /// Add Child
        internal static let button = L10n.tr("Localizable", "children.list.add.button")
      }
      internal enum Delete {
        /// When you delete a child, all the related documents are also deleted.
        internal static let message = L10n.tr("Localizable", "children.list.delete.message")
        /// Save changes?
        internal static let title = L10n.tr("Localizable", "children.list.delete.title")
      }
    }
  }

  internal enum Contact {
    internal enum Qr {
      /// Scan QR code
      internal static let scan = L10n.tr("Localizable", "contact.qr.scan")
      internal enum Accessibility {
        /// QR Code
        internal static let qrCode = L10n.tr("Localizable", "contact.qr.accessibility.qrCode")
        /// QR Code Scanner
        internal static let qrCodeScanner = L10n.tr("Localizable", "contact.qr.accessibility.qrCodeScanner")
      }
      internal enum Button {
        /// CLOSE QR CODE SCANNER
        internal static let closeScanner = L10n.tr("Localizable", "contact.qr.button.closeScanner")
        /// SELF CHECK-IN
        internal static let selfCheckin = L10n.tr("Localizable", "contact.qr.button.selfCheckin")
      }
    }
  }

  internal enum ContactViewController {
    /// Save
    internal static let save = L10n.tr("Localizable", "contactViewController.save")
    internal enum EmptyAddress {
      /// You must enter a complete address!
      internal static let message = L10n.tr("Localizable", "contactViewController.emptyAddress.message")
    }
    internal enum EmptyRest {
      /// You must fill out all fields.
      internal static let message = L10n.tr("Localizable", "contactViewController.emptyRest.message")
    }
    internal enum SaveFailed {
      /// Save failed: %@
      internal static func message(_ p1: Any) -> String {
        return L10n.tr("Localizable", "contactViewController.saveFailed.message", String(describing: p1))
      }
    }
    internal enum ShouldSave {
      /// Apply all changes? If you've changed your name and it doesn't match the name in your imported tests, these might be deleted.
      internal static let message = L10n.tr("Localizable", "contactViewController.shouldSave.message")
      /// Save changes
      internal static let title = L10n.tr("Localizable", "contactViewController.shouldSave.title")
    }
  }

  internal enum DailyKey {
    internal enum Fetch {
      internal enum FailedToDownload {
        /// The luca Service is currently not available, please try again later.
        internal static let message = L10n.tr("Localizable", "dailyKey.fetch.failedToDownload.message")
      }
      internal enum FailedToSave {
        /// The daily key could not be saved! %@
        internal static func message(_ p1: Any) -> String {
          return L10n.tr("Localizable", "dailyKey.fetch.failedToSave.message", String(describing: p1))
        }
      }
    }
  }

  internal enum Data {
    internal enum Access {
      /// SHOW
      internal static let showButton = L10n.tr("Localizable", "data.access.showButton")
      /// New data request
      internal static let title = L10n.tr("Localizable", "data.access.title")
      internal enum Notification {
        /// A health department has requested your contact data.
        internal static let description = L10n.tr("Localizable", "data.access.notification.description")
      }
      internal enum Title {
        /// Show new data request
        internal static let accessibility = L10n.tr("Localizable", "data.access.title.accessibility")
      }
      internal enum WarningLevel1 {
        /// Du hast eine neue Datenanfrage.
        internal static let title = L10n.tr("Localizable", "data.access.warningLevel1.title")
      }
      internal enum WarningLevel2 {
        /// Du hast dich an einer besonders kritischen Stelle aufgehalten und wirst gewarnt.
        internal static let title = L10n.tr("Localizable", "data.access.warningLevel2.title")
      }
    }
    internal enum Clear {
      /// Do you really want to clear your history?\n\nPast entries won't be displayed in your app anymore, but will stay in the system for up to 28 days and will be shared with the health authorities if you share your data.
      internal static let description = L10n.tr("Localizable", "data.clear.description")
      /// Clear history
      internal static let title = L10n.tr("Localizable", "data.clear.title")
    }
    internal enum ResetData {
      /// Do you really want to delete your account?\n\nIf you delete your account, you will not be able to check in or access your history.\n\nIn accordance with the Corona/COVID-19 infection control regulations, all encrypted check-in data will be deleted after 4 weeks.\n\nOnce your account is deleted, you can still be notified by a health department up to 4 weeks after your last check-in.
      internal static let description = L10n.tr("Localizable", "data.resetData.description")
      /// Delete account
      internal static let title = L10n.tr("Localizable", "data.resetData.title")
    }
    internal enum Shared {
      /// You have shared your check-ins from the last %@ days with the health department.
      internal static func description(_ p1: Any) -> String {
        return L10n.tr("Localizable", "data.shared.description", String(describing: p1))
      }
      /// Data shared
      internal static let title = L10n.tr("Localizable", "data.shared.title")
    }
  }

  internal enum DataPrivacy {
    internal enum Info {
      /// Please enter your contact data in the next step.\n\nYour data will be encrypted. Only a health department can decrypt it during contact tracing.
      internal static let description = L10n.tr("Localizable", "dataPrivacy.info.description")
      /// GOT IT!
      internal static let okButton = L10n.tr("Localizable", "dataPrivacy.info.okButton")
      /// Your contact data
      internal static let title = L10n.tr("Localizable", "dataPrivacy.info.title")
    }
  }

  internal enum DataRelease {
    internal enum Tan {
      /// TAN
      internal static let title = L10n.tr("Localizable", "dataRelease.tan.title")
      internal enum Failure {
        /// The TAN could not be loaded: %@
        internal static func message(_ p1: Any) -> String {
          return L10n.tr("Localizable", "dataRelease.tan.failure.message", String(describing: p1))
        }
      }
    }
  }

  internal enum DataReport {
    /// All information about the processing, purposes, legal bases and deletion periods can be found in our privacy policy: https://www.luca-app.de/app-privacy-policy/. Your data will not be transferred to third countries. Automated decision-making and profiling according to Art. 22 DSGVO are not carried out by us or the contractors of the luca system.\nIf you would like to receive information about your data, which we have received as clear data through your own contact (e.g. support requests), please contact our Privacy Team at privacy@culture4life.de. Please share any other information that would help us identify you and research your related data with us. If you have been in contact with us via social media, information such as your e-mail address or the pseudonym you use in those social media channels are important in order to provide you with your requested information.\nIn accordance with Art. 77 DSGVO, you have the right to complain to the supervisory authority if you believe that your personal data is not processed lawfully. The address of the supervisory authority responsible for our company is: Berliner Beauftragte für Datenschutz und Informationsfreiheit, Friedrichstr. 219, 10969 Berlin, Tel.: 030/13889-0, e-mail: mailbox@datenschutz-berlin.de.
    internal static let footer = L10n.tr("Localizable", "dataReport.footer")
    internal enum AccountSettings {
      /// Covid certificates
      internal static let certificatesReport = L10n.tr("Localizable", "dataReport.accountSettings.certificatesReport")
      /// Contact tracing
      internal static let contactReport = L10n.tr("Localizable", "dataReport.accountSettings.contactReport")
      /// Request data
      internal static let title = L10n.tr("Localizable", "dataReport.accountSettings.title")
    }
    internal enum ContactData {
      /// Check in time: %@\nDuration: %@\nLocation: %@
      internal static func checkIn(_ p1: Any, _ p2: Any, _ p3: Any) -> String {
        return L10n.tr("Localizable", "dataReport.contactData.checkIn", String(describing: p1), String(describing: p2), String(describing: p3))
      }
      /// Contact tracing:\nYour data collected and processed for contact tracing is stored locally in your app and encrypted on the luca server. The luca server is hosted by Deutsche Telekom AG. Certification of this can be found at: https://open-telekom-cloud.com/de/sicherheit/datenschutz-compliance. Furthermore, Bundesdruckerei Gruppe GmbH provides other IT infrastructure services that enable secure transmission of your data to health authorities. Our subcontractor neXenio provides software development, software maintenance and software operation services. Neither we nor neXenio GmbH can decrypt or clearly view your data at any time. During registration we verify your phone number by sending an automated SMS. For this purpose, your phone number will be sent to our SMS service provider. The current recipients can be found in our current privacy policy: https://www.luca-app.de/app-privacy-policy/\n\nOnly you can view, edit and delete your data within your app. If you have already checked in at a luca location and a health department has requested it from the operator, the dual-encrypted data will be decrypted by the operator and then by the health department. Only the health department can access the decrypted data while contact tracing and view your data. With the exception of the health authorities (legal basis Art. 6 para. 1 lit. c DSGVO in conjunction with the respective state ordinance to combat COVID-19 infections, Infection Protection Act), we do not pass on your data to third parties.
      internal static let header = L10n.tr("Localizable", "dataReport.contactData.header")
      /// Private meeting\nHost name: %@\nCheck in time: %@\nDuration: %@
      internal static func privateMeetingCheckIn(_ p1: Any, _ p2: Any, _ p3: Any) -> String {
        return L10n.tr("Localizable", "dataReport.contactData.privateMeetingCheckIn", String(describing: p1), String(describing: p2), String(describing: p3))
      }
      /// Private meeting\nGuests: %@\nCheck in time: %@\nDuration: %@
      internal static func privateMeetingHost(_ p1: Any, _ p2: Any, _ p3: Any) -> String {
        return L10n.tr("Localizable", "dataReport.contactData.privateMeetingHost", String(describing: p1), String(describing: p2), String(describing: p3))
      }
      /// Trace Info\nCheck in Zeit: %@\nCheck out Zeit: %@\nLocation Id: %@
      internal static func traceInfo(_ p1: Any, _ p2: Any, _ p3: Any) -> String {
        return L10n.tr("Localizable", "dataReport.contactData.traceInfo", String(describing: p1), String(describing: p2), String(describing: p3))
      }
      /// Contact data:\nName: %@\nAddress: %@\nPhone number: %@\nMail: %@\nStorage location: luca-Server, local\nStorage period: Deletion takes place 28 days after the deletion of the account (delete button) or annually at the end of each year
      internal static func userData(_ p1: Any, _ p2: Any, _ p3: Any, _ p4: Any) -> String {
        return L10n.tr("Localizable", "dataReport.contactData.userData", String(describing: p1), String(describing: p2), String(describing: p3), String(describing: p4))
      }
      /// Date Transfer\nTime: %@\nNumber of shared days: %i
      internal static func userDataTransfer(_ p1: Any, _ p2: Int) -> String {
        return L10n.tr("Localizable", "dataReport.contactData.userDataTransfer", String(describing: p1), p2)
      }
    }
    internal enum Document {
      /// Covid certificates:\nWithin your luca app, you have the option to store a test result, vaccination or recovery certificate. Your data stored and transferred to the luca app will not leave your smartphone. Your data is only stored locally in your app on your smartphone. A transmission to our servers, to the operator or to the health authorities does not take place. Your data will not be passed on to third parties.\nOnly information necessary for identity matching (first and last name, date of birth, vaccination date, issuer and vaccine) is displayed in the app. Other data may be included in the test or ID document, but will not be displayed to you. All data contained in the QR code is listed here. To prevent misuse, so that the same document cannot be stored by different people in the luca app, a pseudonymized identifier is created by your app and transmitted to the luca system. Only the identifier is stored in the luca system. We cannot assign it to you.
      internal static let header = L10n.tr("Localizable", "dataReport.document.header")
      internal enum Appointment {
        /// %@\nCertificate information:\n%@\nQR-Code: %@\nStorage location: local\n%@
        internal static func description(_ p1: Any, _ p2: Any, _ p3: Any, _ p4: Any) -> String {
          return L10n.tr("Localizable", "dataReport.document.appointment.description", String(describing: p1), String(describing: p2), String(describing: p3), String(describing: p4))
        }
      }
      internal enum Certificate {
        /// %@\nCertificate information:\n%@\nQR-Code: %@\nStorage location: local\n%@
        internal static func description(_ p1: Any, _ p2: Any, _ p3: Any, _ p4: Any) -> String {
          return L10n.tr("Localizable", "dataReport.document.certificate.description", String(describing: p1), String(describing: p2), String(describing: p3), String(describing: p4))
        }
        /// Storage period: Deletion takes place via deletion in the app or as soon as validity is exceeded
        internal static let storagePeriodAppointment = L10n.tr("Localizable", "dataReport.document.certificate.storagePeriodAppointment")
        /// Storage period: Deletion takes place via deletion in the app or after %@
        internal static func storagePeriodTest(_ p1: Any) -> String {
          return L10n.tr("Localizable", "dataReport.document.certificate.storagePeriodTest", String(describing: p1))
        }
        /// Storage period: Deletion takes place via deletion in the app
        internal static let storagePeriodVaccine = L10n.tr("Localizable", "dataReport.document.certificate.storagePeriodVaccine")
      }
      internal enum CertificateType {
        /// Certificate type: Appointment
        internal static let appointment = L10n.tr("Localizable", "dataReport.document.certificateType.appointment")
        /// Certificate type: Test(BärCode Format)
        internal static let baercodeTest = L10n.tr("Localizable", "dataReport.document.certificateType.baercodeTest")
        /// Certificate type: Vaccination (BärCode)
        internal static let baercodeVaccination = L10n.tr("Localizable", "dataReport.document.certificateType.baercodeVaccination")
        /// Certificate type: Recovery (DGC)
        internal static let dgcRecovery = L10n.tr("Localizable", "dataReport.document.certificateType.dgcRecovery")
        /// Certificate type: Test (DGC)
        internal static let dgcTest = L10n.tr("Localizable", "dataReport.document.certificateType.dgcTest")
        /// Certificate type: Vaccination (DGC)
        internal static let dgcVaccination = L10n.tr("Localizable", "dataReport.document.certificateType.dgcVaccination")
        /// Certificate type: Test(ticket.io Format)
        internal static let ticketIOTest = L10n.tr("Localizable", "dataReport.document.certificateType.ticketIOTest")
      }
      internal enum ContactData {
        /// Date of birth: %@
        internal static func dateOfBirth(_ p1: Any) -> String {
          return L10n.tr("Localizable", "dataReport.document.contactData.dateOfBirth", String(describing: p1))
        }
        /// First name: %@\nLast name: %@
        internal static func general(_ p1: Any, _ p2: Any) -> String {
          return L10n.tr("Localizable", "dataReport.document.contactData.general", String(describing: p1), String(describing: p2))
        }
        /// Name Hash: %@
        internal static func unknownUser(_ p1: Any) -> String {
          return L10n.tr("Localizable", "dataReport.document.contactData.unknownUser", String(describing: p1))
        }
      }
      internal enum DescriptionDetails {
        /// Test type: %@\nTest centre: %@\nDate: %@\nAddress of test centre: %@
        internal static func appointment(_ p1: Any, _ p2: Any, _ p3: Any, _ p4: Any) -> String {
          return L10n.tr("Localizable", "dataReport.document.descriptionDetails.appointment", String(describing: p1), String(describing: p2), String(describing: p3), String(describing: p4))
        }
        /// Result: %@\nIssuer: %@
        internal static func baercodeTest(_ p1: Any, _ p2: Any) -> String {
          return L10n.tr("Localizable", "dataReport.document.descriptionDetails.baercodeTest", String(describing: p1), String(describing: p2))
        }
        /// Targeted disease: %i\nProcedure operator: %@
        internal static func baercodeVaccination(_ p1: Int, _ p2: Any) -> String {
          return L10n.tr("Localizable", "dataReport.document.descriptionDetails.baercodeVaccination", p1, String(describing: p2))
        }
        /// Disease or agent targeted "tg": %@\nISO 8601 complete date of first positive NAA test result "fr": %@\nCountry of Test "co": %@\nCertificate Issuer "is": %@\nISO 8601 complete date: Certificate Valid From "df": %@\nISO 8601 complete date: Certificate Valid Unti "du": %@\nUnique Certificate Identifier: UVCI "ci": %@
        internal static func dgcRecovery(_ p1: Any, _ p2: Any, _ p3: Any, _ p4: Any, _ p5: Any, _ p6: Any, _ p7: Any) -> String {
          return L10n.tr("Localizable", "dataReport.document.descriptionDetails.dgcRecovery", String(describing: p1), String(describing: p2), String(describing: p3), String(describing: p4), String(describing: p5), String(describing: p6), String(describing: p7))
        }
        /// Targeted disease "tg": %@\nType of test "tt": %@\nDate/Time of Sample Collection "sc": %@\nTest Result "tr": %@\nTesting Centre "tc": %@\nCountry of Test "co": %@\nCertificate Issuer "is": %@\nUnique Certificate Identifier, UVCI "ci": %@
        internal static func dgcTest(_ p1: Any, _ p2: Any, _ p3: Any, _ p4: Any, _ p5: Any, _ p6: Any, _ p7: Any, _ p8: Any) -> String {
          return L10n.tr("Localizable", "dataReport.document.descriptionDetails.dgcTest", String(describing: p1), String(describing: p2), String(describing: p3), String(describing: p4), String(describing: p5), String(describing: p6), String(describing: p7), String(describing: p8))
        }
        /// Disease or agent targeted "tg": %@\nVaccine or prophylaxis "vp": %@\nVaccine medicinal product "mp": %@\nMarketing Authorization Holder - if no MAH present, then manufacturer "ma": %@\nDose Number "dn": %i\nTotal Series of Doses "sd": %i\nISO8601 complete date: Date of Vaccination "dt": %@\nCountry of Vaccination "co": %@\nCertificate Issuer "is":  %@\nUnique Certificate Identifier: UVCI "ci": %@
        internal static func dgcVaccination(_ p1: Any, _ p2: Any, _ p3: Any, _ p4: Any, _ p5: Int, _ p6: Int, _ p7: Any, _ p8: Any, _ p9: Any, _ p10: Any) -> String {
          return L10n.tr("Localizable", "dataReport.document.descriptionDetails.dgcVaccination", String(describing: p1), String(describing: p2), String(describing: p3), String(describing: p4), p5, p6, String(describing: p7), String(describing: p8), String(describing: p9), String(describing: p10))
        }
        /// Result: %@\nTest type: %@\nDate: %@\nIssuer: %@\nDoc: %@
        internal static func ticketIOTest(_ p1: Any, _ p2: Any, _ p3: Any, _ p4: Any, _ p5: Any) -> String {
          return L10n.tr("Localizable", "dataReport.document.descriptionDetails.ticketIOTest", String(describing: p1), String(describing: p2), String(describing: p3), String(describing: p4), String(describing: p5))
        }
        internal enum Baercode {
          /// Procedure %i: %@ %@
          internal static func procedure(_ p1: Int, _ p2: Any, _ p3: Any) -> String {
            return L10n.tr("Localizable", "dataReport.document.descriptionDetails.baercode.procedure", p1, String(describing: p2), String(describing: p3))
          }
        }
      }
    }
  }

  internal enum Done {
    /// You can now share your encrypted contact data by presenting your QR code or by scanning a code from a place that uses luca.\n\nChecking in is possible at places that use luca or private meetings with other luca App users.
    internal static let description = L10n.tr("Localizable", "done.description")
    /// OKAY!
    internal static let okButton = L10n.tr("Localizable", "done.okButton")
    /// That's it!
    internal static let title = L10n.tr("Localizable", "done.title")
  }

  internal enum Error {
    internal enum DeleteUser {
      /// Your account has already been deleted.\n\nYou can now register again.
      internal static let alreadyDeleted = L10n.tr("Localizable", "error.deleteUser.alreadyDeleted")
      /// Your account could not be deleted. (400 - Bad Request Error)\n\nPlease contact our support.
      internal static let badInput = L10n.tr("Localizable", "error.deleteUser.badInput")
      /// Your account could not be deleted. (403 - Forbidden Error)\n\nPlease contact our support.
      internal static let invalidSignature = L10n.tr("Localizable", "error.deleteUser.invalidSignature")
      /// Your account could not be deleted.\n\nPlease try again.
      internal static let rateLimit = L10n.tr("Localizable", "error.deleteUser.rateLimit")
      /// Your account could not be deleted.\n\nPlease contact our support.
      internal static let unableToBuildPayload = L10n.tr("Localizable", "error.deleteUser.unableToBuildPayload")
      /// Your account does not exist.\n\nYou can now register again.
      internal static let userNotFound = L10n.tr("Localizable", "error.deleteUser.userNotFound")
    }
    internal enum Network {
      /// The luca Service is currently unavailable, please try again later.
      internal static let badGateway = L10n.tr("Localizable", "error.network.badGateway")
    }
  }

  internal enum FetchTraceId {
    internal enum Failure {
      /// Error while checking the user status: %@
      internal static func message(_ p1: Any) -> String {
        return L10n.tr("Localizable", "fetchTraceId.failure.message", String(describing: p1))
      }
    }
  }

  internal enum FormViewController {
    internal enum PhoneVerificationFailure {
      /// Please enter a new phone number.
      internal static let message = L10n.tr("Localizable", "formViewController.phoneVerificationFailure.message")
      /// Phone number verification failed
      internal static let title = L10n.tr("Localizable", "formViewController.phoneVerificationFailure.title")
    }
    internal enum UserDataFailure {
      internal enum Failed {
        /// User could not be registered: %@
        internal static func message(_ p1: Any) -> String {
          return L10n.tr("Localizable", "formViewController.userDataFailure.failed.message", String(describing: p1))
        }
      }
      internal enum Unavailable {
        /// User data is not available
        internal static let message = L10n.tr("Localizable", "formViewController.userDataFailure.unavailable.message")
      }
    }
    internal enum VerificationCancelled {
      /// Phone number unverified.
      internal static let message = L10n.tr("Localizable", "formViewController.verificationCancelled.message")
    }
    internal enum VerificationUnconfirmed {
      /// Phone number unconfirmed.
      internal static let message = L10n.tr("Localizable", "formViewController.verificationUnconfirmed.message")
    }
  }

  internal enum General {
    /// Privacy policy
    internal static let dataPrivacy = L10n.tr("Localizable", "general.dataPrivacy")
    /// FAQ
    internal static let faq = L10n.tr("Localizable", "general.faq")
    /// Daily Key
    internal static let healthDepartmentKey = L10n.tr("Localizable", "general.healthDepartmentKey")
    /// Imprint
    internal static let imprint = L10n.tr("Localizable", "general.imprint")
    /// https://www.luca-app.de/impressum/
    internal static let linkImprint = L10n.tr("Localizable", "general.linkImprint")
    /// Support
    internal static let support = L10n.tr("Localizable", "general.support")
    /// Terms of use
    internal static let termsAndConditions = L10n.tr("Localizable", "general.termsAndConditions")
    /// luca
    internal static let title = L10n.tr("Localizable", "general.title")
    internal enum Failure {
      internal enum InvalidCertificate {
        /// When attempting to establish a secure connection to the luca servers, we found untrusted certificates.\n\nPlease try switching your network and make sure that you're using the latest app version.
        internal static let message = L10n.tr("Localizable", "general.failure.invalidCertificate.message")
        /// Unexpected server certificates
        internal static let title = L10n.tr("Localizable", "general.failure.invalidCertificate.title")
      }
      internal enum NoInternet {
        /// The process could not be executed. Please connect to the internet and try again.
        internal static let message = L10n.tr("Localizable", "general.failure.noInternet.message")
        /// No internet connection
        internal static let title = L10n.tr("Localizable", "general.failure.noInternet.title")
      }
      internal enum Unknown {
        /// Unknown error: %@
        internal static func message(_ p1: Any) -> String {
          return L10n.tr("Localizable", "general.failure.unknown.message", String(describing: p1))
        }
      }
    }
    internal enum Gitlab {
      internal enum Alert {
        /// Open GitLab
        internal static let actionButton = L10n.tr("Localizable", "general.gitlab.alert.actionButton")
        /// luca is open source. It's important to us that everyone can comprehend how the luca system works and that it's possible to give independent feedback. If you want to take a closer look, you'll find the code repository on GitLab.
        internal static let description = L10n.tr("Localizable", "general.gitlab.alert.description")
        /// Take me to the code repository 
        internal static let title = L10n.tr("Localizable", "general.gitlab.alert.title")
      }
    }
    internal enum Greeting {
      /// Hello!
      internal static let title = L10n.tr("Localizable", "general.greeting.title")
    }
    internal enum OperationFailure {
      internal enum Error {
        /// Operation failed
        internal static let title = L10n.tr("Localizable", "general.operationFailure.error.title")
      }
    }
    internal enum Support {
      /// hello@luca-app.de
      internal static let email = L10n.tr("Localizable", "general.support.email")
      /// Emails cannot be sent from this device because no email account is linked.
      internal static let error = L10n.tr("Localizable", "general.support.error")
      internal enum Email {
        /// Dear luca support,<br><br>The following problem occurred while using the app:<br><br><i>Please describe your problem and where it occurs, as precisely as possible. It would also be very helpful if you could attach a screenshot or screen capture of your problem or error message.</i><br><br><br>Device: %@<br>Operating system: iOS %@<br>App version: %@
        internal static func body(_ p1: Any, _ p2: Any, _ p3: Any) -> String {
          return L10n.tr("Localizable", "general.support.email.body", String(describing: p1), String(describing: p2), String(describing: p3))
        }
      }
    }
  }

  internal enum History {
    internal enum Accessibility {
      /// View data accesses
      internal static let dataAccessButton = L10n.tr("Localizable", "history.accessibility.dataAccessButton")
    }
    internal enum Alert {
      /// Would you like to share your contact data and the last %@ days of your history with the health department? This is only done with your voluntary consent in accordance with art. 9 (2) a) GDPR (more: %@).
      internal static func description(_ p1: Any, _ p2: Any) -> String {
        return L10n.tr("Localizable", "history.alert.description", String(describing: p1), String(describing: p2))
      }
      /// data privacy - app
      internal static let link = L10n.tr("Localizable", "history.alert.link")
      /// Confirm share
      internal static let title = L10n.tr("Localizable", "history.alert.title")
      internal enum Description {
        /// Would you like to share your contact data and the last 14 days of your history with the health department? This is only done with your voluntary consent under article 9 (2) a) GDPR (more: %@).
        internal static func accessibility(_ p1: Any) -> String {
          return L10n.tr("Localizable", "history.alert.description.accessibility", String(describing: p1))
        }
      }
    }
    internal enum Checkin {
      internal enum Checkout {
        /// %@ to %@
        internal static func time(_ p1: Any, _ p2: Any) -> String {
          return L10n.tr("Localizable", "history.checkin.checkout.time", String(describing: p1), String(describing: p2))
        }
      }
    }
    internal enum Data {
      /// Your history collects all your activities from the last 28 days. You don't have any entries during this period at the moment.
      internal static let empty = L10n.tr("Localizable", "history.data.empty")
      /// Share history
      internal static let share = L10n.tr("Localizable", "history.data.share")
      /// Shared data
      internal static let shared = L10n.tr("Localizable", "history.data.shared")
      /// Updated data
      internal static let updated = L10n.tr("Localizable", "history.data.updated")
    }
    internal enum DataAccess {
      /// %@ data requests
      internal static func count(_ p1: Any) -> String {
        return L10n.tr("Localizable", "history.dataAccess.count", String(describing: p1))
      }
    }
    internal enum Detail {
      internal enum PrivateMeeting {
        /// Private meetings are meant for small gatherings with friends and family. Private meetings are used as reminders and won't be shared with health authorities - that's why they display the full names of you and your guests.
        internal static let information = L10n.tr("Localizable", "history.detail.privateMeeting.information")
        /// Information
        internal static let title = L10n.tr("Localizable", "history.detail.privateMeeting.title")
      }
    }
    internal enum Kids {
      /// Kids: %@
      internal static func title(_ p1: Any) -> String {
        return L10n.tr("Localizable", "history.kids.title", String(describing: p1))
      }
    }
    internal enum Share {
      internal enum DisplayTAN {
        /// Okay
        internal static let continueButtonTitle = L10n.tr("Localizable", "history.share.displayTAN.continueButtonTitle")
        /// If you give this TAN to a health department, they can decrypt your history and inform luca places.
        internal static let description = L10n.tr("Localizable", "history.share.displayTAN.description")
        /// Share data
        internal static let title = L10n.tr("Localizable", "history.share.displayTAN.title")
      }
      internal enum GenerateTAN {
        /// Generate TAN
        internal static let continueButtonTitle = L10n.tr("Localizable", "history.share.generateTAN.continueButtonTitle")
        /// Share data
        internal static let title = L10n.tr("Localizable", "history.share.generateTAN.title")
      }
      internal enum SelectTimeFrame {
        /// Select
        internal static let continueButtonTitle = L10n.tr("Localizable", "history.share.selectTimeFrame.continueButtonTitle")
        /// Your history gathers all of your check-ins from the last 28 days. You can decide how many days you want to share with the health department. Define the number of days:
        internal static let description = L10n.tr("Localizable", "history.share.selectTimeFrame.description")
        /// Define time period
        internal static let title = L10n.tr("Localizable", "history.share.selectTimeFrame.title")
      }
    }
    internal enum TraceId {
      /// Trace ID: %@
      internal static func pasteboard(_ p1: Any) -> String {
        return L10n.tr("Localizable", "history.traceId.pasteboard", String(describing: p1))
      }
      internal enum Alert {
        /// %@\n\nThe trace ID was copied to your clipboard.
        internal static func description(_ p1: Any) -> String {
          return L10n.tr("Localizable", "history.traceId.alert.description", String(describing: p1))
        }
      }
    }
  }

  internal enum List {
    ///  and 
    internal static let and = L10n.tr("Localizable", "list.and")
    /// , and 
    internal static let lastElement = L10n.tr("Localizable", "list.lastElement")
  }

  internal enum LocationCheckinViewController {
    /// Automatic checkout
    internal static let autoCheckout = L10n.tr("Localizable", "locationCheckinViewController.autoCheckout")
    /// You're checked in!
    internal static let welcomeMessage = L10n.tr("Localizable", "locationCheckinViewController.welcomeMessage")
    /// You're checked in with %@!
    internal static func welcomeMessageWithKids(_ p1: Any) -> String {
      return L10n.tr("Localizable", "locationCheckinViewController.welcomeMessageWithKids", String(describing: p1))
    }
    internal enum Accessibility {
      /// Check out
      internal static let checkoutSlider = L10n.tr("Localizable", "locationCheckinViewController.accessibility.checkoutSlider")
      /// Checkout of location
      internal static let directCheckout = L10n.tr("Localizable", "locationCheckinViewController.accessibility.directCheckout")
    }
    internal enum AdditionalData {
      /// Table: %@
      internal static func table(_ p1: Any) -> String {
        return L10n.tr("Localizable", "locationCheckinViewController.additionalData.table", String(describing: p1))
      }
    }
    internal enum AutoCheckout {
      /// Off
      internal static let off = L10n.tr("Localizable", "locationCheckinViewController.autoCheckout.off")
      /// On
      internal static let on = L10n.tr("Localizable", "locationCheckinViewController.autoCheckout.on")
      internal enum Permission {
        internal enum BeforePrompt {
          /// data privacy - app
          internal static let link = L10n.tr("Localizable", "locationCheckinViewController.autoCheckout.permission.beforePrompt.link")
          /// This automatic checkout feature uses geofencing. If a luca-location uses it as well, you can check out automatically using your location through the GPS function of your phone, even when the app is closed. This is only done with your consent under art. 6 (1) 1 a) GDPR (more: %@). However, you can still check out manually at any time.
          internal static func message(_ p1: Any) -> String {
            return L10n.tr("Localizable", "locationCheckinViewController.autoCheckout.permission.beforePrompt.message", String(describing: p1))
          }
          /// OKAY
          internal static let okButton = L10n.tr("Localizable", "locationCheckinViewController.autoCheckout.permission.beforePrompt.okButton")
          /// Automatic checkout
          internal static let title = L10n.tr("Localizable", "locationCheckinViewController.autoCheckout.permission.beforePrompt.title")
        }
      }
    }
    internal enum AutoCheckoutPermissionDisabled {
      /// Change your luca location permission to "Always".
      internal static let message = L10n.tr("Localizable", "locationCheckinViewController.autoCheckoutPermissionDisabled.message")
      /// Auto-checkout is not possible
      internal static let title = L10n.tr("Localizable", "locationCheckinViewController.autoCheckoutPermissionDisabled.title")
    }
    internal enum CheckOutFailed {
      internal enum LocationNotAvailable {
        /// There is no information available about the current location.
        internal static let message = L10n.tr("Localizable", "locationCheckinViewController.checkOutFailed.locationNotAvailable.message")
      }
      internal enum LowDuration {
        /// You just checked in! You can first check out when you've stayed a bit longer.
        internal static let message = L10n.tr("Localizable", "locationCheckinViewController.checkOutFailed.lowDuration.message")
      }
      internal enum StillInLocation {
        /// You are still at %@. Move farther away to check out.
        internal static func message(_ p1: Any) -> String {
          return L10n.tr("Localizable", "locationCheckinViewController.checkOutFailed.stillInLocation.message", String(describing: p1))
        }
        /// You are still at the location. Move farther away to check out.
        internal static let messageWithoutName = L10n.tr("Localizable", "locationCheckinViewController.checkOutFailed.stillInLocation.messageWithoutName")
        /// Info
        internal static let title = L10n.tr("Localizable", "locationCheckinViewController.checkOutFailed.stillInLocation.title")
      }
    }
    internal enum LocationInfoFetchFailure {
      /// Error while loading the location data: %@
      internal static func message(_ p1: Any) -> String {
        return L10n.tr("Localizable", "locationCheckinViewController.locationInfoFetchFailure.message", String(describing: p1))
      }
    }
    internal enum Permission {
      internal enum Change {
        /// To use auto-checkout you must choose the option "Always" in your luca location settings.
        internal static let message = L10n.tr("Localizable", "locationCheckinViewController.permission.change.message")
        /// Location Settings
        internal static let title = L10n.tr("Localizable", "locationCheckinViewController.permission.change.title")
      }
      internal enum Denied {
        /// Your location setting has still not been enabled. Without your position we cannot check if you have left %@.
        internal static func message(_ p1: Any) -> String {
          return L10n.tr("Localizable", "locationCheckinViewController.permission.denied.message", String(describing: p1))
        }
        /// Your location setting has still not been enabled. Without your position we cannot check if you have left the location.
        internal static let messageWithoutName = L10n.tr("Localizable", "locationCheckinViewController.permission.denied.messageWithoutName")
      }
    }
  }

  internal enum MainTabBarViewController {
    internal enum ScannerFailure {
      /// Error: %@
      internal static func message(_ p1: Any) -> String {
        return L10n.tr("Localizable", "mainTabBarViewController.scannerFailure.message", String(describing: p1))
      }
      /// Scanner error
      internal static let title = L10n.tr("Localizable", "mainTabBarViewController.scannerFailure.title")
    }
    internal enum UserRegistrationFailure {
      internal enum General {
        /// User could not be registered: %@
        internal static func message(_ p1: Any) -> String {
          return L10n.tr("Localizable", "mainTabBarViewController.userRegistrationFailure.general.message", String(describing: p1))
        }
      }
      internal enum NoInternet {
        /// User could not be registered! No internet connection could be made.
        internal static let message = L10n.tr("Localizable", "mainTabBarViewController.userRegistrationFailure.noInternet.message")
      }
    }
  }

  internal enum My {
    internal enum Luca {
      /// Calendar
      internal static let calendar = L10n.tr("Localizable", "my.luca.calendar")
      /// My luca
      internal static let title = L10n.tr("Localizable", "my.luca.title")
    }
  }

  internal enum Navigation {
    /// More
    internal static let menu = L10n.tr("Localizable", "navigation.menu")
    internal enum Basic {
      /// AGREE
      internal static let agree = L10n.tr("Localizable", "navigation.basic.agree")
      /// Attention!
      internal static let attention = L10n.tr("Localizable", "navigation.basic.attention")
      /// BACK
      internal static let back = L10n.tr("Localizable", "navigation.basic.back")
      /// Cancel
      internal static let cancel = L10n.tr("Localizable", "navigation.basic.cancel")
      /// CLOSE
      internal static let close = L10n.tr("Localizable", "navigation.basic.close")
      /// Confirm
      internal static let confirm = L10n.tr("Localizable", "navigation.basic.confirm")
      /// Continue
      internal static let `continue` = L10n.tr("Localizable", "navigation.basic.continue")
      /// DONE
      internal static let done = L10n.tr("Localizable", "navigation.basic.done")
      /// Error
      internal static let error = L10n.tr("Localizable", "navigation.basic.error")
      /// Attention
      internal static let hint = L10n.tr("Localizable", "navigation.basic.hint")
      /// LET'S GO
      internal static let look = L10n.tr("Localizable", "navigation.basic.look")
      /// NEXT
      internal static let next = L10n.tr("Localizable", "navigation.basic.next")
      /// No
      internal static let no = L10n.tr("Localizable", "navigation.basic.no")
      /// Ok
      internal static let ok = L10n.tr("Localizable", "navigation.basic.ok")
      /// SKIP
      internal static let skip = L10n.tr("Localizable", "navigation.basic.skip")
      /// Start
      internal static let start = L10n.tr("Localizable", "navigation.basic.start")
      /// Yes
      internal static let yes = L10n.tr("Localizable", "navigation.basic.yes")
      internal enum Cancel {
        /// CANCEL
        internal static let uppercased = L10n.tr("Localizable", "navigation.basic.cancel.uppercased")
      }
    }
    internal enum DataAccess {
      /// Data requests
      internal static let title = L10n.tr("Localizable", "navigation.dataAccess.title")
    }
    internal enum Tab {
      /// Account
      internal static let account = L10n.tr("Localizable", "navigation.tab.account")
      /// Check in
      internal static let checkin = L10n.tr("Localizable", "navigation.tab.checkin")
      /// My luca
      internal static let health = L10n.tr("Localizable", "navigation.tab.health")
      /// History
      internal static let history = L10n.tr("Localizable", "navigation.tab.history")
    }
  }

  internal enum Notification {
    internal enum Checkout {
      /// Don't forget to checkout in the luca app when you've left your location.
      internal static let description = L10n.tr("Localizable", "notification.checkout.description")
      /// Important!
      internal static let title = L10n.tr("Localizable", "notification.checkout.title")
    }
    internal enum Permission {
      /// You didn't allow notifications. Please enable luca notifications in your settings to be reminded to checkout.
      internal static let description = L10n.tr("Localizable", "notification.permission.description")
      /// Enable notifications
      internal static let title = L10n.tr("Localizable", "notification.permission.title")
    }
  }

  internal enum OptionalCheckin {
    /// CHECK IN VOLUNTARILY
    internal static let button = L10n.tr("Localizable", "optionalCheckin.button")
    /// Optionally share contact information with the health department
    internal static let checkBox = L10n.tr("Localizable", "optionalCheckin.checkBox")
    /// Checking into this location is optional.  If you still want to help with contact tracing and be alerted in case of risk, you can check in voluntarily and even anonymously. Your contact information will not be shared with the health department if you check in anonymously.
    internal static let message = L10n.tr("Localizable", "optionalCheckin.message")
    /// Voluntary Check-in
    internal static let title = L10n.tr("Localizable", "optionalCheckin.title")
  }

  internal enum PhoneNumber {
    internal enum Confirmation {
      internal enum Description {
        /// You will receive a call with a message containing the TAN that is needed to verify your phone number in the next step. Please make sure the phone is nearby and the number is correct:
        internal static let fixed = L10n.tr("Localizable", "phoneNumber.confirmation.description.fixed")
        /// To verify your phone number we will send a TAN to the number you provided in the next step. Please make sure this number is correct:
        internal static let mobile = L10n.tr("Localizable", "phoneNumber.confirmation.description.mobile")
      }
    }
  }

  internal enum Private {
    internal enum Meeting {
      /// Let your guests scan the QR code to check into your meeting.
      internal static let description = L10n.tr("Localizable", "private.meeting.description")
      /// Length
      internal static let length = L10n.tr("Localizable", "private.meeting.length")
      /// Private Meeting
      internal static let title = L10n.tr("Localizable", "private.meeting.title")
      internal enum Accessibility {
        /// End meeting
        internal static let endMeeting = L10n.tr("Localizable", "private.meeting.accessibility.endMeeting")
        /// Guests: %d out of %d
        internal static func guests(_ p1: Int, _ p2: Int) -> String {
          return L10n.tr("Localizable", "private.meeting.accessibility.guests", p1, p2)
        }
        /// Length: %@
        internal static func length(_ p1: Any) -> String {
          return L10n.tr("Localizable", "private.meeting.accessibility.length", String(describing: p1))
        }
      }
      internal enum Alert {
        /// When you check yourself in at a private meeting, your host can see your first and last name.\n\nPrivate meetings are used as reminders and won't be shared with health authorities.
        internal static let description = L10n.tr("Localizable", "private.meeting.alert.description")
      }
      internal enum Create {
        /// There was an error while creating a new meeting: %@
        internal static func failure(_ p1: Any) -> String {
          return L10n.tr("Localizable", "private.meeting.create.failure", String(describing: p1))
        }
      }
      internal enum Details {
        /// Duration
        internal static let duration = L10n.tr("Localizable", "private.meeting.details.duration")
        /// Participants: %d
        internal static func guests(_ p1: Int) -> String {
          return L10n.tr("Localizable", "private.meeting.details.guests", p1)
        }
        /// Begin
        internal static let start = L10n.tr("Localizable", "private.meeting.details.start")
        /// Details
        internal static let title = L10n.tr("Localizable", "private.meeting.details.title")
      }
      internal enum End {
        /// Do you really want to end this meeting?
        internal static let description = L10n.tr("Localizable", "private.meeting.end.description")
        /// End meeting
        internal static let title = L10n.tr("Localizable", "private.meeting.end.title")
      }
      internal enum Info {
        /// When friends and family check into your private meeting, their first and last names will be shown in your app.\n\nThey will also see your first and last name in their app.\n\nYou can already use this feature at locations that are not part of the luca system to keep track of private meetings yourself.
        internal static let description = L10n.tr("Localizable", "private.meeting.info.description")
        /// Private Meeting
        internal static let title = L10n.tr("Localizable", "private.meeting.info.title")
      }
      internal enum Participants {
        /// No participants.
        internal static let `none` = L10n.tr("Localizable", "private.meeting.participants.none")
        /// Participants
        internal static let title = L10n.tr("Localizable", "private.meeting.participants.title")
      }
      internal enum Start {
        /// Do you want to start a private meeting now?\n\nWhen friends and family check into your private meeting, their first and last names will be shown in your app. They will also see your first and last name in their app.\n\nPrivate meetings are used as reminders and won't be shared with health authorities.
        internal static let description = L10n.tr("Localizable", "private.meeting.start.description")
        /// Set up a private meeting
        internal static let title = L10n.tr("Localizable", "private.meeting.start.title")
      }
    }
  }

  internal enum QrCodeGeneration {
    internal enum Failure {
      /// QR Code could not be generated: %@
      internal static func message(_ p1: Any) -> String {
        return L10n.tr("Localizable", "qrCodeGeneration.failure.message", String(describing: p1))
      }
    }
  }

  internal enum Terms {
    internal enum Acceptance {
      /// We are updating our terms of use and privacy policy.
      internal static let description = L10n.tr("Localizable", "terms.acceptance.description")
      /// https://www.luca-app.de/changes-terms-of-use-app/
      internal static let linkChanges = L10n.tr("Localizable", "terms.acceptance.linkChanges")
      /// Tap "AGREE" to accept the updated terms of use and privacy policy.
      internal static let linkDescription = L10n.tr("Localizable", "terms.acceptance.linkDescription")
      /// Changes
      internal static let termsAndConditionsChanges = L10n.tr("Localizable", "terms.acceptance.termsAndConditionsChanges")
    }
  }

  internal enum Test {
    internal enum Add {
      /// Add document
      internal static let title = L10n.tr("Localizable", "test.add.title")
    }
    internal enum BirthdateMismatch {
      internal enum Error {
        /// The document's date of birth does not match your other documents.
        internal static let description = L10n.tr("Localizable", "test.birthdateMismatch.error.description")
        /// Import
        internal static let ok = L10n.tr("Localizable", "test.birthdateMismatch.error.ok")
      }
    }
    internal enum Delete {
      /// Do you really want to delete this document?
      internal static let description = L10n.tr("Localizable", "test.delete.description")
      /// Delete document
      internal static let title = L10n.tr("Localizable", "test.delete.title")
    }
    internal enum Expiry {
      /// h
      internal static let hours = L10n.tr("Localizable", "test.expiry.hours")
      /// m
      internal static let minutes = L10n.tr("Localizable", "test.expiry.minutes")
    }
    internal enum Result {
      /// dd.MM.yyyy
      internal static let dateFormat = L10n.tr("Localizable", "test.result.dateFormat")
      /// Expiry: %@
      internal static func expiry(_ p1: Any) -> String {
        return L10n.tr("Localizable", "test.result.expiry", String(describing: p1))
      }
      /// Rapid test
      internal static let fast = L10n.tr("Localizable", "test.result.fast")
      /// Testresult: Negative
      internal static let negative = L10n.tr("Localizable", "test.result.negative")
      /// Other
      internal static let other = L10n.tr("Localizable", "test.result.other")
      /// PCR test
      internal static let pcr = L10n.tr("Localizable", "test.result.pcr")
      /// Testresult: Positive
      internal static let positive = L10n.tr("Localizable", "test.result.positive")
      /// Successful
      internal static let success = L10n.tr("Localizable", "test.result.success")
      internal enum Child {
        internal enum Age {
          /// Documents can only be added for your children if they are under the age of 14.
          internal static let error = L10n.tr("Localizable", "test.result.child.age.error")
        }
      }
      internal enum Delete {
        /// It was not possible to delete this test.
        internal static let error = L10n.tr("Localizable", "test.result.delete.error")
      }
      internal enum Duration {
        /// one hour
        internal static let hour = L10n.tr("Localizable", "test.result.duration.hour")
        /// %i hours
        internal static func hours(_ p1: Int) -> String {
          return L10n.tr("Localizable", "test.result.duration.hours", p1)
        }
        /// %i minutes
        internal static func minutes(_ p1: Int) -> String {
          return L10n.tr("Localizable", "test.result.duration.minutes", p1)
        }
        /// %i months
        internal static func months(_ p1: Int) -> String {
          return L10n.tr("Localizable", "test.result.duration.months", p1)
        }
      }
      internal enum Error {
        /// The Document could not be imported
        internal static let title = L10n.tr("Localizable", "test.result.error.title")
      }
      internal enum Expiration {
        /// Unfortunately, the document cannot be imported because it already expired.
        internal static let error = L10n.tr("Localizable", "test.result.expiration.error")
      }
      internal enum Future {
        /// This document's issue date is set in the future. You can only add documents that are valid at the moment.
        internal static let error = L10n.tr("Localizable", "test.result.future.error")
      }
      internal enum Name {
        internal enum Validation {
          /// It was not possible to validate your name. Please check if your name is entered correctly in the app.
          internal static let error = L10n.tr("Localizable", "test.result.name.validation.error")
        }
      }
      internal enum Parsing {
        /// Data couldn't be processed.
        internal static let error = L10n.tr("Localizable", "test.result.parsing.error")
        /// Your document was added successfully.
        internal static let success = L10n.tr("Localizable", "test.result.parsing.success")
      }
      internal enum Positive {
        /// luca helps you to check in easily. Since only negative test results are relevant for checking in, you can only add negative results at the moment. You can only add a positive result if it is a PCR test older than 14 days. 
        internal static let error = L10n.tr("Localizable", "test.result.positive.error")
        internal enum Error {
          /// Adding not possible
          internal static let title = L10n.tr("Localizable", "test.result.positive.error.title")
        }
      }
      internal enum Validation {
        /// It was not possible to validate that this document belongs to you or your children. Please check if the names are entered correctly in the app.
        internal static let error = L10n.tr("Localizable", "test.result.validation.error")
      }
      internal enum Verification {
        /// Unfortunately, the document cannot be imported because the signature is not valid.
        internal static let error = L10n.tr("Localizable", "test.result.verification.error")
      }
    }
    internal enum Scanner {
      /// QR code scanner
      internal static let camera = L10n.tr("Localizable", "test.scanner.camera")
      /// Close
      internal static let close = L10n.tr("Localizable", "test.scanner.close")
      /// Scan QR code
      internal static let title = L10n.tr("Localizable", "test.scanner.title")
    }
    internal enum Uniqueness {
      internal enum Create {
        internal enum RandomTag {
          /// Failed to create a random tag.
          internal static let error = L10n.tr("Localizable", "test.uniqueness.create.randomTag.error")
        }
      }
      internal enum Encoding {
        /// Failed to encode test data.
        internal static let error = L10n.tr("Localizable", "test.uniqueness.encoding.error")
      }
      internal enum Rate {
        internal enum Limit {
          /// The rate limit has been reached.
          internal static let error = L10n.tr("Localizable", "test.uniqueness.rate.limit.error")
        }
      }
      internal enum Redeemed {
        /// This document was already imported.
        internal static let error = L10n.tr("Localizable", "test.uniqueness.redeemed.error")
      }
      internal enum Release {
        /// Document could not be deleted.
        internal static let error = L10n.tr("Localizable", "test.uniqueness.release.error")
      }
    }
  }

  internal enum Tests {
    internal enum Uniqueness {
      internal enum Consent {
        /// I hereby declare my consent in accordance with Art. 9 (2) a) in conjunction with Art. 6 (1) 1 a) DSGVO (more: %@) to the processing of my data during the import of my COVID test, recovery or vaccination certificate in the luca app. The data included in the document can be matched to my first and last name in the luca app. I also allow the storage of a pseudonymized identifier on the luca server. This is solely to prevent misuse, so that a document cannot be used by different people. The document’s individual identifier is automatically deleted from the luca system after 72 hours of storage.
        internal static func description(_ p1: Any) -> String {
          return L10n.tr("Localizable", "tests.uniqueness.consent.description", String(describing: p1))
        }
        /// DSGVO
        internal static let title = L10n.tr("Localizable", "tests.uniqueness.consent.title")
        internal enum Title {
          /// D.S.G.V.O
          internal static let accessibility = L10n.tr("Localizable", "tests.uniqueness.consent.title.accessibility")
        }
      }
    }
  }

  internal enum Update {
    internal enum Screen {
      /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">Hooray! A lot has happened at luca.</p>\n                     <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">Swipe right to see what's new.</p>\n                     <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">No time right now? You can also find all the information in the "Account" tab.</p>
      internal static let _1 = L10n.tr("Localizable", "update.screen.1")
      /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">Extension of "My luca"</p>\n                     <p style="font-family: Montserrat-bold; font-size: bodySize; color: white">Add children</p>\n                     <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">You can now use luca for your children as well.</p>\n                     <p style="font-family: Montserrat-bold; font-size: bodySize; color: white">Important notifications</p>\n                     <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">You will receive any notifications or warnings here.</p>
      internal static let _2 = L10n.tr("Localizable", "update.screen.2")
      /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">Adding Children</p>\n                     <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">Add your children to check them in with you. You can even import documents for them, but make sure to input full and correct information.</p>
      internal static let _3 = L10n.tr("Localizable", "update.screen.3")
      /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">Optimization of warnings</p>\n                     <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">Our different warning levels can inform you in more detail, when you were checked in somewhere at the same time as an infected person. Affected events are marked in your history.</p>
      internal static let _4 = L10n.tr("Localizable", "update.screen.4")
      /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">Faster check-in</p>\n                     <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">When you press on "Check-in", luca now directly starts on the camera. Just activate the camera once and you're good to go. Access your personal QR code by clicking on the button below it.</p>
      internal static let _5 = L10n.tr("Localizable", "update.screen.5")
      /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">Relocation of your history</p>\n                     <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">You can now access your history via the clock icon in the top right corner of the "Check-in" tab.</p>\n                     <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">As always, your data cannot be viewed until you generate your TAN and share it with the health department.</p>
      internal static let _6 = L10n.tr("Localizable", "update.screen.6")
      /// <p style="font-family: Montserrat-bold; font-size: titleSize; color: white">That's it!</p>\n                     <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">You can find all info and even more explanation screens in the news section of the "Account" tab.</p>\n                     <p style="font-family: Montserrat-medium; font-size: bodySize; color: white">PS: Don't forget to check out!</p>
      internal static let _7 = L10n.tr("Localizable", "update.screen.7")
    }
  }

  internal enum UserData {
    /// You have given no address in your data yet. Please fill it before further usage.
    internal static let addressNotFilledMessage = L10n.tr("Localizable", "userData.addressNotFilledMessage")
    internal enum Address {
      /// Hosts usually have to collect the addresses of their guests. Therefore, in order to use luca, it's mandatory to provide your address.\n\nFields marked with * are mandatory.
      internal static let mandatory = L10n.tr("Localizable", "userData.address.mandatory")
    }
    internal enum Form {
      /// Please fill out all mandatory fields to continue
      internal static let accessibilityError = L10n.tr("Localizable", "userData.form.accessibilityError")
      /// * City
      internal static let city = L10n.tr("Localizable", "userData.form.city")
      /// Email
      internal static let email = L10n.tr("Localizable", "userData.form.email")
      /// * First name
      internal static let firstName = L10n.tr("Localizable", "userData.form.firstName")
      /// * Number
      internal static let houseNumber = L10n.tr("Localizable", "userData.form.houseNumber")
      /// * Last name
      internal static let lastName = L10n.tr("Localizable", "userData.form.lastName")
      /// * Phone number
      internal static let phoneNumber = L10n.tr("Localizable", "userData.form.phoneNumber")
      /// * Post code
      internal static let postCode = L10n.tr("Localizable", "userData.form.postCode")
      /// * Street
      internal static let street = L10n.tr("Localizable", "userData.form.street")
      internal enum Address {
        /// Where do you live?
        internal static let formTitle = L10n.tr("Localizable", "userData.form.address.formTitle")
      }
      internal enum City {
        /// Please enter your city to continue.
        internal static let accessibilityError = L10n.tr("Localizable", "userData.form.city.accessibilityError")
      }
      internal enum Field {
        /// This field is mandatory.
        internal static let error = L10n.tr("Localizable", "userData.form.field.error")
      }
      internal enum FirstName {
        /// Please enter your first name to continue.
        internal static let accessibilityError = L10n.tr("Localizable", "userData.form.firstName.accessibilityError")
      }
      internal enum HouseNumber {
        /// Please enter your house number to continue.
        internal static let accessibilityError = L10n.tr("Localizable", "userData.form.houseNumber.accessibilityError")
      }
      internal enum LastName {
        /// Please enter your last name to continue.
        internal static let accessibilityError = L10n.tr("Localizable", "userData.form.lastName.accessibilityError")
      }
      internal enum Name {
        /// What's your name?
        internal static let formTitle = L10n.tr("Localizable", "userData.form.name.formTitle")
      }
      internal enum Phone {
        /// How can you be contacted?
        internal static let formTitle = L10n.tr("Localizable", "userData.form.phone.formTitle")
      }
      internal enum PhoneNumber {
        /// Please enter your phone number to continue.
        internal static let accessibilityError = L10n.tr("Localizable", "userData.form.phoneNumber.accessibilityError")
      }
      internal enum PostCode {
        /// Please enter your zip code to continue.
        internal static let accessibilityError = L10n.tr("Localizable", "userData.form.postCode.accessibilityError")
      }
      internal enum Street {
        /// Please enter your street to continue.
        internal static let accessibilityError = L10n.tr("Localizable", "userData.form.street.accessibilityError")
      }
    }
    internal enum Name {
      /// Fields marked with * are mandatory.
      internal static let mandatory = L10n.tr("Localizable", "userData.name.mandatory")
    }
    internal enum Navigation {
      /// Edit contact data
      internal static let edit = L10n.tr("Localizable", "userData.navigation.edit")
      /// Contact Data
      internal static let title = L10n.tr("Localizable", "userData.navigation.title")
    }
    internal enum Phone {
      /// Please provide a correct mobile or landline number. This number will be used for verification. You can choose if you want to provide an email address.\n\nFields marked with * are mandatory.
      internal static let mandatory = L10n.tr("Localizable", "userData.phone.mandatory")
    }
  }

  internal enum Vaccine {
    internal enum Result {
      /// Complete vaccination (%i/%i)
      internal static func complete(_ p1: Int, _ p2: Int) -> String {
        return L10n.tr("Localizable", "vaccine.result.complete", p1, p2)
      }
      /// Complete vaccination in %i days
      internal static func completeInDays(_ p1: Int) -> String {
        return L10n.tr("Localizable", "vaccine.result.complete_in_days", p1)
      }
      /// BioNTech/Pfizer
      internal static let cormirnaty = L10n.tr("Localizable", "vaccine.result.cormirnaty")
      /// Vaccination
      internal static let `default` = L10n.tr("Localizable", "vaccine.result.default")
      /// Vaccination:
      internal static let description = L10n.tr("Localizable", "vaccine.result.description")
      /// Johnson & Johnson
      internal static let janssen = L10n.tr("Localizable", "vaccine.result.janssen")
      /// Moderna
      internal static let moderna = L10n.tr("Localizable", "vaccine.result.moderna")
      /// Partial vaccination (%i/%i)
      internal static func partially(_ p1: Int, _ p2: Int) -> String {
        return L10n.tr("Localizable", "vaccine.result.partially", p1, p2)
      }
      /// (%i/%i)
      internal static func partiallyShort(_ p1: Int, _ p2: Int) -> String {
        return L10n.tr("Localizable", "vaccine.result.partially_short", p1, p2)
      }
      /// Sputnik V
      internal static let sputnikV = L10n.tr("Localizable", "vaccine.result.sputnikV")
      /// AstraZeneca
      internal static let vaxzevria = L10n.tr("Localizable", "vaccine.result.vaxzevria")
    }
  }

  internal enum Verification {
    internal enum PhoneNumber {
      /// TAN
      internal static let code = L10n.tr("Localizable", "verification.phoneNumber.code")
      /// The TAN you entered was incorrect.
      internal static let failureMessage = L10n.tr("Localizable", "verification.phoneNumber.failureMessage")
      /// You have not verified your phone number yet. Please resave your contact data and verify your phone number.
      internal static let notYetVerified = L10n.tr("Localizable", "verification.phoneNumber.notYetVerified")
      /// No SMS could be sent out.
      internal static let requestFailure = L10n.tr("Localizable", "verification.phoneNumber.requestFailure")
      /// The verification was succesful.
      internal static let successMessage = L10n.tr("Localizable", "verification.phoneNumber.successMessage")
      /// Successful verification
      internal static let successTitle = L10n.tr("Localizable", "verification.phoneNumber.successTitle")
      /// Your data could not be saved.
      internal static let updateFailure = L10n.tr("Localizable", "verification.phoneNumber.updateFailure")
      /// verified
      internal static let verified = L10n.tr("Localizable", "verification.phoneNumber.verified")
      /// The given phone number is not in the right format. Please enter a valid phone number and its country code (e.g. +49xxxxxxxxxxx).
      internal static let wrongFormat = L10n.tr("Localizable", "verification.phoneNumber.wrongFormat")
      internal enum Info {
        /// It can take a few minutes for the TAN to arrive. Please wait a moment before you request another TAN. If nothing happens after a few minutes, please check if you've provided the correct number and try again.
        internal static let message = L10n.tr("Localizable", "verification.phoneNumber.info.message")
        /// Info
        internal static let title = L10n.tr("Localizable", "verification.phoneNumber.info.title")
      }
      internal enum LimitReached {
        /// Your daily SMS limit has been reached, please try again later.
        internal static let message = L10n.tr("Localizable", "verification.phoneNumber.limitReached.message")
        /// Rate limit exceeded
        internal static let title = L10n.tr("Localizable", "verification.phoneNumber.limitReached.title")
      }
      internal enum TimerDelay {
        /// You have recently requested a TAN. Please wait a moment. If nothing happens, you can try again in:\n\n%@
        internal static func message(_ p1: Any) -> String {
          return L10n.tr("Localizable", "verification.phoneNumber.timerDelay.message", String(describing: p1))
        }
        /// TAN already requested
        internal static let title = L10n.tr("Localizable", "verification.phoneNumber.timerDelay.title")
        internal enum Message {
          /// You have recently requested a TAN. Please wait a moment. If nothing happens, you can try again in:\n\n%@ minutes, %@ seconds
          internal static func accessibility(_ p1: Any, _ p2: Any) -> String {
            return L10n.tr("Localizable", "verification.phoneNumber.timerDelay.message.accessibility", String(describing: p1), String(describing: p2))
          }
        }
      }
    }
  }

  internal enum VersionSupportChecker {
    /// Your luca version is not supported anymore! Please update your luca app in the AppStore.
    internal static let failureMessage = L10n.tr("Localizable", "versionSupportChecker.failureMessage")
  }

  internal enum Welcome {
    internal enum Checkboxes {
      /// Please accept the terms of use to continue.
      internal static let accessibilityError = L10n.tr("Localizable", "welcome.checkboxes.accessibilityError")
    }
    internal enum Info {
      /// luca helps you encrypt and securely submit your contact data. With luca, you don't have to worry about your data when visiting events, restaurants, cafés or bars anymore.
      internal static let description = L10n.tr("Localizable", "welcome.info.description")
      /// Hello
      internal static let hello = L10n.tr("Localizable", "welcome.info.hello")
      /// GET STARTED!
      internal static let okButton = L10n.tr("Localizable", "welcome.info.okButton")
    }
  }

  internal enum WelcomeViewController {
    /// GitLab
    internal static let gitLab = L10n.tr("Localizable", "welcomeViewController.gitLab")
    /// https://www.luca-app.de/faq/
    internal static let linkFAQ = L10n.tr("Localizable", "welcomeViewController.linkFAQ")
    /// https://gitlab.com/lucaapp/ios
    internal static let linkGitLab = L10n.tr("Localizable", "welcomeViewController.linkGitLab")
    /// https://luca-app.de/app-privacy-policy/
    internal static let linkPrivacyPolicy = L10n.tr("Localizable", "welcomeViewController.linkPrivacyPolicy")
    /// https://luca-app.de/app-terms-and-conditions
    internal static let linkTC = L10n.tr("Localizable", "welcomeViewController.linkT_C")
    /// privacy policy
    internal static let termPrivacyPolicy = L10n.tr("Localizable", "welcomeViewController.termPrivacyPolicy")
    /// terms of use
    internal static let termTC = L10n.tr("Localizable", "welcomeViewController.termT_C")
    internal enum PrivacyPolicy {
      /// Privacy Policy Checkbox
      internal static let checkboxAccessibility = L10n.tr("Localizable", "welcomeViewController.privacyPolicy.checkboxAccessibility")
      /// You can view our privacy policy under this link.
      internal static let message = L10n.tr("Localizable", "welcomeViewController.privacyPolicy.message")
      internal enum Checkbox {
        /// Confirmed: I have read and agree to the privacy policy. Double tap to unconfirm.
        internal static let confirmed = L10n.tr("Localizable", "welcomeViewController.privacyPolicy.checkbox.confirmed")
        /// Not confirmed: I have read and agree to the privacy policy. Double tap to confirm.
        internal static let notConfirmed = L10n.tr("Localizable", "welcomeViewController.privacyPolicy.checkbox.notConfirmed")
      }
    }
    internal enum TermsAndConditions {
      /// Terms of Use Checkbox
      internal static let checkboxAccessibility = L10n.tr("Localizable", "welcomeViewController.termsAndConditions.checkboxAccessibility")
      /// Not confirmed
      internal static let checkboxAccessibilityOff = L10n.tr("Localizable", "welcomeViewController.termsAndConditions.checkboxAccessibilityOff")
      /// Confirmed
      internal static let checkboxAccessibilityOn = L10n.tr("Localizable", "welcomeViewController.termsAndConditions.checkboxAccessibilityOn")
      /// I accept the terms of use.
      internal static let checkboxMessage = L10n.tr("Localizable", "welcomeViewController.termsAndConditions.checkboxMessage")
      internal enum Checkbox {
        /// Confirmed: I accept the terms of use. Double tap to unconfirm.
        internal static let confirmed = L10n.tr("Localizable", "welcomeViewController.termsAndConditions.checkbox.confirmed")
        /// Not confirmed: I accept the terms of use. Double tap to confirm.
        internal static let notConfirmed = L10n.tr("Localizable", "welcomeViewController.termsAndConditions.checkbox.notConfirmed")
      }
    }
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: nil, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
