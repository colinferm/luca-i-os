import UIKit
import DependencyInjection

public enum FormComponentType {

    case firstName
    case lastName
    case street
    case houseNumber
    case postCode
    case city
    case phoneNumber
    case email

    var placeholder: String {
        switch self {
        case .firstName:    return L10n.UserData.Form.firstName
        case .lastName:     return L10n.UserData.Form.lastName
        case .street:       return L10n.UserData.Form.street
        case .houseNumber:  return L10n.UserData.Form.houseNumber
        case .postCode:     return L10n.UserData.Form.postCode
        case .city:         return L10n.UserData.Form.city
        case .phoneNumber:  return L10n.UserData.Form.phoneNumber
        case .email:        return L10n.UserData.Form.email
        }
    }

    func value(_ preferences: LucaPreferences) -> String? {
        switch self {
        case .firstName:    return preferences.blocking.get(\.firstName) ?? nil
        case .lastName:     return preferences.blocking.get(\.lastName) ?? nil
        case .street:       return preferences.blocking.get(\.street) ?? nil
        case .houseNumber:  return preferences.blocking.get(\.houseNumber) ?? nil
        case .postCode:     return preferences.blocking.get(\.postCode) ?? nil
        case .city:         return preferences.blocking.get(\.city) ?? nil
        case .phoneNumber:  return preferences.blocking.get(\.phoneNumber) ?? nil
        case .email:        return preferences.blocking.get(\.email) ?? nil
        }
    }

    var keyboardType: UIKeyboardType {
        print("Keyboard type for: \(self)")
        switch self {
        case .phoneNumber:  return .phonePad
        case .postCode:     return .webSearch
        case .email:        return .emailAddress
        case .lastName,
             .firstName:    return .webSearch
        default:            return .webSearch
        }
    }

    var textContentType: UITextContentType {
        switch self {
        case .firstName:    return .givenName
        case .lastName:     return .familyName
        case .street:       return .streetAddressLine1
        case .houseNumber:  return .streetAddressLine2
        case .postCode:     return .location  // .postalCode has to many restrictions e.g. letters for zip codes abroad
        case .city:         return .addressCity
        case .phoneNumber:  return .telephoneNumber
        case .email:        return .emailAddress
        }
    }

    var accessibilityError: String? {
        switch self {
        case .email:        return nil
        default:            return L10n.UserData.Form.Field.error
        }
    }

}
