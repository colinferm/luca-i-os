import UIKit
import DependencyInjection

class DataPrivacyViewController: UIViewController {
    @InjectStatic(\.lucaPreferences) private var lucaPreferences

    @IBOutlet weak var descriptionTitle: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var gotItButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }

    func setupViews() {
        setupAccessibility()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        gotItButton.layer.cornerRadius = gotItButton.frame.size.height / 2
    }

    @IBAction func gotItButtonPressed(_ sender: UIButton) {
        lucaPreferences.blocking.set(\.dataPrivacyPresented, value: true)
        self.dismiss(animated: true, completion: nil)
    }

}

// MARK: - Accessibility
extension DataPrivacyViewController {

    private func setupAccessibility() {
        descriptionTitle.accessibilityTraits = .header
        UIAccessibility.setFocusTo(descriptionTitle, notification: .layoutChanged, delay: 0.8)
    }

}
