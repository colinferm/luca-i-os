import UIKit
import MaterialComponents.MaterialTextFields
import JGProgressHUD
import DependencyInjection
import RxSwift

class FormViewController: UIViewController, HandlesLucaErrors {

    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.userService) private var userService

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var formView: FormView!
    @IBOutlet weak var progressBar: UIProgressView!

    private var progressHud = JGProgressHUD.lucaLoading()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadViews()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        nextButton.layer.cornerRadius = nextButton.frame.size.height / 2
        for field in formView.textFields {
            var fieldFrame = field.frame
            fieldFrame.size.width = formView.frame.size.width
            field.frame = fieldFrame
        }
    }

    func reloadViews() {
        let step = OnboardingStep(rawValue: lucaPreferences.blocking.get(\.currentOnboardingPage))!
        formView.setup(step: step)
        _ = formView.textFields.map { [weak self] in $0.textField.delegate = self }
        nextButton.setTitle(step.buttonTitle, for: .normal)
        titleLabel.text = step.formTitle
        progressBar.progress = step.progress
        setupAccessibility()
    }

    @IBAction func nextButtonPressed(_ sender: UIButton) {
        showNextPage()
    }

    @IBAction func rightSwiped(_ sender: UISwipeGestureRecognizer) {
        showPreviousPage()
    }

    @IBAction func leftSwiped(_ sender: UISwipeGestureRecognizer) {
        showNextPage()
    }

    func showNextPage() {
        self.hideKeyboard()
        let page = lucaPreferences.blocking.get(\.currentOnboardingPage)
        guard let pageEnum = OnboardingStep(rawValue: page) else {
            print("No eligible current page!")
            return
        }
        if formView.textFieldsFilledOut {
            UIAccessibility.setFocusTo(titleLabel, notification: .screenChanged, delay: 0.8)
            print("Current page: \(pageEnum) \(String(describing: lucaPreferences.blocking.get(\.email)))")
            if pageEnum == .phoneNumber {
                if lucaPreferences.blocking.get(\.phoneNumberVerified) {
                    lucaPreferences.blocking.set(\.currentOnboardingPage, value: page + 1)
                    reloadViews()
                } else {
                    verifyPhoneNumber()
                }
            } else if page + 1 <= 2 {
                lucaPreferences.blocking.set(\.currentOnboardingPage, value: page + 1)
                reloadViews()
            } else {
                registerUser()
            }
        } else {
            formView.showErrorStatesForEmptyFields()
        }

    }

    func verifyPhoneNumber() {
        let phoneNumberVerificationService = PhoneNumberVerificationService(presenting: self)

        guard let phoneNumber = lucaPreferences.blocking.get(\.phoneNumber) as? String else {
            return
        }

        phoneNumberVerificationService.verify(phoneNumber: phoneNumber)
            .subscribe(onCompleted: { [weak self] in
                            DispatchQueue.main.async { self?.showNextPage() }
                        }, onError: { [weak self] error in
                            DispatchQueue.main.async {
                                self?.processErrorMessages(error: error, completion: nil)
                            }
                        })
    }

    func showPreviousPage() {
        let page = lucaPreferences.blocking.get(\.currentOnboardingPage)
           if page - 1 >= 0 {
               lucaPreferences.blocking.set(\.currentOnboardingPage, value: page - 1)
            reloadViews()
        }
    }

    func registerUser() {
        guard lucaPreferences.blocking.get(\.userRegistrationData) != nil else {
            let alert = UIAlertController.infoAlert(title: L10n.Navigation.Basic.error,
                                                    message: L10n.FormViewController.UserDataFailure.Unavailable.message)
            self.present(alert, animated: true, completion: nil)
            return
        }

        progressBar.progress = 1.0
        progressHud.show(in: self.view)
        _ = userService.registerIfNeeded()
            .do(onSuccess: { (_) in
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: nil)
                }
            })
            .do(onError: { (error) in
                DispatchQueue.main.async {
                    self.processErrorMessages(error: error)
                }
            })
            .do(onDispose: {
                DispatchQueue.main.async {
                    self.progressHud.dismiss()
                }
            })
            .subscribe()
    }

    @IBAction func viewTapped(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }

}
extension FormViewController: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {
        formView.showNormalStatesForEmptyFields()
    }

    func textFieldDidEndEditing(_ textField: UITextField) {

        guard let formTextField = self.formView.textFields.first(where: { $0.textField == textField }),
              let type = formTextField.type,
              let text = textField.text?.sanitize() else {
            return
        }
        switch type {
        case .firstName:    lucaPreferences.blocking.set(\.firstName, value: text)
        case .lastName:     lucaPreferences.blocking.set(\.lastName, value: text)
        case .street:       lucaPreferences.blocking.set(\.street, value: text)
        case .houseNumber:  lucaPreferences.blocking.set(\.houseNumber, value: text)
        case .postCode:     lucaPreferences.blocking.set(\.postCode, value: text)
        case .city:         lucaPreferences.blocking.set(\.city, value: text)
        case .email:        lucaPreferences.blocking.set(\.email, value: text)
        case .phoneNumber:
            lucaPreferences.blocking.set(\.phoneNumber, value: text)
            lucaPreferences.blocking.set(\.phoneNumberVerified, value: false)
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        var tag = 0
        // Find first responder manually, since textField returns a tag of 0 for every textfield since we use FormTextField
        for view in formView.textFields where view.textField.isFirstResponder {
            tag = view.tag
        }

        if tag == formView.textFields.count - 1 {
            showNextPage()
        } else if let nextField = view.superview?.viewWithTag(textField.tag + 1) as? FormTextField {
            nextField.textField.becomeFirstResponder()
        }
        return false
    }

}

// MARK: - Accessibility
extension FormViewController {

    private func setupAccessibility() {
        titleLabel.accessibilityTraits = .header
        UIAccessibility.setFocusTo(titleLabel, notification: .layoutChanged)
    }

}
