import UIKit
import RxSwift
import JGProgressHUD
import DependencyInjection

public class DeleteAccountCoordinator: NSObject, Coordinator {

    @InjectStatic(\.dataResetService) private var dataResetService
    @InjectStatic(\.documentRepoService) private var documentRepoService
    @InjectStatic(\.documentUniquenessChecker) private var documentUniquenessChecker
    @InjectStatic(\.userService) private var userService

    private weak var presenter: UIViewController?

    private let progressHUD = JGProgressHUD.lucaLoading()

    public init(presenter: UIViewController) {
        self.presenter = presenter
    }

    public func start() {
        guard let presenter = self.presenter else { return }
        UIAlertController(title: L10n.Data.ResetData.title,
                          message: L10n.Data.ResetData.description,
                          preferredStyle: .alert)
            .actionAndCancelAlert(actionText: L10n.Data.ResetData.title, action: {
                self.deleteUser()
            }, viewController: presenter)
    }

    private func deleteUser() {
        if let view = presenter?.view {
            progressHUD.show(in: view)
        }
        _ = documentRepoService
            .currentAndNewTests
            .take(1)
            .asSingle()
            .flatMapCompletable { docs in
                self.documentUniquenessChecker.release(documents: docs)
            }
            .andThen(deleteUserData())
            .observe(on: MainScheduler.instance)
            .do(onError: { [weak self] error in
                if let localizedTitledError = error as? LocalizedTitledError {
                    let alert = UIAlertController.infoAlert(
                        title: localizedTitledError.localizedTitle,
                        message: localizedTitledError.localizedDescription
                    )
                    self?.presenter?.present(alert, animated: true, completion: nil)
                } else {
                    let alert = UIAlertController.infoAlert(
                        title: L10n.Navigation.Basic.error,
                        message: error.localizedDescription
                    )
                    self?.presenter?.present(alert, animated: true, completion: nil)
                }
            })
            .do(onDispose: { [weak self] in
                self?.progressHUD.dismiss()
            })
            .subscribe()
    }

    private func deleteUserData() -> Completable {
        userService.deleteUserData()
            .subscribe(on: MainScheduler.instance)
            .catch({ error in

                // In case account was already deleted, or account doesn't exist, reset app and begin onboarding.
                if let deleteError = error as? UserServiceError,
                   case let UserServiceError.userDeletionError(error: backendError) = deleteError,
                   backendError.backendError == .alreadyDeleted || backendError.backendError == .userNotFound {
                    return Completable.empty()
                }

                throw error
            })
            .andThen(dataResetService.resetAll())
            .andThen(
                Completable.from { self.presenter?.dismiss(animated: true, completion: nil) }
                    .subscribe(on: MainScheduler.asyncInstance)
            )
    }
}
