import Foundation
import RxSwift
import UIKit
import DependencyInjection

public class AccountDataReportCoordinator: NSObject, Coordinator {

    @InjectStatic(\.contactDataReporter) private var contactDataReporter
    @InjectStatic(\.documentsReporter) private var documentsReporter

    private weak var presenter: UIViewController?
    var disposeBag = DisposeBag()

    public init(presenter: UIViewController) {
        self.presenter = presenter
    }

    private func shareReport(report: Single<String>) {
        report
            .observe(on: MainScheduler.instance)
            .map { [weak self] report in
                let activityViewController = UIActivityViewController(activityItems: [report], applicationActivities: nil)
                self?.presenter?.present(activityViewController, animated: true, completion: nil)
            }
            .subscribe()
            .disposed(by: disposeBag)
    }

    public func start() {
        guard let presenter = presenter else { return }
        let alert = UIAlertController(title: L10n.DataReport.AccountSettings.title, message: nil, preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: L10n.DataReport.AccountSettings.contactReport, style: .default, handler: { _ in
            self.shareReport(report: self.contactDataReporter.createReport())
        }))

        alert.addAction(UIAlertAction(title: L10n.DataReport.AccountSettings.certificatesReport, style: .default, handler: { _ in
            self.shareReport(report: self.documentsReporter.createReport())
        }))

        alert.addAction(UIAlertAction(title: L10n.Navigation.Basic.cancel, style: .cancel))

        presenter.present(alert, animated: true)
    }
}
