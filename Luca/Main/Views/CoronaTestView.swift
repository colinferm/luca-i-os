import UIKit

class CoronaTestView: DocumentView, DocumentViewProtocol {

    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var durationSinceLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var labLabel: UILabel!
    @IBOutlet weak var doctorLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var qrCodeImageView: UIImageView!
    @IBOutlet weak var providerLabel: UILabel!

    @IBOutlet weak var wrapperView: UIView!
    @IBOutlet weak var expandView: UIView!

    weak var timer: Timer?

    var indexPath: IndexPath?

    var document: CoronaTest? {
        didSet {
            setup()
        }
    }

    deinit {
        stopDateUpdateTimer()
    }

    public static func createView(document: Document, isExpanded: Bool, delegate: DocumentViewDelegate?) -> DocumentView? {
        guard let document = document as? CoronaTest else { return nil }

        let itemView: CoronaTestView = CoronaTestView.fromNib()
        itemView.delegate = delegate
        itemView.isExpanded = isExpanded
        itemView.document = document

        return itemView
    }

    private func setup() {
        guard let test = document else { return }

        resultLabel.text = test.isNegative ? L10n.Test.Result.negative : L10n.Test.Result.positive

        wrapperView.layer.cornerRadius = 8
        wrapperView.backgroundColor = test.isNegative ? Asset.lucaHealthGreen.color : UIColor.white

        categoryLabel.text = test.testType.localized

        durationSinceLabel.text = test.issuedAt.durationSinceDate
        dateLabel.text = test.issuedAt.formattedDateTime
        dateLabel.accessibilityLabel = test.issuedAt.accessibilityDate
        labLabel.text = test.laboratory.replacingOccurrences(of: "\\s[\\s]+", with: "\n", options: .regularExpression, range: nil)
        doctorLabel.text = test.doctor

        qrCodeImageView.layer.cornerRadius = 8
        setupQRCodeImage(for: test)

        qrCodeImageView.isAccessibilityElement = true
        qrCodeImageView.accessibilityLabel = L10n.Contact.Qr.Accessibility.qrCode

        deleteButton.addTarget(self, action: #selector(didPressDelete(sender:)), for: .touchUpInside)
        deleteButton.layer.borderWidth = 1
        deleteButton.layer.borderColor = UIColor.black.cgColor
        deleteButton.layer.cornerRadius = 16

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped(_:)))
        tapGestureRecognizer.numberOfTapsRequired = 1
        tapGestureRecognizer.isEnabled = true
        tapGestureRecognizer.cancelsTouchesInView = true
        addGestureRecognizer(tapGestureRecognizer)
        providerLabel.text = test.provider

        toggleView()
        startDateUpdateTimer()

        position = .single
    }

    private func setupQRCodeImage(for test: CoronaTest) {
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let image = QRCodeGenerator.generateQRCode(string: test.originalCode)
        if let scaledQr = image?.transformed(by: transform) {
            qrCodeImageView.image = UIImage(ciImage: scaledQr)
        }
    }

    // MARK: - DocumentViewProtocol

    func toggleView() {
        expandView.isHidden = !self.isExpanded
    }

    func startDateUpdateTimer() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 60.0, repeats: true) { [weak self] _ in
            self?.durationSinceLabel.text = self?.document?.issuedAt.durationSinceDate
        }
    }

    func stopDateUpdateTimer() {
        timer?.invalidate()
    }
}

// MARK: - Actions

extension CoronaTestView {
    @objc
    private func viewTapped(_ sender: UITapGestureRecognizer) {
        guard let document = document else { return }
        delegate?.didSelect(document: document)
    }

    @objc
    private func didPressDelete(sender: UIButton) {
        if let test = self.document {
            delegate?.didTapDelete(for: test)
        }
    }
}
