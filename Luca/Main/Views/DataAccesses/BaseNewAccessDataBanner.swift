import UIKit
import RxSwift
import RxCocoa
import LucaUIComponents

@IBDesignable class BaseNewAccessDataBanner: UIView {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var textLabel: UILabel!

    private var publisher = PublishSubject<Void>()
    var onShowButton: Driver<Void> {
        publisher.asDriver(onErrorJustReturn: Void())
    }
    var text: String = "" {
        didSet {
            textLabel?.text = text
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        nibSetup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        nibSetup()
    }

    private func nibSetup() {
        backgroundColor = .clear

        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true

        addSubview(view)
    }

    private func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        // swiftlint:disable:next force_cast
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView

        return nibView
    }

    @IBAction func onButton(_ sender: UIButton) {
        publisher.onNext(Void())
    }

    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        applyToSubviews(theme: .light)
    }
}

extension BaseNewAccessDataBanner: Themable {
    var theme: AppearanceTheme { .light }
}
