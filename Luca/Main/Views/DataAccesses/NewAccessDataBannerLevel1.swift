import UIKit
import RxSwift

@IBDesignable class NewAccessDataBannerLevel1: BaseNewAccessDataBanner {
    @IBOutlet weak var eyeIcon: UIImageView!

    override func layoutSubviews() {
        super.layoutSubviews()

        // iOS 12 fix. Setting this in Interface Builder doesn't work
        eyeIcon.image = Asset.eye.image.withRenderingMode(.alwaysTemplate)
        eyeIcon.tintColor = .black
    }
}
