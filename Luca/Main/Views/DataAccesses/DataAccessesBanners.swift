import UIKit
import RxSwift
import RxCocoa
import DependencyInjection

@IBDesignable
class DataAccessesBanners: UIView {

    @InjectStatic(\.accessedTraceIdRepo) private var accessedTraceIdRepo
    @InjectStatic(\.notificationConfigCachedDataSource) private var notificationConfigCachedDataSource
    @InjectStatic(\.accessTraceIdChecker) private var accessTraceIdChecker

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var stackView: UIStackView!

    private var warningLevel1View: BaseNewAccessDataBanner!
    private var warningLevel2View: BaseNewAccessDataBanner!
    private var warningLevel3View: BaseNewAccessDataBanner!
    private var warningLevel4View: BaseNewAccessDataBanner!

    private var disposeBag: DisposeBag!

    private var warningLevelViews: [UInt8: UIView] = [:]

    var onShowWarningLevel: Driver<UInt8> {
        Driver.merge(
            warningLevel1View.onShowButton.map { _ in 1 },
            warningLevel2View.onShowButton.map { _ in 2 },
            warningLevel3View.onShowButton.map { _ in 3 },
            warningLevel4View.onShowButton.map { _ in 4 }
        )
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        nibSetup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        nibSetup()
    }

    private func nibSetup() {
        backgroundColor = .clear

        view = loadViewFromNib()

        addSubview(view)

        view.translatesAutoresizingMaskIntoConstraints = false
        view.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        view.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        view.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0).isActive = true
        view.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0).isActive = true

        warningLevel1View = NewAccessDataBannerLevel1()
        warningLevel2View = NewAccessDataBannerLevel2()
        warningLevel3View = NewAccessDataBannerLevel2()
        warningLevel4View = NewAccessDataBannerLevel2()

        warningLevelViews[1] = warningLevel1View
        warningLevelViews[2] = warningLevel2View
        warningLevelViews[3] = warningLevel3View
        warningLevelViews[4] = warningLevel4View
        self.isHidden = true
    }

    private func loadData() -> Completable {
        accessedTraceIdRepo.restore()
            .map { data -> [AccessedTraceId] in
                data.filter { !$0.isRead }
            }
            .observe(on: MainScheduler.instance)
            .do(onSuccess: { if !$0.isEmpty { self.isHidden = false } })
            .delay(.milliseconds(50), scheduler: MainScheduler.instance)
            .do(onSuccess: { data in
                for entry in self.warningLevelViews {
                    if data.contains(where: { $0.warningLevel == entry.key }) {
                        self.stackView.addArrangedSubview(entry.value, animated: true)
                    } else {
                        self.stackView.removeArrangedSubview(entry.value, animated: true)
                    }
                }
            })
            .delay(.milliseconds(100), scheduler: MainScheduler.instance)
            .do(onSuccess: { if $0.isEmpty { self.isHidden = true } })
            .asCompletable()
    }

    /// Reloads data and refreshes the view
    /// - Parameter useOnlyCachedData: set it to `true` to prevent loading content from internet and to load data from the database.
    public func fetch(useOnlyCachedData: Bool) -> Completable {

        let dataLoading: Completable
        if useOnlyCachedData {
            dataLoading = loadData()
        } else {
            dataLoading = accessTraceIdChecker.fetch().asCompletable().andThen(loadData())
        }

        return loadConfig().andThen(dataLoading)
    }

    private func loadConfig() -> Completable {
        if warningLevel1View.text != "" {
            return Completable.empty()
        }
        return notificationConfigCachedDataSource
            .retrieve()
            .compactMap { $0.first }
            .observe(on: MainScheduler.instance)
            .do(onNext: { config in
                if let msg = config.default.retrieveMessages(for: 1),
                   let text = msg.banner {
                    self.warningLevel1View.text = text
                }
                if let msg = config.default.retrieveMessages(for: 2),
                   let text = msg.banner {
                    self.warningLevel2View.text = text
                }
                if let msg = config.default.retrieveMessages(for: 3),
                   let text = msg.banner {
                    self.warningLevel3View.text = text
                }
                if let msg = config.default.retrieveMessages(for: 4),
                   let text = msg.banner {
                    self.warningLevel4View.text = text
                }
            })
            .asObservable()
            .ignoreElementsAsCompletable()

    }

    private func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        // swiftlint:disable:next force_cast
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView

        return nibView
    }
}
