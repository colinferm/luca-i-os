import UIKit
import LucaUIComponents

class DocumentGroupView: UIView {

    // MARK: - UI elements

    lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.spacing = 0

        return stackView
    }()

    lazy var titleStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.spacing = 16

        return stackView
    }()

    // MARK: - Properties

    lazy var titleLabel: Luca16PtLabel = {
        let label = Luca16PtLabel(frame: CGRect.zero)

        return label
    }()

    lazy var childrenIconContainerView: UIView = {
        return UIView()
    }()

    lazy var childrenIconView: UIImageView = {
        let imageView = UIImageView(image: Asset.personWhite.image)

        return imageView
    }()

    var documentViewList: [DocumentView]? {
        didSet {
            setupStackView()
        }
    }

    var isChildren: Bool
    var title: String

    init(title: String, isChildren: Bool, views: [DocumentView]) {
        self.title = title
        self.isChildren = isChildren

        super.init(frame: CGRect.zero)

        setupUI()
        setupConstraints()

        self.documentViewList = views

        setupStackView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Setup

extension DocumentGroupView {
    private func setupUI() {
        clipsToBounds = true
        backgroundColor = .clear

        titleLabel.text = title

        childrenIconContainerView.addSubview(childrenIconView)
        titleStackView.addArrangedSubview(childrenIconContainerView)
        titleStackView.addArrangedSubview(titleLabel)
        addSubview(titleStackView)
        addSubview(stackView)
    }

    private func setupStackView() {
        stackView.removeAllArrangedSubviews()

        childrenIconContainerView.isHidden = !isChildren

        guard let documentViewList = documentViewList else { return }
        DocumentViewFactory.group(views: documentViewList).forEach { stackView.addArrangedSubview($0) }
    }

    private func setupConstraints() {
        titleStackView.translatesAutoresizingMaskIntoConstraints = false
        titleStackView.topAnchor.constraint(equalTo: topAnchor, constant: 8).isActive = true
        titleStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        titleStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 16).isActive = true

        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.topAnchor.constraint(equalTo: titleStackView.bottomAnchor, constant: 0).isActive = true
        stackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0).isActive = true
        stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0).isActive = true

        childrenIconContainerView.widthAnchor.constraint(equalToConstant: 16).isActive = true

        childrenIconView.translatesAutoresizingMaskIntoConstraints = false
        childrenIconView.widthAnchor.constraint(equalToConstant: 16).isActive = true
        childrenIconView.heightAnchor.constraint(equalToConstant: 16).isActive = true
        childrenIconView.centerYAnchor.constraint(equalTo: childrenIconContainerView.centerYAnchor).isActive = true
        childrenIconView.centerXAnchor.constraint(equalTo: childrenIconContainerView.centerXAnchor).isActive = true
    }
}
