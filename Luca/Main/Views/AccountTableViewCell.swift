import UIKit
import LucaUIComponents

class AccountTableViewCell: UITableViewCell, Identifiable {

    private lazy var titleLabel: UILabel = {
        let titleLabel = Luca16PtBoldLabel()
        titleLabel.textColor = .white
        titleLabel.numberOfLines = 0
        addSubview(titleLabel)

        return titleLabel
    }()

    func configureTitle(_ title: String) {
        titleLabel.text = title
        backgroundColor = Asset.lucaWhiteLowAlpha.color

        setupConstraints()
    }

    private func setupConstraints() {
        titleLabel.setAnchor(top: topAnchor,
                             leading: leadingAnchor,
                             bottom: bottomAnchor,
                             trailing: trailingAnchor,
                             padding: .init(top: 16, left: 16, bottom: 16, right: 16))
    }
}
