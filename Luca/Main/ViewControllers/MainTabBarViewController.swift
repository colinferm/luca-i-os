import UIKit
import RxSwift
import JGProgressHUD
import Alamofire
import DependencyInjection

enum TabBarIndex: Int {
    case myLuca = 0
    case checkin = 1
    case account = 2
}

class MainTabBarViewController: UITabBarController, HandlesLucaErrors {

    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.documentProcessingService) private var documentProcessingService
    @InjectStatic(\.documentRepoService) private var documentRepoService
    @InjectStatic(\.personRepo) private var personRepo
    @InjectStatic(\.traceIdService) private var traceIdService
    @InjectStatic(\.userService) private var userService
    @InjectStatic(\.accessTraceIdChecker) private var accessTraceIdChecker
    @InjectStatic(\.privateMeetingService) private var privateMeetingService
    @InjectStatic(\.selfCheckin) private var selfCheckin

    private var disposeBag = DisposeBag()

    private var progressHud = JGProgressHUD.lucaLoading()

    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self

        tabBar.barTintColor = .black
        tabBar.backgroundColor = .black
        tabBar.tintColor = .white
        tabBar.isTranslucent = false

        let borderView = UIView(frame: CGRect(x: 0, y: 0, width: tabBar.frame.size.width, height: 1))
        borderView.backgroundColor = Asset.lucaGrey.color
        tabBar.addSubview(borderView)

        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        if let tabBarItems = tabBar.items {
            tabBar.selectionIndicatorImage = UIImage().createTabBarSelectionIndicator(tabSize: CGSize(width: tabBar.frame.width/CGFloat(tabBarItems.count), height: tabBar.frame.height))
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.changeTabBar(to: .myLuca)

        _ = userService.registerIfNeeded()
            .flatMapCompletable { result in
                if result == .userRecreated {
                    return self
                        .traceIdService
                        .disposeData(clearTraceHistory: true)
                }
                return Completable.empty()
            }
            .do(onError: { (error) in
                DispatchQueue.main.async {
                    self.processErrorMessages(error: error)
                }
            })
            .do(onDispose: {
                DispatchQueue.main.async { self.progressHud.dismiss() }
            })
            .subscribe()

        subscribeToSelfCheckin()
        subscribeToDocumentUpdates()

        // Check accessed TraceIDs once in app runtime lifecycle
        accessTraceIdChecker
            .fetch()
            .logError(self, "Accessed Trace Id check")
            .subscribe()
            .disposed(by: disposeBag)

        documentProcessingService
            .deeplinkStore
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { deepLink in
                if !deepLink.isEmpty {
                    self.parseQRCode(testString: deepLink)
                }
            })
            .disposed(by: disposeBag)

        // If app was terminated
        if privateMeetingService.currentMeeting != nil {
            self.changeTabBar(to: .checkin)
        }
        _ = self.traceIdService.isCurrentlyCheckedIn
            .observe(on: MainScheduler.instance)
            .do(onSuccess: { checkedIn in
                if checkedIn { self.changeTabBar(to: .checkin) }
            })
            .subscribe()
            .disposed(by: disposeBag)

        // If entering app from background
        UIApplication.shared.rx.applicationWillEnterForeground
            .flatMap { _ -> Single<(Bool)> in
                self.traceIdService.isCurrentlyCheckedIn.map { checkedIn -> (Bool) in
                    let privateMeeting = self.privateMeetingService.currentMeeting != nil
                    return (checkedIn || privateMeeting)
                }
            }.asObservable()
            .observe(on: MainScheduler.instance)
            .do(onNext: { checkedIn in
                if checkedIn { self.changeTabBar(to: .checkin) }
            }).subscribe()
            .disposed(by: disposeBag)

            let updateService = UpdateService(parent: self)
            _ = updateService
                .handleUpdateInfoScreens()
                .subscribe(on: MainScheduler.instance)
                .subscribe()
    }

    private func parseQRCode(testString: String) {
        let alert = ViewControllerFactory.Alert.createTestPrivacyConsent(confirmAction: {
            self.documentProcessingService
                .parseQRCode(qr: testString, additionalValidators: self.initAdditionalValidators())
                .subscribe(onError: { error in
                    DispatchQueue.main.async {
                        UIAlertController.presentErrorAlert(presenter: self, for: error)
                    }
                })
                .disposed(by: self.disposeBag)
        })
        alert.modalTransitionStyle = .crossDissolve
        alert.modalPresentationStyle = .overCurrentContext
        present(alert, animated: true, completion: nil)
    }

    private func initAdditionalValidators() -> [DocumentValidator] {
        [VaccinationBirthDateValidator(
            presenter: self,
            userFirstName: lucaPreferences.blocking.get(\.firstName) ??? "",
            userLastName: lucaPreferences.blocking.get(\.lastName) ??? "",
            documentSource: documentRepoService.currentAndNewTests,
            personsSource: personRepo.restore())
        ]
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        disposeBag = DisposeBag()
    }

    private func subscribeToSelfCheckin() {
        // Continuously check if there is any pending self check in request and consume it if its the case
        selfCheckin
            .pendingSelfCheckinRx
            .flatMap { pendingCheckin in
                return Completable.from { self.selfCheckin.consumeCurrent() }
                .andThen(CheckInInteractor(presenter: self,
                                           qrString: pendingCheckin.url.absoluteString).interact())
                .do(onSubscribe: {
                    DispatchQueue.main.async {
                        self.progressHud.show(in: self.view)
                        self.changeTabBar(to: .checkin)
                    }
                })
                    .andThen(self.traceIdService.fetchTraceStatusRx())
                    .do(onDispose: { [weak self] in DispatchQueue.main.async { self?.progressHud.dismiss() } })
            }
            .ignoreElementsAsCompletable()
            .debug("Self checkin")
            .catch {
                self.rxErrorAlert(for: $0)
            }
            .logError(self, "Pending self checkin")
            .retry(delay: .seconds(1), scheduler: MainScheduler.instance)
            .subscribe()
            .disposed(by: disposeBag)

    }

    private func subscribeToDocumentUpdates() {
        documentRepoService
            .documentUpdateSignal
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: {
                self.changeTabBar(to: .myLuca)
            }).disposed(by: disposeBag)
    }

    private func rxErrorAlert(for error: Error) -> Completable {
        self.processErrorMessagesRx(error: error)
            .andThen(Completable.error(error)) // Push the error through to retry the stream
    }

    func changeTabBar(to index: TabBarIndex) {
        self.selectedIndex = index.rawValue
    }
}

extension MainTabBarViewController: UITabBarControllerDelegate {

    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        return viewController != tabBarController.selectedViewController
    }

}

extension MainTabBarViewController: UnsafeAddress, LogUtil {}
