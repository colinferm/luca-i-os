import UIKit
import RxSwift
import RxCocoa
import RxAppState
import JGProgressHUD
import DependencyInjection

class TestQRCodeScannerController: UIViewController, HandlesLucaErrors {

    @InjectStatic(\.qrProcessingService) private var qrProcessingService

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet private weak var cameraView: UIView!
    private var loadingHUD = JGProgressHUD.lucaLoading()

    var closeButton: UIBarButtonItem?

    var qrCodeScannerViewController: QRScannerViewController!

    var scan: Observable<QRType> {
        return Observable.merge(Observable.just(Void()), rx.viewState.map { _ in Void() })
            .filter { _ in self.isViewLoaded }
            .take(1)
            .flatMap { _ in self.qrCodeScannerViewController.scan }
            .take(1)
            .flatMap {
                self.qrProcessingService.processQRCode(qr: $0, expectedType: .document, presenter: self)
                    .do(onSubscribe: { DispatchQueue.main.async { self.loadingHUD.show(in: self.view) } })
                    .do(onDispose: { DispatchQueue.main.async { self.loadingHUD.dismiss() } })
            }
            .catch {
                if let authorizationError = $0 as? MetadataScannerServiceLocalizedError,
                   authorizationError == .captureDeviceNotAuthorized {
                    return self.handleAuthorizationError().andThen(Observable<QRType>.empty())
                }
                return self.processErrorMessagesRx(error: $0)
                    .andThen(Observable.error($0))
            }
            .retry()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
        setupNavigationbar()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupAccessibility()
    }

    func setupViews() {
        cameraView.layer.cornerRadius = 4

        qrCodeScannerViewController = ViewControllerFactory.Checkin.createQRScannerViewController()

        addChild(qrCodeScannerViewController)
        cameraView.addSubview(qrCodeScannerViewController.view)
        qrCodeScannerViewController.didMove(toParent: self)

        qrCodeScannerViewController.view.setAnchorConstraintsFullSizeTo(view: cameraView)
    }

    func setupNavigationbar() {
        set(title: L10n.Test.Scanner.title)
        closeButton = UIBarButtonItem(image: UIImage(named: "closeButton"), style: .plain, target: self, action: #selector(closeTapped))
        navigationItem.rightBarButtonItem = closeButton
    }

    @objc func closeTapped() {
        dismiss(animated: true, completion: nil)
    }

    private func handleAuthorizationError() -> Completable {
        UIAlertController.okAndCancelAlertRx(
            viewController: self,
            title: L10n.Camera.Access.title,
            message: L10n.Camera.Access.description
        )
        .do(onSuccess: {
            if $0 {
                UIApplication.shared.openApplicationSettings()
            }
        })
        .do(onDispose: {
            self.dismiss(animated: true, completion: nil)
        })
        .asCompletable()
    }
}

// MARK: - Accessibility
extension TestQRCodeScannerController {

    private func setupAccessibility() {
        closeButton?.accessibilityLabel = L10n.Test.Scanner.close
        cameraView.accessibilityLabel = L10n.Test.Scanner.camera
        cameraView.isAccessibilityElement = true
        self.view.accessibilityElements = [navigationbarTitleLabel, closeButton, descriptionLabel, cameraView].map { $0 as Any }
        UIAccessibility.setFocusTo(navigationbarTitleLabel, notification: .layoutChanged, delay: 0.8)
    }

}
