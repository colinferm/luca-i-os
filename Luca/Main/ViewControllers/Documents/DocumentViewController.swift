import UIKit
import RxSwift

protocol DocumentViewControllerDelegate: AnyObject {
    func didTapDelete(for document: Document, viewController: UIViewController)
}

class DocumentViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var stackView: UIStackView!

    weak var delegate: DocumentViewControllerDelegate?

    var documents: [Document] = []
    var documentName: String?
    var focusedDocument: Document?

    private var disposeBag: DisposeBag?

    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavigationBar()
        setup()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        scrollToDocument()
    }
}

extension DocumentViewController {

    private func setup() {
        view.backgroundColor = .black
        setupStackView()
    }

    private func setupNavigationBar() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        set(title: documentName ?? "")
    }

    private func setupStackView() {
        stackView.removeAllArrangedSubviews()
        let itemViews: [DocumentView] = documents.compactMap { DocumentViewFactory.createView(for: $0, isExpanded: true, with: self) }
        DocumentViewFactory.group(views: itemViews).forEach { stackView.addArrangedSubview($0) }
    }

    private func scrollToDocument() {
        guard let horizontalView = stackView.arrangedSubviews.first as? HorizontalDocumentListView, let focusedDocument = focusedDocument else { return }

        horizontalView.scroll(to: focusedDocument)
    }
}

extension DocumentViewController: DocumentViewDelegate {
    func didTapDelete(for document: Document) {
        delegate?.didTapDelete(for: document, viewController: self)
    }

    func didSelect(document: Document) {
        // Not used
    }
}
