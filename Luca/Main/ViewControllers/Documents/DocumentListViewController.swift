import UIKit
import RxSwift
import LucaUIComponents
import DependencyInjection

class DocumentListViewController: UIViewController {

    @InjectStatic(\.keyValueRepo) private var keyValueRepo
    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.documentRepoService) private var documentRepoService
    @InjectStatic(\.documentProcessingService) private var documentProcessingService
    @InjectStatic(\.backendMiscV3) private var backendMisc
    @InjectStatic(\.personService) private var personService
    @InjectStatic(\.accessTraceIdChecker) private var accessTraceIdChecker

    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var addButton: LightStandardButton!
    @IBOutlet weak var dataAccessBanners: DataAccessesBanners!
	@IBOutlet weak var notificationStackView: UIStackView!

    @IBOutlet weak var emptyStateView: UIView!
    @IBOutlet weak var dataAccessHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var stackView: UIStackView!

    weak var timeDifferenceView: TimeDifferenceView?
    var addChildrenButtonView: AddChildrenBarButtonView?

    private var disposeBag: DisposeBag?
    private var scanningDisposeBag: DisposeBag?

    private var documents = [Document]()
    private var persons = [Person]()
    private var personsDidChange = false
    private var groupedDocuments: [String: [Document]] = [:]
    private var testScannerNavController: UINavigationController?

    private var dataAccessHeight: CGFloat = 84
    private var noDataAccessHeight: CGFloat = 0

    private let calendarURLString = "https://www.luca-app.de/coronatest/search"

    private let revalidationKey = "revalidationKey"

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

		setup()
        setupNavigationBar()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        installObservers()
		checkTimesync()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupAccessibility()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        disposeBag = nil
    }
}

// MARK: - Setup

extension DocumentListViewController {

    private func setup() {
        notificationStackView.removeAllArrangedSubviews()
        notificationStackView.isHidden = true

        setupStackView()
        setupApplicationStateObserver()
    }

    private func setupNavigationBar() {
        set(title: L10n.My.Luca.title)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

        let calendarButton = UIBarButtonItem(image: Asset.calendar.image, style: .plain, target: self, action: #selector(calendarTapped))
        calendarButton.accessibilityLabel = L10n.My.Luca.calendar

        addChildrenButtonView = AddChildrenBarButtonView(colorMode: .light)
        addChildrenButtonView?.accessibilityLabel = L10n.Children.List.title
        addChildrenButtonView?.delegate = self
        let addPersonButton = UIBarButtonItem(customView: addChildrenButtonView!)
        addPersonButton.accessibilityLabel = L10n.Children.List.title

        navigationItem.rightBarButtonItems = [calendarButton, addPersonButton]

        scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 43, right: 0)
    }

    func setupStackView() {
        stackView.removeAllArrangedSubviews()

        var groupViews: [DocumentGroupView] = []
        groupedDocuments.forEach { (key, docs) in
            let isChildren = persons.filter { $0.formattedName == key }.count > 0
            let sortedDocs = sortDocuments(docs)
            let itemViews: [DocumentView] = sortedDocs.compactMap { DocumentViewFactory.createView(for: $0, isExpanded: false, with: self) }
            let groupView = DocumentGroupView(title: key, isChildren: isChildren, views: itemViews)
            groupViews.append(groupView)
        }

        groupViews
            .sorted { (!$0.isChildren && $1.isChildren) || $0.isChildren == $1.isChildren && $0.documentViewList?.count ?? 0 > $1.documentViewList?.count ?? 0 }
            .forEach { stackView.addArrangedSubview( $0 ) }
    }

    private func installObservers() {
        let newDisposeBag = DisposeBag()
        revalidateIfNeeded()
            .andThen(loadPersons())
            .andThen(loadDocuments())
            .subscribe()
            .disposed(by: newDisposeBag)

        Observable<Void>.merge(
            Observable.just(Void()),
            accessTraceIdChecker.onNewData.map { _ in Void() }
        )
        .debounce(.milliseconds(500), scheduler: MainScheduler.instance)
        .observe(on: MainScheduler.instance)
        .flatMap { _ in self.dataAccessBanners.fetch(useOnlyCachedData: true) }
        .subscribe()
        .disposed(by: newDisposeBag)

        dataAccessBanners
            .onShowWarningLevel
            .do(onNext: { [weak self] warningLevel in
                let vc = ViewControllerFactory.History.createHistoryViewController(filterForWarningLevel: warningLevel)
                self?.navigationController?.pushViewController(vc, animated: true)
            })
            .drive()
            .disposed(by: newDisposeBag)

        disposeBag = newDisposeBag
    }

    private func loadDocuments() -> Completable {
        documentRepoService
            .currentAndNewTests
            .subscribe(on: ConcurrentDispatchQueueScheduler(qos: .userInitiated))
            .filter({ newDocuments in
                if self.personsDidChange { return true }
                if self.documents.count != newDocuments.count { return true }
                return !self.documents.elementsEqual(newDocuments) { $0.originalCode == $1.originalCode }
            })
            .observe(on: MainScheduler.instance)
            .do(onNext: { docs in
                self.documents = docs.compactMap { $0 }
                self.groupDocuments()
                self.updateViewControllerStyle()
                self.setupStackView()
                self.personsDidChange = false
            }, onSubscribe: {
                self.updateViewControllerStyle()
            })
            .ignoreElementsAsCompletable()
    }

    private func loadPersons() -> Completable {
        personService
            .retrieve()
            .subscribe(on: ConcurrentDispatchQueueScheduler(qos: .userInitiated))
            .observe(on: MainScheduler.instance)
            .do(onSuccess: { entries in
                self.personsDidChange = !self.persons.elementsEqual(entries) { $0.formattedName == $1.formattedName }
                self.persons = entries
                self.addChildrenButtonView?.setCount(entries.count)
            })
            .asCompletable()
    }

    private func groupDocuments() {
        guard let userData = lucaPreferences.blocking.get(\.userRegistrationData),
              let firstName = userData.firstName,
              let lastName = userData.lastName else { return }

        groupedDocuments = [:]

        let docs = documentsFor(firstname: firstName, lastname: lastName, includeDocsWithoutUser: true)
        groupedDocuments["\(firstName) \(lastName)"] = docs

        persons.forEach { person in
            let docs = documentsFor(firstname: person.firstName, lastname: person.lastName)
            groupedDocuments[person.formattedName] = docs
        }
    }

    private func sortDocuments(_ documents: [Document]) -> [Document] {
        // sort by date
        var sortedDocs = documents.sorted(by: { $0.issuedAt.compare($1.issuedAt) == .orderedDescending })

        // if latest vaccine is not valid yet, present first valid vaccine instead
        if let index = sortedDocs.firstIndex(where: { doc in
            if let vaccination = doc as? Vaccination {
                return vaccination.isComplete()
            }
            return false
        }),
        index > 0 {
            let vaccination = sortedDocs.remove(at: index)
            sortedDocs.insert(vaccination, at: 0)
        }

        return sortedDocs
    }

    private func documentsFor(firstname: String, lastname: String, includeDocsWithoutUser: Bool = false) -> [Document] {
        return documents.compactMap { document in
            if let doc = document as? AssociableToIdentity {
                if doc.belongsToUser(withFirstName: firstname, lastName: lastname) {
                    return document
                }
            } else if includeDocsWithoutUser {
                return document
            }

            return nil
        }
    }

    private func name(for document: Document) -> String {
        var name = ""
        for (key, items) in groupedDocuments {
            if items.filter({$0.identifier == document.identifier}).count > 0 {
                name = key
            }
        }

        return name
    }

    private func getLastRevalidationDate() -> Single<Date?> {
        keyValueRepo.load(revalidationKey)
            .catch { _ in Single.just(nil) }
    }

    private func revalidateIfNeeded() -> Completable {
        return getLastRevalidationDate()
            .flatMapCompletable { date in

                if date == nil || !Calendar.current.isDateInToday(date!) {
                    return self.documentProcessingService.revalidateSavedTests()
                        .andThen(self.keyValueRepo.store(self.revalidationKey, value: Date()))
                }

                return Completable.empty()
            }
    }

    private func setupApplicationStateObserver() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(applicationDidEnterBackground(_:)),
            name: UIApplication.didEnterBackgroundNotification,
            object: nil)

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(applicationDidBecomeActive(_:)),
            name: UIApplication.didBecomeActiveNotification,
            object: nil)
    }

    @objc
    func applicationDidEnterBackground(_ notification: NSNotification) {
        self.disposeBag = nil

        let scannerViewController = self.testScannerNavController?.viewControllers.first as? TestQRCodeScannerController
        self.testScannerNavController?.dismiss(animated: true, completion: nil)
    }

    @objc
    func applicationDidBecomeActive(_ notification: NSNotification) {
        self.installObservers()
        self.checkTimesync()
    }

    private func updateViewControllerStyle() {
        DispatchQueue.main.async { [weak self] in
            guard let unwrappedSelf = self else { return }
            let isEmptyState = unwrappedSelf.documents.isEmpty
            unwrappedSelf.emptyStateView.isHidden = !isEmptyState
            unwrappedSelf.stackView.isHidden = isEmptyState
        }
	}
}

// MARK: - Actions

extension DocumentListViewController {

    @IBAction func addTestPressed(_ sender: UIButton) {
        testScannerNavController = ViewControllerFactory.Document.createTestQRScannerViewController()
        if let scanner = testScannerNavController {
            scanner.modalPresentationStyle = .overFullScreen
            scanner.definesPresentationContext = true
            present(scanner, animated: true, completion: nil)
        }
        if let scannerVC = testScannerNavController?.viewControllers.compactMap({ $0 as? TestQRCodeScannerController }).first {

            let newDisposeBag = DisposeBag()

            scannerVC.scan
                .observe(on: MainScheduler.asyncInstance)
                .take(1)
                .take(until: scannerVC.rx.viewDidDisappear)
                .do(onNext: { (qrType: QRType) in
                    self.testScannerNavController?.dismiss(animated: true, completion: nil)
                    if qrType == .checkin {
                        (self.tabBarController as? MainTabBarViewController)?.changeTabBar(to: .checkin)
                    }
                })
                .subscribe()
                .disposed(by: newDisposeBag)

            scanningDisposeBag = newDisposeBag
        }
    }

    @objc private func calendarTapped() {
        if let url = URL(string: calendarURLString) {
            UIApplication.shared.open(url)
        }
    }
}

extension DocumentListViewController {
    private func delete(document: Document, viewController: UIViewController) {
        let alert = UIAlertController.yesOrNo(title: L10n.Test.Delete.title, message: L10n.Test.Delete.description, onYes: {
            _ = self.documentProcessingService.remove(document: document)
                .observe(on: MainScheduler.instance)
                .do(onError: { error in
                    let alert = UIAlertController.infoAlert(title: L10n.Navigation.Basic.error, message: L10n.Test.Result.Delete.error)
                    viewController.present(alert, animated: true, completion: nil)
                }, onCompleted: { [weak self] in
                    self?.navigationController?.popViewController(animated: true)
                })
                .subscribe()
        }, onNo: nil)

        viewController.present(alert, animated: true, completion: nil)
    }
}

// MARK: - Timesync

extension DocumentListViewController {
    private func checkTimesync() {
        guard let disposeBag = disposeBag else { return }

        backendMisc.fetchTimesync()
            .asSingle()
            .observe(on: MainScheduler.instance)
            .do(onSuccess: { [weak self] time in
                // only show the view, if there is a time difference > 5 min
                let isValid = (time.unix - 5 * 60) ... (time.unix + 5 * 60) ~= Int(Date().timeIntervalSince1970)
                isValid ? self?.hideTimeDifferenceView() : self?.showTimeDifferenceView()
            })
            .subscribe()
            .disposed(by: disposeBag)
    }

    private func showTimeDifferenceView() {
        if timeDifferenceView == nil {
            timeDifferenceView = TimeDifferenceView.fromNib()
            notificationStackView.addArrangedSubview(timeDifferenceView!)
        }
        notificationStackView.isHidden = false

        timeDifferenceView?.isHidden = false
    }

    private func hideTimeDifferenceView() {
        timeDifferenceView?.isHidden = true
        notificationStackView.isHidden = true
    }
}

// MARK: - DataAccess

extension DocumentListViewController {

    private func showDataAccessAlert(onOk: (() -> Void)? = nil, accesses: [SectionedAccessedTraceId]) {
        let alert = ViewControllerFactory.Alert.createDataAccessAlertViewController(accesses: accesses, allAccessesPressed: allAccessesPressed)
        alert.modalTransitionStyle = .crossDissolve
        alert.modalPresentationStyle = .overCurrentContext
        (self.tabBarController ?? self).present(alert, animated: true, completion: nil)
    }

    private func allAccessesPressed() {
        let viewController = ViewControllerFactory.Main.createDataAccessViewController()
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension DocumentListViewController: UnsafeAddress, LogUtil {}

// MARK: - Delegates

extension DocumentListViewController: DocumentViewDelegate {
    func didTapDelete(for document: Document) {
        // Not used
    }

    func didSelect(document: Document) {

        let documentName = name(for: document)
        var documents: [Document] = []
        if document is Vaccination {
            documents = self.groupedDocuments[documentName]?.filter { $0 is Vaccination } ?? []
        } else {
            documents.append(document)
        }

        let viewController = ViewControllerFactory.Document.createDocumentViewControllerTab()
        viewController.delegate = self
        viewController.documents = documents
        viewController.documentName = documentName
        viewController.focusedDocument = document

        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension DocumentListViewController: DocumentViewControllerDelegate {
    func didTapDelete(for document: Document, viewController: UIViewController) {
        delete(document: document, viewController: viewController)
    }
}

extension DocumentListViewController: AddChildrenBarButtonViewDelegate {
    func didTapButton() {
        let viewController = ViewControllerFactory.Children.createChildrenListViewController()
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

// MARK: - Accessibility

extension DocumentListViewController {

    private func setupAccessibility() {
        addButton.accessibilityLabel = L10n.Test.Add.title
        UIAccessibility.setFocusTo(navigationbarTitleLabel, notification: .layoutChanged, delay: 0.8)
    }

}
