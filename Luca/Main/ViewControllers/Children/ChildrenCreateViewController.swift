import UIKit
import LucaUIComponents
import RxSwift
import RxCocoa
import DependencyInjection

protocol ChildrenCreateViewControllerDelegate: AnyObject {
    func didAddPerson()
}

class ChildrenCreateViewController: UIViewController, LucaModalAppearance {

    @InjectStatic(\.personService) private var personService

    // MARK: - Outlets

    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var saveButton: LightStandardButton!
    @IBOutlet weak var titleLabel: Luca20PtBoldLabel!
    @IBOutlet weak var descriptionLabel: Luca14PtLabel!
    @IBOutlet weak var firstnameTextField: LucaDefaultTextField!
    @IBOutlet weak var lastnameTextField: LucaDefaultTextField!

    weak var delegate: ChildrenCreateViewControllerDelegate?
    private var disposeBag: DisposeBag!

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        bindRxControls()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        disposeBag = nil
    }

    func bindRxControls() {
        let newDisposeBag = DisposeBag()

        Observable.combineLatest(firstnameTextField.rx.text.orEmpty.asObservable(), lastnameTextField.rx.text.orEmpty.asObservable())
            .map { (t: (String, String)) -> Bool in
                t.0.sanitize().count > 0 && t.1.sanitize().count > 0
            }
            .bind(to: self.saveButton.rx.isEnabled)
            .disposed(by: newDisposeBag)

        disposeBag = newDisposeBag
    }

}

// MARK: - Private functions

extension ChildrenCreateViewController {
    private func setup() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: L10n.Navigation.Basic.cancel, style: .plain, target: self, action: #selector(cancelTapped))

        stackView.spacing = 20

        titleLabel.text = L10n.Children.Add.title
        titleLabel.numberOfLines = 1
        titleLabel.textAlignment = .center

        descriptionLabel.text = L10n.Children.Add.description
        descriptionLabel.numberOfLines = 0

        let attributes = [
            NSAttributedString.Key.foregroundColor: Asset.luca747480.color,
            NSAttributedString.Key.font: FontFamily.Montserrat.medium.font(size: 14)
        ]
        firstnameTextField.attributedPlaceholder = NSAttributedString(string: L10n.Children.Add.Placeholder.firstname, attributes: attributes)
        lastnameTextField.attributedPlaceholder = NSAttributedString(string: L10n.Children.Add.Placeholder.lastname, attributes: attributes)

        saveButton.setTitle(L10n.Children.Add.button.uppercased(), for: .normal)
        saveButton.addTarget(self, action: #selector(saveTapped), for: .touchUpInside)

        let tap = UITapGestureRecognizer(target: self, action: #selector(self.viewTapped(_:)))
        view.addGestureRecognizer(tap)

        applyColors()
    }

    private func savePerson(firstName: String, lastname: String) {
        _ = personService
            .create(firstName: firstName, lastName: lastname)
            .observe(on: MainScheduler.instance)
            .do(onSuccess: { _ in
                self.dismiss(animated: true, completion: nil)
                self.delegate?.didAddPerson()
            })
            .subscribe()
    }
}

// MARK: - Actions

extension ChildrenCreateViewController {
    @objc func cancelTapped() {
        dismiss(animated: true, completion: nil)
    }

    @objc func saveTapped() {
        guard let firstname = firstnameTextField.text?.sanitize(),
              let lastname = lastnameTextField.text?.sanitize(),
              firstname.count > 0,
              lastname.count > 0
        else { return }

        savePerson(firstName: firstname, lastname: lastname)
    }

    @IBAction func viewTapped(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
}
