import UIKit
import RxSwift
import DependencyInjection

class LaunchViewController: UIViewController, HandlesLucaErrors {

    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.dailyKeyRepository) private var dailyKeyRepository
    @InjectStatic(\.backendMiscV3) private var backendMisc
    @InjectStatic(\.userService) private var userService
    @InjectStatic(\.traceIdService) private var traceIdService
    @InjectStatic(\.dailyKeyRepoHandler) private var dailyKeyRepoHandler

    var keyAlreadyFetched = false

    // Holds last app version where TermsAcceptanceViewController needs to be presented
    let lastTermsUpdatedVersion = 53

    var versionCheckerDisposeBag = DisposeBag()

    // It's a safety check if data has been corrupted between updates. Or the initial state
    var dataComplete: Bool {
        let uuid = lucaPreferences.blocking.get(\.uuid)
        let hasUUID = uuid != nil
        let isPersonalDataComplete = (try? userService.isPersonalDataComplete.retrieveBlocking()) ?? false
        return hasUUID && isPersonalDataComplete
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if fetchDailyKey() {
            launchStoryboard()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if !dataComplete {
            lucaPreferences.blocking.set(\.welcomePresented, value: false)
            lucaPreferences.blocking.set(\.dataPrivacyPresented, value: false)
        }

        // Continuous version checker
        backendMisc.fetchSupportedVersions()
            .asSingle()
            .asObservable()
            .flatMap { supportedVersions in
                Single.from { Bundle.main.buildVersionNumber }
                    .asObservable()
                    .unwrapOptional()
                    .map { Int($0) }
                    .unwrapOptional()
                    .map { $0 >= supportedVersions.minimumVersion  }
            }
            .observe(on: MainScheduler.instance)
            .flatMap { isSupported -> Completable in
                if !isSupported,
                   let topVC = UIApplication.shared.topViewController {

                    return UIAlertController.infoBoxRx(viewController: topVC,
                                                       title: L10n.Navigation.Basic.error,
                                                       message: L10n.VersionSupportChecker.failureMessage)
                        .ignoreElementsAsCompletable()

                }
                return Completable.empty()
            }
            .logError(self, "Version support checker")
            .retry(delay: .seconds(10), scheduler: LucaScheduling.backgroundScheduler)
            .subscribe()
            .disposed(by: versionCheckerDisposeBag)
    }

    func launchStoryboard() {

        var viewController: UIViewController!
        if !lucaPreferences.blocking.get(\.welcomePresented) {
            viewController = ViewControllerFactory.Onboarding.createWelcomeViewController()
        } else if !dataComplete {
            if !lucaPreferences.blocking.get(\.dataPrivacyPresented) {
                viewController = ViewControllerFactory.Onboarding.createDataPrivacyViewController()
            } else {
                lucaPreferences.blocking.set(\.userRegistrationData, value: UserRegistrationData())
                lucaPreferences.blocking.set(\.currentOnboardingPage, value: 0)
                lucaPreferences.blocking.set(\.phoneNumberVerified, value: false)
                _ = traceIdService.disposeData(clearTraceHistory: true).subscribe()

                viewController = ViewControllerFactory.Onboarding.createFormViewController()
            }
        } else if dataComplete && !lucaPreferences.blocking.get(\.donePresented) {
            viewController = ViewControllerFactory.Onboarding.createDoneViewController()
        } else if lucaPreferences.blocking.get(\.termsAcceptedVersion) < lastTermsUpdatedVersion {
            viewController = ViewControllerFactory.Terms.createTermsAcceptanceViewController()
        } else {
            viewController = ViewControllerFactory.Main.createTabBarController()
        }
        viewController.modalPresentationStyle = .fullScreen
        viewController.modalTransitionStyle = .crossDissolve
        self.present(viewController, animated: true, completion: nil)
    }

    /// Returns true if there is any daily key. It will be updated anyway, but the app can proceed
    func fetchDailyKey() -> Bool {
        var isAnyKey = false
        if let newestId = dailyKeyRepository.newestId,
            (try? dailyKeyRepository.restore(index: newestId)) != nil {
            isAnyKey = true
        }
        if keyAlreadyFetched && isAnyKey {
            return true
        }

        _ = dailyKeyRepoHandler.fetchRx()
            .observe(on: MainScheduler.asyncInstance)
            .subscribe {
                self.keyAlreadyFetched = true
                // Continue with UI
                if !isAnyKey {
                    self.launchStoryboard()
                }
            } onError: { error in
                self.processErrorMessages(error: error, completion: nil)
            }

        return isAnyKey
    }

    private func showErrorAlert(for error: LocalizedTitledError) {
        DispatchQueue.main.async {
            let alert = UIAlertController.infoBox(
                title: error.localizedTitle,
                message: error.localizedDescription)

            UIViewController.visibleViewController?.present(alert, animated: true, completion: nil)
        }
    }
}

extension LaunchViewController: UnsafeAddress, LogUtil {}
