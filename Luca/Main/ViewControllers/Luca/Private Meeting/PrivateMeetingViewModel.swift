import Foundation
import RxSwift
import RxCocoa
import DependencyInjection

// swiftlint:disable:next type_body_length
final class PrivateMeetingViewModel: LogUtil, UnsafeAddress {

    @InjectStatic(\.privateMeetingQRCodeBuilderV3) private var privateMeetingQRCodeBuilderV3
    @InjectStatic(\.privateMeetingService) private var privateMeetingService

	private var meetingRelay: BehaviorRelay<PrivateMeetingData>
    var meetingDriver: Driver<PrivateMeetingData> {
        meetingRelay.asDriver()
    }
    var guestsAsStringsDriver: Driver<[String]> {
        meetingRelay
            .map { [weak self] meeting in
                meeting.guests.compactMap { guest in
                    try? self?.privateMeetingService.decrypt(guestData: guest, meetingKeyIndex: meeting.keyIndex)
                }
                .enumerated()
                .map { "\($0+1)  \($1.fn) \($1.ln)" }

            }
            .map { Array(Set($0)) }
            .asDriver(onErrorJustReturn: [""])
    }
    var guestsDriver: Driver<[PrivateMeetingGuest]> {
        meetingRelay.map {$0.guests}.asDriver(onErrorJustReturn: [])
    }

	private var timerRelay = BehaviorRelay<String>(value: "")
    var timerDriver: Driver<String> {
        timerRelay.asDriver()
    }

    var newQRData: Driver<UIImage?> {
        meetingDriver.map { [weak self] meeting -> UIImage? in
            if let url = try? self?.privateMeetingQRCodeBuilderV3.build(scannerId: meeting.ids.scannerId).generatedUrl,
                 let data = url.data(using: .utf8) {
                return self?.setupQrImage(qrCodeData: data)
            }
            return nil
        }
        .asDriver(onErrorJustReturn: nil)
    }

	private var disposeBag = DisposeBag()

    deinit {
        print("PrivateMeetingViewModel.deinit")
    }

    init(meeting: PrivateMeetingData) {

		meetingRelay = BehaviorRelay<PrivateMeetingData>(value: meeting)

		Observable<Int>.timer(.seconds(1), period: .seconds(10), scheduler: LucaScheduling.backgroundScheduler)
            .take(until: { [weak self] _ in
                self == nil || self?.privateMeetingService == nil
            })
			.flatMap { [weak self] _ -> Single<PrivateMeetingData> in
				guard let selfRef = self else { throw RxError.disposed(object: PrivateMeetingViewModel.self) }
                return selfRef.privateMeetingService.refresh(meeting: selfRef.meetingRelay.value)
			}
			.observe(on: MainScheduler.instance)
			.do(onNext: { [weak self] refreshedMeeting in
				self?.meetingRelay.accept(refreshedMeeting)
			})
			.logError(self, "Meeting fetch")
			.retry(delay: .seconds(1), scheduler: LucaScheduling.backgroundScheduler)
			.subscribe()
			.disposed(by: disposeBag)
	}

	func startTimer() {
		CheckinTimer.shared.delegate = self
		CheckinTimer.shared.start(from: meetingRelay.value.createdAt)
	}

    func stopTimer() {
        CheckinTimer.shared.stop()
    }

	func endMeeting(completion:@escaping (() -> Void), failure:@escaping((Error) -> Void)) {
		privateMeetingService.close(meeting: meetingRelay.value) {
			DispatchQueue.main.async {
				CheckinTimer.shared.stop()
				completion()
			}
		} failure: { (error) in
			failure(error)
		}
	}

	private	func setupQrImage(qrCodeData: Data) -> UIImage? {

		// Temp QR Code generation.
		let qrCode = QRCodeGenerator.generateQRCode(data: qrCodeData)
		if let qr = qrCode {
			let transform = CGAffineTransform(scaleX: 10, y: 10)
			let scaledQr = qr.transformed(by: transform)

			return UIImage(ciImage: scaledQr)

		}
		return nil
	}
}

extension PrivateMeetingViewModel: TimerDelegate {
	func timerDidTick() {
		timerRelay.accept(CheckinTimer.shared.counter.formattedTimeString)
	}
}
