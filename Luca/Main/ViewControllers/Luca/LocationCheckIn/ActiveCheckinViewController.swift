import UIKit
import JGProgressHUD
import CoreLocation
import RxSwift
import RxCocoa
import RxAppState
import StoreKit
import LucaUIComponents
import DependencyInjection

// enum CheckinType {
//		case privateMeeting
//		case location
// }

class ActiveCheckinViewController: UIViewController, HandlesLucaErrors {
    @InjectStatic(\.lucaPreferences) private var lucaPreferences
	@IBOutlet weak var checkinSlider: CheckinSliderButton?
	@IBOutlet weak var timerLabel: UILabel!
	@IBOutlet weak var checkinDateLabel: UILabel!
	@IBOutlet weak var automaticCheckoutSwitch: UISwitch!
	@IBOutlet weak var checkOutLabel: UILabel!
	@IBOutlet weak var welcomeLabel: UILabel!
	@IBOutlet weak var tableNumberLabel: UILabel!
	@IBOutlet weak var automaticCheckoutLabel: UILabel!
	@IBOutlet weak var autoCheckoutView: UIView!
	@IBOutlet weak var checkinTimeDescription: Luca14PtLabel!

	var viewModel: LocationCheckInViewModel!
	weak var rootViewController: RootCheckinViewController!

    var addChildrenButtonView: AddChildrenBarButtonView?

	private var loadingHUD = JGProgressHUD.lucaLoading()
	private var userStatusFetcherDisposeBag: DisposeBag?
	private var checkOutDisposeBag: DisposeBag?

	override func viewDidLoad() {
		super.viewDidLoad()
		NotificationPermissionHandler.shared.requestAuthorization(viewController: self)
		setupViews()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.welcomeLabel.textColor = Asset.lucaBlack87Percent.color
		self.checkinDateLabel.textColor = Asset.lucaBlack87Percent.color
		self.automaticCheckoutLabel.textColor = Asset.lucaBlack.color
		self.checkinTimeDescription.textColor = Asset.lucaBlack.color
		self.checkOutLabel.textColor = Asset.lucaBlack.color

		setupAccessibility()

		installObservers()
	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		removeObservers()

		userStatusFetcherDisposeBag = nil
	}

	@IBAction func autoCheckoutViewPressed(_ sender: UITapGestureRecognizer) {
		guard let acv = autoCheckoutView, acv.accessibilityElementIsFocused() && UIAccessibility.isVoiceOverRunning else { return }
		let isOn = automaticCheckoutSwitch.isOn
		automaticCheckoutSwitch.setOn(!isOn, animated: true)
		viewModel.isAutoCheckoutEnabled.onNext(automaticCheckoutSwitch.isOn)
		let switchState = automaticCheckoutSwitch.isOn ? L10n.LocationCheckinViewController.AutoCheckout.on : L10n.LocationCheckinViewController.AutoCheckout.off
		autoCheckoutView?.accessibilityLabel = "\(L10n.LocationCheckinViewController.autoCheckout) \(switchState)"
	}

	@objc private func checkoutForAccessibility() -> Bool {
		checkout()
		return true
	}

	private func checkout() {
		if checkOutDisposeBag != nil {
			return
		}

		showAppStoreReview()

		let disposeBag = DisposeBag()

		viewModel.checkOut()
			.observe(on: MainScheduler.instance)
			.logError(self, "Check out")
			.do(onError: { (error) in
				self.processErrorMessages(error: error)
			}, onDispose: {
				self.checkOutDisposeBag = nil
			})
			.onErrorComplete()
			.subscribe()
			.disposed(by: disposeBag)

		checkOutDisposeBag = disposeBag
	}

	private func resetCheckInSlider() {
		checkinSlider?.reset()
		checkOutLabel.isHidden = false
		checkOutLabel.alpha = 1.0
	}

	func showAppStoreReview() {
        var appStoreReviewCheckoutCounter = lucaPreferences.blocking.get(\.appStoreReviewCheckoutCounter)
		appStoreReviewCheckoutCounter += 1
        lucaPreferences.blocking.set(\.appStoreReviewCheckoutCounter, value: appStoreReviewCheckoutCounter)
		if appStoreReviewCheckoutCounter % 5 == 0 {
			SKStoreReviewController.requestReview()
		}
	}

	// MARK: View setup functions.

	func setupViews() {

        addChildrenButtonView = AddChildrenBarButtonView(colorMode: .dark)
        addChildrenButtonView?.delegate = self
        let addPersonButton = UIBarButtonItem(customView: addChildrenButtonView!)
        addPersonButton.accessibilityLabel = L10n.Children.List.title

        navigationItem.rightBarButtonItem = addPersonButton

//		welcomeLabel.text = L10n.LocationCheckinViewController.welcomeMessage
		checkinTimeDescription.text = L10n.Checkin.noun.string
		automaticCheckoutLabel.text = L10n.LocationCheckinViewController.autoCheckout.string
		resetCheckInSlider()

        // child VCs show back button without button title
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.hidesBackButton = true
	}

	// swiftlint:disable:next function_body_length
	private func installObservers() {

		let newDisposeBag = DisposeBag()

		viewModel.isCheckedIn
			.do { [weak self] (isCheckedIn) in
				if !isCheckedIn {
					self?.removeObservers()
					self?.rootViewController?.goToRoot()
				}
			}
			.drive()
			.disposed(by: newDisposeBag)

		viewModel.isBusy.do { [weak self] (busy) in
			if let s = self, busy {
				self?.loadingHUD.show(in: s.view)
			} else {
				self?.loadingHUD.dismiss()
			}
		}
		.drive()
		.disposed(by: newDisposeBag)

		viewModel.isCheckingOut.do { [weak self] (checkoutInProgress) in
			self?.checkinSlider?.processing = checkoutInProgress
		}
		.drive()
		.disposed(by: newDisposeBag)

		viewModel.alert
			.asObservable()
			.flatMapFirst { alert in
				return UIAlertController.infoAlertRx(viewController: self, title: alert.title, message: alert.message)
			}
			.subscribe()
			.disposed(by: newDisposeBag)

		viewModel.additionalDataLabelHidden
			.drive(tableNumberLabel.rx.isHidden)
			.disposed(by: newDisposeBag)

		viewModel.additionalDataLabelText
			.drive(tableNumberLabel.rx.text)
			.disposed(by: newDisposeBag)

		viewModel.time
			.drive(self.timerLabel.rx.text)
			.disposed(by: newDisposeBag)

		viewModel.isAutoCheckoutAvailable
			.map { !$0 }
			.drive(self.autoCheckoutView.rx.isHidden)
			.disposed(by: newDisposeBag)

		viewModel.isAutoCheckoutAvailable
			.asObservable()
			.subscribe(onNext: { isAvailable in
				self.autoCheckoutView?.isAccessibilityElement = isAvailable
			})
			.disposed(by: newDisposeBag)

		viewModel.checkInTime
			.drive(checkinDateLabel.rx.text)
			.disposed(by: newDisposeBag)

		viewModel.checkedInString
			.drive(welcomeLabel.rx.text)
			.disposed(by: newDisposeBag)

        viewModel.checkedInChildren
            .asObservable()
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { persons in
                self.addChildrenButtonView?.setCount(persons.count)
            })
            .disposed(by: newDisposeBag)

		Driver.combineLatest(viewModel.groupName, viewModel.locationName, viewModel.isPrivateMeeting).drive(onNext: { [weak self] (groupName, locationName, isPrivateMeeting) in
			self?.setupLocationLabels(with: groupName, and: locationName, isPrivateMeeting: isPrivateMeeting)
		}).disposed(by: newDisposeBag)

		(automaticCheckoutSwitch.rx.value <-> viewModel.isAutoCheckoutEnabled).disposed(by: newDisposeBag)

		let checkinSliderCompletionBlock:(() -> Void)? = { [weak self] in
			self?.checkout()
		}
		checkinSlider?.completionBlock = checkinSliderCompletionBlock

		viewModel.checkInTimeDate
			.subscribe(onSuccess: { time in
				self.checkinDateLabel.accessibilityLabel = L10n.Checkin.Slider.date(time.accessibilityDate)
			})
			.disposed(by: newDisposeBag)

		viewModel.connect(viewController: self)

		userStatusFetcherDisposeBag = newDisposeBag
	}

	private func removeObservers() {
		userStatusFetcherDisposeBag = nil
		viewModel.release()
	}

	private func setupLocationLabels(with groupName: String?, and locationName: String?, isPrivateMeeting: Bool) {

		let stackView = UIStackView()
		stackView.axis = .vertical
		stackView.alignment = .center
		if isPrivateMeeting || locationName != nil {
			let heading = Luca12PtLabel()
			heading.text = isPrivateMeeting ? L10n.Private.Meeting.Info.title.string : (locationName ?? "")
			stackView.addArrangedSubview(heading)
		}
		if let g = groupName {
			let title = Luca20PtBoldLabel()
			title.text = g
			title.textColor = Asset.lucaBlack.color
			stackView.addArrangedSubview(title)
		}
		self.navigationItem.titleView = stackView
	}

    @IBAction func infoTapped(_ sender: UIButton) {
        _ = ViewControllerFactory.Alert.createLocationAccessInformationViewController(presentedOn: self)
            .ignoreElementsAsCompletable()
            .subscribe(on: MainScheduler.instance)
            .subscribe()
    }

}

extension ActiveCheckinViewController: UnsafeAddress, LogUtil {}

// MARK: - Accessibility
extension ActiveCheckinViewController {

	private func setupAccessibility() {
		checkinSlider?.setAccessibilityLabel(text: L10n.LocationCheckinViewController.Accessibility.checkoutSlider)

		autoCheckoutView?.accessibilityTraits = automaticCheckoutSwitch.accessibilityTraits
		//		 groupNameLabel?.accessibilityTraits = .header
		//		 locationNameLabel?.accessibilityTraits = .header

		autoCheckoutView?.isAccessibilityElement = true
		timerLabel.isAccessibilityElement = false

		//		 if groupNameLabel?.text != nil {
		//				 UIAccessibility.setFocusTo(groupNameLabel, notification: .layoutChanged, delay: 0.8)
		//		 } else if locationNameLabel?.text != nil {
		//				 UIAccessibility.setFocusTo(locationNameLabel, notification: .layoutChanged, delay: 0.8)
		//		 }

		let switchState = automaticCheckoutSwitch.isOn ? L10n.LocationCheckinViewController.AutoCheckout.on : L10n.LocationCheckinViewController.AutoCheckout.off
		autoCheckoutView?.accessibilityLabel = "\(L10n.LocationCheckinViewController.autoCheckout) \(switchState)"

		setupAccessibilityAutocheckoutAction()

		self.view.accessibilityElements = [welcomeLabel, checkinDateLabel, tableNumberLabel, autoCheckoutView, checkinSlider?.sliderImage].compactMap {$0}.map { $0 as Any }
	}

	private func setupAccessibilityAutocheckoutAction() {
		let accessibilityCompleteAction = UIAccessibilityCustomAction(
			name: L10n.LocationCheckinViewController.Accessibility.directCheckout,
			target: self,
			selector: #selector(checkoutForAccessibility))

		accessibilityCustomActions = [accessibilityCompleteAction]
	}
}

extension ActiveCheckinViewController: AddChildrenBarButtonViewDelegate {
    func didTapButton() {
        navigationController?.pushViewController(
            ViewControllerFactory.Children.createEmbeddedChildrenListViewController(viewModel: self.viewModel),
            animated: true
        )
    }
}
