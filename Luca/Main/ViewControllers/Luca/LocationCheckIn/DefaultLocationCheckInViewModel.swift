import Foundation
import RxSwift
import RxCocoa
import CoreLocation
import DependencyInjection

// swiftlint:disable:next type_body_length
class DefaultLocationCheckInViewModel: LocationCheckInViewModel {

    var didCheckoutSubject = PublishSubject<Void>()
    var checkoutTriggered: Driver<Void> {
        didCheckoutSubject.asDriver(onErrorDriveWith: Driver<Void>.empty())
    }

    var alertSubject = PublishSubject<PrintableMessage>()
    var alert: Driver<PrintableMessage> {
        alertSubject.asDriver(onErrorDriveWith: Driver<PrintableMessage>.empty())
    }

    var isBusySubject = PublishSubject<Bool>()
    var isBusy: Driver<Bool> {
        isBusySubject.asDriver(onErrorJustReturn: false)
    }

    var isCheckingOutSubject = PublishSubject<Bool>()
    var isCheckingOut: Driver<Bool> {
        isCheckingOutSubject.distinctUntilChanged().asDriver(onErrorJustReturn: false)
    }

    var isAutoCheckoutAvailableSubject = BehaviorSubject<Bool>(value: false)
    var isAutoCheckoutAvailable: Driver<Bool> {
        location
            .map { $0?.geoLocationRequired ?? false }
            .asDriver(onErrorJustReturn: false)
    }
    var isAutoCheckoutEnabled = PublishSubject<Bool>()

    var isCheckedIn: Driver<Bool> {
        traceIdService.isCurrentlyCheckedInChanges
            .flatMap { isCheckedIn -> Single<Bool> in
                if !isCheckedIn { return self.notificationService.removePendingNotifications().andThen(.just(isCheckedIn)) }
                return Completable.empty().andThen(.just(isCheckedIn))
            } // Remove pending notifications
            .asDriver(onErrorJustReturn: true)
    }

    var locationName: Driver<String?> {
        location.map { $0?.locationName }.asDriver(onErrorDriveWith: Driver<String?>.empty())
    }

    var groupName: Driver<String?> {
        location.map { $0?.groupName }.asDriver(onErrorDriveWith: Driver<String?>.empty())
    }

    var isPrivateMeeting: Driver<Bool> {
        location.map {$0?.isPrivate ?? false}.asDriver(onErrorJustReturn: false)
    }

    var timeSubject = BehaviorSubject<String>(value: "00:00:00")
    var time: Driver<String> {
        timeSubject.asDriver(onErrorJustReturn: "")
    }

    var checkedInChildrenRelay = BehaviorRelay<[Person]>(value: [])
    var checkedInChildren: Observable<[Person]> {
        checkedInChildrenRelay.asObservable()
    }

    var checkedInString: Driver<String> {
        checkedInChildren.flatMap { (children: [Person]) -> Observable<String> in
            if children.count > 0 {
                return Observable.just(L10n.LocationCheckinViewController.welcomeMessageWithKids(children.map {$0.firstName}.asSentence))
            } else {
                return Observable.just(L10n.LocationCheckinViewController.welcomeMessage)
            }
        }.asDriver(onErrorJustReturn: L10n.LocationCheckinViewController.welcomeMessage)
    }

    var checkInTime: Driver<String> {
        Single.from { L10n.Checkin.Slider.date(self.traceInfo.checkInDate.formattedDateTime) }.asDriver(onErrorJustReturn: "")
    }

    var checkInTimeDate: Single<Date> {
        Single.from { self.traceInfo.checkInDate }
    }

    /// Emits true when the label with additional data should be hidden
    var additionalDataLabelHidden: Driver<Bool> {
        Single.from { self.traceIdService.additionalData == nil }.asDriver(onErrorJustReturn: true)
    }

    /// Contents of the additional data label
    var additionalDataLabelText: Driver<String> {
        Single.from {
            if let additionalData = self.traceIdService.additionalData as? TraceIdAdditionalData {
                return L10n.LocationCheckinViewController.AdditionalData.table(additionalData.table)
            } else if self.traceIdService.additionalData as? PrivateMeetingQRCodeV3AdditionalData != nil {
                return ""
            } else if let additionalData = self.traceIdService.additionalData as? [String: String],
                      let first = additionalData.first {
                return "\(first.key): \(first.value)"
            }
            return ""
        }
        .asDriver(onErrorJustReturn: "")
    }

    func checkOut() -> Completable {

        self.isCheckingOutSubject.onNext(true)
        let performTimeCheck = Completable.from {
            let secondsDiff = Date().timeIntervalSince1970 - self.traceInfo.checkInDate.timeIntervalSince1970
            if secondsDiff < 2 * 60.0 {
							self.isCheckingOutSubject.onNext(false)
                throw PrintableError(
                    title: L10n.Navigation.Basic.hint,
                    message: L10n.LocationCheckinViewController.CheckOutFailed.LowDuration.message
                )
            }
        }

        let checkOutBackend = traceIdService.checkOut()
            .catch({ (error) in
								self.isCheckingOutSubject.onNext(false)
                var errorTitle = L10n.Navigation.Basic.error
                if let localizedError = error as? LocalizedTitledError {
                    errorTitle = localizedError.localizedTitle
                }

                return Completable.error(
                    PrintableError(
                        title: errorTitle,
                        message: error.localizedDescription
                    )
                )
            })

        return performTimeCheck
            .andThen(checkOutBackend)
            .andThen(notificationService.removePendingNotifications())
						.do(onCompleted: {
							self.isCheckingOutSubject.onNext(false)
						})
            .subscribe(on: LucaScheduling.backgroundScheduler)
    }

    func togglePersonAssociate(_ person: Person, traceInfo: TraceInfo) -> Completable {
        var base: Completable
        if person.isAssociated(with: traceInfo) {
            base = personService.removeAssociation(persons: [person], from: traceInfo)
        } else {
            base = personService.associate(persons: [person], with: traceInfo)
        }
        return base.andThen(personService.retrieveAssociated(with: traceInfo))
            .do(onSuccess: { children in
                self.checkedInChildrenRelay.accept(children)
            })
            .asObservable()
            .ignoreElementsAsCompletable()
            .observe(on: MainScheduler.instance)
        }

    func release() {
        checkInTimer.delegate = nil
        checkInTimer.stop()
        disposeBag = nil
        viewController = nil
    }

    func connect(viewController: UIViewController) {
        self.viewController = viewController

        let newDisposeBag = DisposeBag()

        personService.retrieveAssociated(with: traceInfo)
            .asObservable()
            .bind(to: checkedInChildrenRelay)
            .disposed(by: newDisposeBag)

        autoCheckoutService.isToggledOn
            .observe(on: MainScheduler.instance)
            .catch { error in
                let viewController: UIViewController
                if let localizedError = error as? LocalizedTitledError {
                    viewController = UIAlertController.infoAlert(
                        title: localizedError.localizedTitle,
                        message: localizedError.localizedDescription
                    )
                } else {
                    viewController = UIAlertController.infoAlert(
                        title: L10n.Navigation.Basic.error,
                        message: error.localizedDescription
                    )
                }
                self.viewController.present(viewController, animated: true, completion: nil)
                return Observable.error(error)
            }
            .retry(delay: .milliseconds(100), scheduler: MainScheduler.instance)
            .bind(to: isAutoCheckoutEnabled)
            .disposed(by: newDisposeBag)

        isAutoCheckoutEnabled
            .asObservable()
            .distinctUntilChanged()
            .flatMap { isEnabled -> Completable in
                if isEnabled {
                    return self.autoCheckoutService.toggleOn(
                        alertToShowWhenNotAskedYet: self.alertBeforeAskingLocationPermissionForCheckout(viewController: self.viewController),
                        alertForOthers: { _ in self.alertAskingToChangePermissionInSettings(viewController: self.viewController)}
                    ).catch { _ in Completable.empty() }
                } else {
                    return self.autoCheckoutService.toggleOff().catch { _ in Completable.empty() }
                }
            }
            .subscribe()
            .disposed(by: newDisposeBag)

        let restoreCheckIn = UIApplication.shared.rx.applicationWillEnterForeground
            .do(onNext: { _ in
                self.checkInTimer.start(from: self.traceInfo.checkInDate)
            })
            .logError(self, "applicationWillEnterForeground")
            .retry(delay: .seconds(1), scheduler: LucaScheduling.backgroundScheduler)
            .ignoreElementsAsCompletable()

        let loadCurrentlySavedLocation = self.traceIdService.loadCurrentLocationInfo()
            .do(onSuccess: { self.location.onNext($0) })
            .asObservable()
            .onErrorComplete()
            .ignoreElementsAsCompletable()

        Completable.zip(
            restoreCheckIn,
            fetchCurrentLocation(),
            loadCurrentlySavedLocation
        )
            .subscribe(on: LucaScheduling.backgroundScheduler)
            .subscribe()
            .disposed(by: newDisposeBag)

        disposeBag = newDisposeBag

        setupCheckInTimer()
    }

    private func setupCheckInTimer() {
        checkInTimer.delegate = self
        let date = traceInfo.createdAtDate ?? traceInfo.checkInDate

        // This is a fix for the case when user set its time manually and is shifted towards future.
        // If this is the case, it selects the current date as the starting point to remedy the shift
        if Date().timeIntervalSince1970 - date.timeIntervalSince1970 < 0 {
            checkInTimer.start(from: Date())
        } else {
            checkInTimer.start(from: date)
        }
    }

	private func fetchCurrentLocation() -> Completable {
		// first load and display the local data. Then fetch the most current data from the server and display it.

		traceIdService
			.fetchCurrentLocationInfo(checkLocalDBFirst: true)
			.do(onSuccess: { location in
				self.location.onNext(location)
			})
			.do(onError: { error in
				self.alertSubject.onNext((title: L10n.Navigation.Basic.error,
																	message: L10n.LocationCheckinViewController.LocationInfoFetchFailure.message(error.localizedDescription)))
			})
			.asCompletable()
			.andThen(traceIdService.fetchCurrentLocationInfo())
			.do(onSuccess: { location in
				self.location.onNext(location)
			})
			.do(onError: { error in
				self.alertSubject.onNext((title: L10n.Navigation.Basic.error,
																	message: L10n.LocationCheckinViewController.LocationInfoFetchFailure.message(error.localizedDescription)))
			})
			.asObservable()
			.ignoreElementsAsCompletable()
			.onErrorComplete()
	}

    @InjectStatic(\.lucaPreferences) private var preferences
    @InjectStatic(\.traceIdService) private var traceIdService: TraceIdService
    @InjectStatic(\.autoCheckoutService) private var autoCheckoutService: AutoCheckoutService
    @InjectStatic(\.notificationService) private var notificationService: NotificationService
    @InjectStatic(\.personService) private var personService: PersonService
    @InjectStatic(\.lucaLocationPermissionWorkflow) private var lucaLocationPermissionWorkflow: LucaLocationPermissionWorkflow
    private let traceInfo: TraceInfo
    private let checkInTimer: CheckinTimer
    private let location = BehaviorSubject<Location?>(value: nil)

    private var disposeBag: DisposeBag?

    private var viewController: UIViewController! = nil

    init(traceInfo: TraceInfo,
         timer: CheckinTimer) {
        self.traceInfo = traceInfo
        self.checkInTimer = timer
    }

    private func retrieveUserID() -> Single<UUID> {
        preferences.get(\.uuid).map {
            if let uuid = $0 {
                return uuid
            }
            throw NSError(domain: "Couldn't obtain user ID", code: 0, userInfo: nil)
        }
    }

    private func askForLocationPermission(viewController: UIViewController) -> Single<CLAuthorizationStatus> {
        let deniedPermissionAlert = alertAskingToChangePermissionInSettings(viewController: viewController)
        let infoAlert = alertBeforeAskingLocationPermissionForCheckout(viewController: viewController)
        return lucaLocationPermissionWorkflow.tryToAcquireLocationPermissionAlways(alertToShowBefore: infoAlert, alertToShowIfDenied: deniedPermissionAlert)
    }

    private func alertBeforeAskingLocationPermissionForCheckout(viewController: UIViewController) -> Completable {
        return ViewControllerFactory.Alert.createLocationAccessInformationViewController(presentedOn: viewController)
            .ignoreElementsAsCompletable()
            .subscribe(on: MainScheduler.instance)
    }

    private func alertAskingToChangePermissionInSettings(viewController: UIViewController) -> Completable {
        UIAlertController.infoAlertRx(
                    viewController: viewController,
                    title: L10n.LocationCheckinViewController.Permission.Change.title,
                    message: L10n.LocationCheckinViewController.Permission.Change.message)
                    .ignoreElementsAsCompletable()
    }

    private func alertAfterLocationPermissionDeniedForCheckout(viewController: UIViewController) -> Completable {
        var message = L10n.LocationCheckinViewController.Permission.Denied.messageWithoutName
        if let name = (try? self.location.value())?.formattedName {
            message = L10n.LocationCheckinViewController.Permission.Denied.message(name)
        }
        return UIAlertController.infoAlertRx(
            viewController: viewController,
            title: L10n.Navigation.Basic.error,
            message: message)
        .ignoreElementsAsCompletable()
            .subscribe(on: MainScheduler.instance)
    }
}

extension DefaultLocationCheckInViewModel: TimerDelegate {
    func timerDidTick() {
        timeSubject.onNext(checkInTimer.counter.formattedTimeString)
    }
}

extension DefaultLocationCheckInViewModel: UnsafeAddress, LogUtil {}
