import UIKit
import RxSwift
import RxCocoa
import RxAppState

class OptionalCheckinDataViewController: UIViewController, LucaModalAppearance {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var `switch`: UISwitch!
    @IBOutlet private weak var button: UIButton!

    /// Emits `true` if button was clicked and checkbox was selected, `false` if checkbox was unseleced
    /// Completes if user cancels or the view controller is otherwise dismissed
    var result: Maybe<Bool> {
        self.loadViewIfNeeded()
        return button.rx.tap
            .asObservable()
            .map { _ in self.switch.isOn }
            .take(1)
            .do(onNext: { _ in
                self.dismiss(animated: true, completion: nil)
            })
            .take(until: rx.viewDidDisappear)
            .asMaybe()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.rightBarButtonItem = UIBarButtonItem(title: L10n.Navigation.Basic.cancel, style: .plain, target: self, action: #selector(cancelTapped))

        applyColors()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        _ = self.switch.rx.isOn
            .take(until: self.rx.viewDidDisappear)      // Complete on viewDidDisappear
            .take(until: self.rx.viewDidAppear.skip(1)) // Complete on viewDidAppear (but skip the first value). It's a safety check to prevent multiple streams
            .asDriver(onErrorJustReturn: false)
            .drive(onNext: { isOn in
                self.imageView.image = isOn ? Asset.anonymousEnabled.image : Asset.anonymous.image
            })
    }

    @objc private func cancelTapped(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
}
