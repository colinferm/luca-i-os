import UIKit
import RxSwift

class UpdatePageViewController: UIPageViewController {

    let vcs = [ViewControllerFactory.Main.createUpdateViewController(text: L10n.Update.Screen._1, image: Asset.updateScreen1.image),
               ViewControllerFactory.Main.createUpdateViewController(text: L10n.Update.Screen._2, image: Asset.updateScreen2.image),
               ViewControllerFactory.Main.createUpdateViewController(text: L10n.Update.Screen._3, image: Asset.updateScreen3.image),
               ViewControllerFactory.Main.createUpdateViewController(text: L10n.Update.Screen._4, image: Asset.updateScreen4.image),
               ViewControllerFactory.Main.createUpdateViewController(text: L10n.Update.Screen._5, image: Asset.updateScreen5.image),
               ViewControllerFactory.Main.createUpdateViewController(text: L10n.Update.Screen._6, image: Asset.updateScreen6.image),
               ViewControllerFactory.Main.createUpdateViewController(text: L10n.Update.Screen._7, image: Asset.updateScreen7.image)]

    var currentPage: BehaviorSubject<Int> = BehaviorSubject(value: 0)

    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        delegate = self
        setViewControllers([vcs[0]], direction: .forward, animated: true, completion: nil)
        setupPageControl()
    }

    func setupPageControl() {
        let pc = UIPageControl.appearance(whenContainedInInstancesOf: [UpdatePageViewController.self])
        pc.currentPageIndicatorTintColor = Asset.lucaBlue.color
        pc.pageIndicatorTintColor = UIColor.gray
        pc.backgroundColor = .clear
    }

}
 extension UpdatePageViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let vc = viewController as? UpdateViewController, let index = vcs.firstIndex(of: vc) else {
            return nil
        }

        let previous = index - 1

        guard previous >= 0, vcs.count > previous else {
            return nil
        }

        return vcs[previous]
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let vc = viewController as? UpdateViewController, let viewControllerIndex = vcs.firstIndex(of: vc) else {
            return nil
        }

        let next = viewControllerIndex + 1

        guard vcs.count != next, vcs.count > next else {
            return nil
        }

        return vcs[next]
    }

     func presentationCount(for pageViewController: UIPageViewController) -> Int {
         return vcs.count
     }

     func presentationIndex(for pageViewController: UIPageViewController) -> Int {
         guard let first = viewControllers?.first, let vc = first as? UpdateViewController, let index = vcs.firstIndex(of: vc) else { return 0 }
         currentPage.onNext(index)
         return index
     }

     func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
         guard let first = viewControllers?.first, let vc = first as? UpdateViewController, let index = vcs.firstIndex(of: vc) else { return }
         currentPage.onNext(index)
     }

 }
