import UIKit
import RxSwift
import LucaUIComponents

class UpdateContainerViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var previousButton: DarkStandardButton!
    @IBOutlet weak var nextButton: LightStandardButton!
    @IBOutlet weak var cancelButton: DesignableButton!
    @IBOutlet weak var buttonStackView: UIStackView!

    // Complete on skip and close buttons
    var onCompleted: (() -> Void)?
    var onCancelled: (() -> Void)?

    var pageVC: UpdatePageViewController!
    var disposeBag: DisposeBag?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPageVC()
        installObservers()
        setupDynamicButtons()
    }

    private func setupPageVC() {
        pageVC = ViewControllerFactory.Main.createUpdatePageViewController()
        addChild(pageVC)

        containerView.addSubview(pageVC.view)
        pageVC.didMove(toParent: self)
        pageVC.view.setAnchorConstraintsFullSizeTo(view: containerView)
    }

    override func viewWillDisappear(_ animated: Bool) {
        disposeBag = nil
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        setupDynamicButtons()
    }

    func setupDynamicButtons() {
        buttonStackView.axis = self.traitCollection.preferredContentSizeCategory >= .accessibilityMedium ? .vertical : .horizontal
    }

    private func installObservers() {
        let newDisposeBag = DisposeBag()

        pageVC.currentPage
            .subscribe(on: MainScheduler.instance)
            .subscribe(onNext: { index in
                self.setup(for: index)
                UIAccessibility.setFocusTo(self.cancelButton, notification: .screenChanged)
            })
            .disposed(by: newDisposeBag)
        disposeBag = newDisposeBag
    }

    private func setup(for index: Int) {
        var previousTitle = L10n.Navigation.Basic.back
        var nextTitle = L10n.Navigation.Basic.next

        if index == 0 {
            previousTitle = L10n.Navigation.Basic.skip
            nextTitle = L10n.Navigation.Basic.look
        } else if index == pageVC.vcs.count - 1 {
            nextTitle = L10n.Navigation.Basic.close
        }

        previousButton.setTitle(previousTitle, for: .normal)
        nextButton.setTitle(nextTitle, for: .normal)
    }

    @IBAction func previousPressed(_ sender: DarkStandardButton) {
        _ = pageVC.currentPage
            .take(1)
            .asSingle()
            .subscribe(onSuccess: { index in
                // If first page dismiss view controller
                if index == 0, let completed = self.onCompleted {
                    completed()
                    DispatchQueue.main.async { self.dismiss(animated: true, completion: nil) }
                } else {
                    self.pageVC.setViewControllers([self.pageVC.vcs[index - 1]], direction: .reverse, animated: true, completion: nil)
                }
            })
    }

    @IBAction func nextPressed(_ sender: LightStandardButton) {
        _ = pageVC.currentPage
            .take(1)
            .asSingle()
            .subscribe(onSuccess: { index in
                // If last page dismiss view controller
                if index == self.pageVC.vcs.count - 1, let completed = self.onCompleted {
                    completed()
                    DispatchQueue.main.async { self.dismiss(animated: true, completion: nil) }
                } else {
                    self.pageVC.setViewControllers([self.pageVC.vcs[index + 1]], direction: .forward, animated: true, completion: nil)
                }
            })
    }

    @IBAction func cancelPressed(_ sender: DesignableButton) {
        if let cancelled = onCancelled {
            cancelled()
            dismiss(animated: true, completion: nil)
        }
    }

}
