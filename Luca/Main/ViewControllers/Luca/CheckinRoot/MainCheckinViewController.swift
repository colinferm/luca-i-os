import Foundation
import UIKit
import LucaUIComponents
import RxSwift
import AVFoundation
import JGProgressHUD
import AVKit
import DependencyInjection

class MainCheckinViewController: UIViewController, BindableType, HandlesLucaErrors, Themable {
    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.qrProcessingService) private var qrProcessingService
    @InjectStatic(\.privateMeetingService) private var privateMeetingService
    @InjectStatic(\.traceIdService) private var traceIdService
    @InjectStatic(\.userService) private var userService
    @InjectStatic(\.checkinPolling) private var checkinPolling

    var theme: AppearanceTheme = .dark

	@IBOutlet var showQRCodeButton: LightStandardButton!
	@IBOutlet var privateMeetingButton: DarkStandardButton!
	@IBOutlet weak var scanViewDescriptionLabel: UILabel!
	@IBOutlet weak var scanPreviewWrapper: DesignableView!
	@IBOutlet weak var activateScannerView: DesignableView!
	@IBOutlet weak var activateScannerLabel: Luca14PtLabel!
	@IBOutlet weak var activateScannerButton: UIButton!
	@IBOutlet weak var lightButton: UIButton!

	var viewModel: MainCheckinViewModel!
	private var onCheckInDisposeBag: DisposeBag?
	private var loadingHUD = JGProgressHUD.lucaLoading()
    private let disposeBag = DisposeBag()

    private var qrCodeScannerViewController: QRScannerViewController! = nil
    private var scanningDisposeBag: DisposeBag! = nil

	weak var rootViewController: RootCheckinViewController?

	override func viewDidLoad() {
		super.viewDidLoad()
		self.title = L10n.Checkin.noun
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
		self.activateScannerLabel.text = L10n.Camera.Access.Activate.label.string
		privateMeetingButton.setTitle(L10n.Button.Private.meeting.uppercased(), for: .normal)
		scanPreviewWrapper.accessibilityLabel = L10n.Contact.Qr.Accessibility.qrCodeScanner
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: Asset.history.image.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(showHistory))
		guard let device = AVCaptureDevice.default(for: .video) else {
			self.lightButton.isHidden = true
			return
		}
		self.lightButton.isHidden = !device.hasTorch || !device.isTorchAvailable
	}

	#if DEBUG
	deinit {
		print("deinit MainCheckinViewController")
	}
	#endif

	func bindViewModel() {

		viewModel.checkin
            .filter { [weak self] _ in self?.viewModel.mainCheckinViewControllerVisible.value ?? false }
			.drive { [weak self] _ in
				if self?.presentedViewController != nil {
					self?.dismiss(animated: true) {
						self?.showCheckinOrMeetingViewController(animated: true)
					}
				} else {
					self?.showCheckinOrMeetingViewController(animated: true)
				}
			}
			.disposed(by: disposeBag)
        viewModel.showScanner
            .drive { [weak self] show in
                self?.showScanner(display: show)
            }
            .disposed(by: disposeBag)
	}

	private func showScanner(display: Bool) {
        if display == false {
			self.endScanner()
		} else {
            if AVCaptureDevice.authorizationStatus(for: .video) == .authorized {
                self.scanPreviewWrapper.isHidden = false
                self.activateScannerView.isHidden = true
                // FIXME: sometimes the scanner won't show when first opening this tab. Giving it an extra run loop (quick fix) fixes the issue temporarily.
                DispatchQueue.main.async {
                    self.startScanner()
                }
            } else {
                self.scanPreviewWrapper.isHidden = true
                self.activateScannerView.isHidden = false
            }
		}
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
        onCheckInDisposeBag = DisposeBag()
		viewModel.mainCheckinViewControllerVisible.accept(true)
        viewModel
            .dailyPublicKeyAvailable
            .distinctUntilChanged()
            .drive { [weak self] keyValid in
                self?.showQRCodeButton.isEnabled = keyValid
                self?.privateMeetingButton.isEnabled = keyValid
                if !keyValid {
                    self?.processErrorMessages(error: MainCheckinViewError.dailyKeyOutdated)
                }
            }
            .disposed(by: onCheckInDisposeBag!)

        checkinPolling.frequency = .low

        showCheckinOrMeetingViewController(animated: false)

		if !remindIfPhoneNumberNotVerified() {
			remindIfAddressNotFilled()
		}
	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		viewModel.mainCheckinViewControllerVisible.accept(false)

		onCheckInDisposeBag = nil
	}

	@objc private func showHistory() {
		let viewController = ViewControllerFactory.History.createHistoryViewController()
		navigationController?.pushViewController(viewController, animated: true)
	}

	private func showPrivateMeetingViewController(meeting: PrivateMeetingData?, animated: Bool) {
		if let meeting = meeting ?? privateMeetingService.currentMeeting {
			rootViewController?.showPrivateMeeting(meeting: meeting, animated: animated)
		}
	}

	private func goToApplicationSettings() {
		UIAlertController(title: L10n.Camera.Access.title, message: L10n.Camera.Access.description, preferredStyle: .alert).goToApplicationSettings(viewController: self, pop: true)
	}

	private func startScanner() {
        scanningDisposeBag = DisposeBag()
        if qrCodeScannerViewController == nil {
            qrCodeScannerViewController = ViewControllerFactory.Checkin.createQRScannerViewController()

            addChild(qrCodeScannerViewController)
            scanPreviewWrapper.addSubview(qrCodeScannerViewController.view)
            qrCodeScannerViewController.didMove(toParent: self)

            qrCodeScannerViewController.view.setAnchorConstraintsFullSizeTo(view: scanPreviewWrapper)
        }

        qrCodeScannerViewController.scan

            // Delay the subscription to force the appearance to continue without waiting for the session
            .delaySubscription(.milliseconds(1), scheduler: MainScheduler.asyncInstance)
            .take(1)
            .flatMap {
                self.qrProcessingService.processQRCode(qr: $0, expectedType: .checkin, presenter: self)
                    .observe(on: MainScheduler.asyncInstance)
                    .do(onSubscribe: { DispatchQueue.main.async { self.loadingHUD.show(in: self.view) } })
                    .do(onDispose: { self.loadingHUD.dismiss() })
            }
            .catch { (error) -> Observable in
                if let localizedError = error as? LocalizedTitledError,
                   case MetadataScannerServiceLocalizedError.captureDeviceNotAuthorized = localizedError {
                    return Completable.from {
                        DispatchQueue.main.async {
                            self.goToApplicationSettings()
                        }
                    }
                    .andThen(Observable.empty())
                } else {
                    return self.processErrorMessagesRx(error: error)
                        .andThen(Observable.error(error))
                }
            }
            .retry()
            .subscribe()
            .disposed(by: scanningDisposeBag)
	}

	private func endScanner() {
        // Delay this call to prevent hiccup on disposal
        DispatchQueue.main.asyncAfter(deadline: .now() + DispatchTimeInterval.milliseconds(1)) {
            self.scanningDisposeBag = nil
        }
	}

	@IBAction func showMyQRCodePressed(_ sender: UIButton) {
		let viewController = ViewControllerFactory.Checkin.createMyQRViewController()
		navigationController?.present(viewController, animated: true)
	}

	private func showCheckinOrMeetingViewController(animated: Bool) {
		if privateMeetingService.currentMeeting != nil {
			showPrivateMeetingViewController(meeting: nil, animated: animated)
			return
		}
		_ = traceIdService.currentTraceInfo
			.observe(on: MainScheduler.instance)
			.do(onNext: { traceInfo in
				self.rootViewController?.showActiveCheckinViewStack(traceInfo: traceInfo, animated: animated)
			})
			.subscribe()
	}

	/// Returns true if phone number is not verified yet
	private func remindIfPhoneNumberNotVerified() -> Bool {
        if !lucaPreferences.blocking.get(\.phoneNumberVerified) {
			let alert = UIAlertController.infoAlert(title: L10n.Navigation.Basic.attention, message: L10n.Verification.PhoneNumber.notYetVerified) {
				let viewController = ViewControllerFactory.Main.createContactViewController()
				self.navigationController?.pushViewController(viewController, animated: true)
			}
			present(alert, animated: true, completion: nil)
			return true
		}
		return false
	}

	/// Returns true if data is incomplete
	private func remindIfAddressNotFilled() {
        let isDataComplete = (try? userService.isDataComplete.retrieveBlocking()) ?? false
        if !isDataComplete {
			let alert = UIAlertController.infoAlert(title: L10n.Navigation.Basic.attention, message: L10n.UserData.addressNotFilledMessage) {
				let viewController = ViewControllerFactory.Main.createContactViewController()
				self.navigationController?.pushViewController(viewController, animated: true)
			}
			present(alert, animated: true, completion: nil)
		}
	}

	@IBAction func privateMeetingPressed(_ sender: UIButton) {
		UIAlertController(title: L10n.Private.Meeting.Start.title,
											message: L10n.Private.Meeting.Start.description,
											preferredStyle: .alert)
			.actionAndCancelAlert(
                actionText: L10n.Navigation.Basic.start,
                action: createPrivateMeeting,
                viewController: self
            )
	}

	private func createPrivateMeeting() {
		if privateMeetingService.currentMeeting == nil {
			_ = privateMeetingService.createMeeting()
				.subscribe(on: MainScheduler.instance)
				.do(onSubscribe: { [weak self] in
                    guard let unwrappedSelf = self else { return }
                    self?.loadingHUD.show(in: unwrappedSelf.view) })
				.do(onDispose: { [weak self] in
                    self?.loadingHUD.dismiss() })
				.do(onSuccess: { [weak self] meeting in
					self?.showPrivateMeetingViewController(meeting: meeting, animated: true)
				})
				.do(onError: { [weak self] error in
					let alert = UIAlertController.infoAlert(title: L10n.Navigation.Basic.error, message: L10n.Private.Meeting.Create.failure(error.localizedDescription))
					self?.present(alert, animated: true, completion: nil)
				})
				.subscribe()
		}
	}
	@IBAction func activateScannerPressed(_ sender: Any) {
        self.startScanner()
	}

	@IBAction func lightButtonTapped(_ sender: Any) {
		guard let device = AVCaptureDevice.default(for: .video) else { return }
		defer { device.unlockForConfiguration() }
		do {
			try device.lockForConfiguration()
			if device.torchMode == .off {
				try device.setTorchModeOn(level: 1.0)
			} else {
				device.torchMode = .off
			}
		} catch let error {
			//			print(error)
		}
	}

}

extension MainCheckinViewController: UnsafeAddress, LogUtil {}
