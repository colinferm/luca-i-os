import UIKit
import RxSwift
import RxCocoa
import RxAppState
import DependencyInjection

enum MainCheckinViewError: LocalizedTitledError {

    /// The public daily key is older than 7 days (outdated)
    case dailyKeyOutdated
}

extension MainCheckinViewError {
    var localizedTitle: String {
        L10n.Checkin.Scanner.publicKeyOutdatedTitle
    }

    var errorDescription: String? {
        switch self {
        case .dailyKeyOutdated:
            return L10n.Checkin.Scanner.publicKeyOutdatedDescription
        }
    }
}

class MainCheckinViewModel: NSObject, LogUtil, UnsafeAddress, ViewControllerOwnedVM {
    @InjectStatic(\.dailyKeyRepoHandler) private var dailyKeyRepoHandler: DailyKeyRepoHandler
    @InjectStatic(\.traceIdService) private var traceIdService: TraceIdService

	private(set) weak var owner: UIViewController?

	// MARK: - Input
	let mainCheckinViewControllerVisible = BehaviorRelay<Bool>(value: false)

	// MARK: - Output

    var checkin: Driver<TraceInfo> {
        traceIdService
        .onCheckInRx()
        .asDriver(onErrorDriveWith: .empty())
    }
    var showScanner: Driver<Bool> {
        Observable.combineLatest(
            UIApplication.shared.rx.currentAndChangedAppState.subscribe(on: MainScheduler.asyncInstance),
            self.mainCheckinViewControllerVisible.asObservable(),
            dailyPublicKeyAvailable.asObservable()
        )
        .flatMap({ (appState: AppState, visible: Bool, dailyValid) -> Observable<Bool> in
            return .just(appState == .active && visible && dailyValid)
        })
        .distinctUntilChanged()
        .asDriver(onErrorJustReturn: false)
    }
    var dailyPublicKeyAvailable: Driver<Bool> {
        dailyKeyRepoHandler.hasValidDailyPublicKeyDriver
    }

    init(owner: UIViewController? = nil) {
		super.init()
		self.owner = owner
	}

	#if DEBUG
	deinit {
		print("deinit MainCheckinViewModel")
	}
	#endif
}
