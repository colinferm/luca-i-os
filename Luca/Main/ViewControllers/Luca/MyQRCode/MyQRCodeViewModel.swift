import Foundation
import RxSwift
import UIKit
import RxCocoa
import DependencyInjection

final class MyQRCodeViewModel: LogUtil, UnsafeAddress {
	/// It will be incremented on every error and resetted on viewWillAppear. If the amount of errors surpasses the threshold, an alert will be shown.
	private var errorsCount = 0
	private static let errorsThreshold = 5

    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.traceIdService) private var traceIdService

	// input
	var infoAlertDismissed = PublishSubject<Void>()

	// output
	private(set) var newQRData: Driver<CIImage?>!
	private let errorMessagePublisher = PublishSubject<PrintableError>()
	var errorMessageDriver: Driver<PrintableError> {
		errorMessagePublisher.asDriver(onErrorJustReturn: PrintableError(error: nil, title: "", message: ""))
	}

	init() {
		self.bindOutput()
	}

	#if DEBUG
	deinit {
		print("deinit")
	}
	#endif

	private func bindOutput() {
		newQRData = startGeneratingQRCodes()
	}

	func startGeneratingQRCodes() -> Driver<CIImage?> {
		UIApplication.shared.rx.currentAndChangedAppState
			.flatMapLatest { appState -> Observable<UUID?> in
				if appState != .active {
					return .just(nil)
				}
                return self.lucaPreferences.currentAndChanges(\.uuid)
			}
			.unwrapOptional()
			.flatMapLatest { (_: UUID) -> Observable<Int> in
				Observable<Int>.timer(.seconds(0),
                                      period: .seconds(10),
                                      scheduler: LucaScheduling.backgroundScheduler)
			}
			.flatMapFirst { [weak self] _ in self?.handleQRCodeGeneration() ?? .just(nil) }
			//				.debug("QR Image")
			.retry(delay: .seconds(1), scheduler: LucaScheduling.backgroundScheduler)
			.asDriver(onErrorJustReturn: nil)
	}

	private func handleQRCodeGeneration() -> Single<CIImage?> {
		traceIdService.getOrCreateQRCode()
			.asObservable()
			.map {
				$0.qrCodeData
			}
			.catch({ [weak self] (error) -> Observable<Data> in
				guard let selfRef = self else {return Observable<Data>.error(error)}

				defer { selfRef.errorsCount += 1 }

				if selfRef.errorsCount < MyQRCodeViewModel.errorsThreshold {
					return Observable<Data>.error(error) // Do not consume, rely on the retry and allow the log to print this error.
				}
				let err = PrintableError(
					title: L10n.Navigation.Basic.error,
					message: L10n.QrCodeGeneration.Failure.message(error.localizedDescription)
				)
				selfRef.errorMessagePublisher.onNext(err)
				return selfRef.infoAlertDismissed
					.ignoreElementsAsCompletable()
					.andThen(Observable<Data>.error(error))
			})
			.map { QRCodeGenerator.generateQRCode(data: $0) }
			.asSingle()
			.observe(on: MainScheduler.instance)
	}

	func setupQrImage(image: CIImage?, in imageView: UIImageView) {

		// Temp QR Code generation.
		let qrCode = image
		if let qr = qrCode {
			let transform = CGAffineTransform(scaleX: 10, y: 10)
			let scaledQr = qr.transformed(by: transform)

			if imageView.image == nil {
				imageView.alpha = 0.0
				UIView.animate(withDuration: 0.3) {
					imageView.alpha = 1.0
				}
			}
			imageView.image = UIImage(ciImage: scaledQr)
		}
	}
}
