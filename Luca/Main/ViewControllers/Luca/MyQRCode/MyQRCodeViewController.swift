import UIKit
import LucaUIComponents
import RxSwift
import DependencyInjection

class MyQRCodeViewController: UIViewController, BindableType, HandlesLucaErrors, LucaModalAppearance {

    @InjectStatic(\.checkinPolling) private var checkinPolling

	@IBOutlet weak var descriptionLabel: Luca14PtLabel!
	@IBOutlet weak var qrCodeImageView: UIImageView!
	var viewModel: MyQRCodeViewModel!
	private var disposeBag: DisposeBag = DisposeBag()
	@IBOutlet weak var expandButton: UIButton!

    var previousFrequency: PollingFrequency = .none

	override func viewDidLoad() {
		super.viewDidLoad()
		self.setupViews()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		applyColors()
        previousFrequency = checkinPolling.frequency
        checkinPolling.frequency = .high
	}

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        checkinPolling.frequency = previousFrequency
    }

	private func setupViews() {
		self.descriptionLabel.text = L10n.Checkin.Qr.description
		self.title = L10n.Checkin.Qr.Title.myQRCode.string
		self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissModal))
		self.expandButton.isEnabled = false
	}

	func bindViewModel() {
		self.viewModel.newQRData.drive { [weak self] (image: CIImage?) in
			guard let selfRef = self else {return}
			selfRef.expandButton.isEnabled = image != nil
			selfRef.viewModel.setupQrImage(image: image, in: selfRef.qrCodeImageView)
		}.disposed(by: disposeBag)

		self.viewModel.errorMessageDriver.drive { [weak self] (error: PrintableError) in
			self?.processErrorMessages(error: error) {
				self?.viewModel.infoAlertDismissed.onNext(())
			}
		}.disposed(by: disposeBag)
	}

	@IBAction func expandButtonClicked(_ sender: Any) {
		let viewController = ViewControllerFactory.Checkin.createMyQRFullScreenViewController()
		if var vc = viewController as? MyQRCodeFullScreenViewController {
			vc.bindViewModel(to: self.viewModel)
		}
		navigationController?.present(viewController, animated: true)
	}

	@objc func dismissModal() {
		self.dismiss(animated: true, completion: nil)
	}
}

extension MyQRCodeViewController: UnsafeAddress, LogUtil {}
