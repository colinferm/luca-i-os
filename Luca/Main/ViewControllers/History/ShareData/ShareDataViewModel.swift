import Foundation
import RxSwift
import RxCocoa
import DependencyInjection

class ShareDataViewModel: NSObject, LogUtil, UnsafeAddress {
    @InjectStatic(\.userService) private var userService

	private static let numberOfDays = 14
	private static let availableDays = Array(1...ShareDataViewModel.numberOfDays)
	private let generatingTANRelay: BehaviorRelay<Bool> = BehaviorRelay(value: false)
	private let newTAN: PublishSubject<String> = PublishSubject()
	private let disposeBag = DisposeBag()

	// input

	let generateTAN: PublishSubject<Void> = PublishSubject()

	// output

	let items: Observable<[Int]> = Observable.of(Array(1...14))

	let selectedNumberOfDays = BehaviorRelay<(row: Int, component: Int)>(value: (ShareDataViewModel.numberOfDays-1, 0))

	var generatingTAN: Driver<Bool> {
		generatingTANRelay
			.distinctUntilChanged()
			.asDriver(onErrorJustReturn: false)
	}

	var newTANDriver: Driver<String> {
		newTAN.asDriver(onErrorJustReturn: "")
	}

	private let errorMessagePublisher = PublishSubject<PrintableError>()
	var errorMessageDriver: Driver<PrintableError> {
		errorMessagePublisher.asDriver(onErrorJustReturn: PrintableError(error: nil, title: "", message: ""))
	}

	override init() {
		super.init()
		generateTAN.asDriver(onErrorDriveWith: .empty()).drive { [weak self] _ in
			self?.transferUserData()
		}.disposed(by: disposeBag)
	}

	private func transferUserData() {
		generatingTANRelay.accept(true)

		userService.transferUserData(forNumberOfDays: selectedNumberOfDays.value.row+1) { [weak self] challengeId in

			self?.generatingTANRelay.accept(false)

			let formattedString = challengeId
				.uppercased()
				.split(every: 4)
				.reduce("") { (res, group) in
					return "\(res)-\(group)"
				}
				.dropFirst()

			let tan = String(formattedString.uppercased())
			self?.newTAN.onNext(tan)
		} failure: { [weak self] (error) in
			self?.handleError(error: error)
		}
	}

	private func handleError(error: Error) {
		self.generatingTANRelay.accept(false)
		self.log("Error loading challenge ID: \(error)", entryType: .error)

		let err = PrintableError(
			title: L10n.Navigation.Basic.error,
			message: L10n.DataRelease.Tan.Failure.message(error.localizedDescription)
		)
		self.errorMessagePublisher.onNext(err)
	}
}
