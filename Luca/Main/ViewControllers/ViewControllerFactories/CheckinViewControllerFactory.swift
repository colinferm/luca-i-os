import UIKit

class CheckinViewControllerFactory {
	static func createContactQRViewControllerTab() -> UIViewController {
		let contactQRViewController = RootCheckinViewController.instantiate(storyboard: UIStoryboard(name: "CheckinRootViewController", bundle: nil),
                                                                            identifier: "RootCheckinViewController")
		contactQRViewController.tabBarItem.image = UIImage.init(named: "scanner")
		contactQRViewController.tabBarItem.title = L10n.Navigation.Tab.checkin

		return contactQRViewController
	}

	static func createMyQRViewController() -> UIViewController {
		let myQRCodeVC = MyQRCodeViewController.fromStoryboard()
		if var vc = myQRCodeVC as? MyQRCodeViewController {
			vc.bindViewModel(to: MyQRCodeViewModel())
		}
		let navigationController = UINavigationController(rootViewController: myQRCodeVC)
		return navigationController
	}

	static func createMyQRFullScreenViewController() -> UIViewController {
		let myQRCodeVC = MyQRCodeFullScreenViewController.fromStoryboard()
		return myQRCodeVC
	}

	static func createLocationCheckinViewController(traceInfo: TraceInfo) -> ActiveCheckinViewController {
		let viewController: ActiveCheckinViewController = ActiveCheckinViewController.fromStoryboard()

        viewController.viewModel = DefaultLocationCheckInViewModel(
            traceInfo: traceInfo,
            timer: CheckinTimer.shared
        )

		return viewController
	}

	static func createPrivateMeetingViewController(meeting: PrivateMeetingData) -> NewPrivateMeetingViewController {
		let vc: NewPrivateMeetingViewController = NewPrivateMeetingViewController.fromStoryboard()
//		vc.meeting = meeting
		return vc
	}

	static func createQRScannerViewController() -> QRScannerViewController {
		return QRScannerViewController()
	}

    static func createOptionalDataCheckinViewController() -> UINavigationController {
        UINavigationController(rootViewController: OptionalCheckinDataViewController.fromStoryboard())
    }
}
