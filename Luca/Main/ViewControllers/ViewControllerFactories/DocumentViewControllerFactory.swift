import UIKit

class DocumentViewControllerFactory {
    static func createDocumentListViewControllerTab() -> UIViewController {
        let documentViewController = DocumentListViewController.fromStoryboard()
        let navigationController = UINavigationController(rootViewController: documentViewController)
        navigationController.tabBarItem.image = UIImage.init(named: "myLuca")
        navigationController.tabBarItem.title = L10n.Navigation.Tab.health

        return navigationController
    }

    static func createDocumentViewControllerTab() -> DocumentViewController {
        return DocumentViewController.fromStoryboard()
    }

    static func createTestQRScannerViewController() -> UINavigationController {
        let navigationController = UINavigationController(rootViewController: TestQRCodeScannerController.fromStoryboard())
        return navigationController
    }
}
