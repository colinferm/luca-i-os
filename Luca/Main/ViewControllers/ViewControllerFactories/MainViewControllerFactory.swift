import UIKit

class MainViewControllerFactory {
    static func createTabBarController() -> MainTabBarViewController {
        let mainTabBarController = MainTabBarViewController()

        mainTabBarController.viewControllers = [ViewControllerFactory.Document.createDocumentListViewControllerTab(),
                                                ViewControllerFactory.Checkin.createContactQRViewControllerTab(),
                                                ViewControllerFactory.Account.createAccountViewControllerTab()]
        return mainTabBarController
    }

    static func createContactViewController() -> ContactViewController {
        return ContactViewController.fromStoryboard()
    }

    static func createDataAccessViewController() -> DataAccessViewController {
        return DataAccessViewController.fromStoryboard()
    }

    static func createHealthDepartmentCryptoInfoViewController() -> UIViewController {
        return HealthDepartmentCryptoInfoViewController.fromStoryboard()
    }

    static func createUpdatePageViewController() -> UpdatePageViewController {
        return UpdatePageViewController.fromStoryboard()
    }

    static func createUpdateContainerViewController() -> UpdateContainerViewController {
        return UpdateContainerViewController.fromStoryboard()
    }

    static func createUpdateViewController(text: String, image: UIImage) -> UpdateViewController {
        let vc: UpdateViewController = UpdateViewController.fromStoryboard()
        vc.text = text
        vc.image = image
        return vc
    }

}
