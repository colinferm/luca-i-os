import UIKit

class AccessedTraceIdsViewControllerFactory {
    static func createDetailViewController(accessedTraceId: AccessedTraceId) -> AccessedTraceIdDetailViewController {
        let vc: AccessedTraceIdDetailViewController = AccessedTraceIdDetailViewController.fromStoryboard()
        vc.viewModel = AccessedTraceIdDetailViewModel(accessedTraceId: accessedTraceId)
        return vc
    }

    static func createWarningLevelsViewController(traceInfo: TraceInfo) -> WarningLevelsViewController {
        let vc: WarningLevelsViewController = WarningLevelsViewController.fromStoryboard()
        vc.viewModel = WarningLevelsViewModel(traceInfo: traceInfo)
        return vc
    }
}
