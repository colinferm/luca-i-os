import Foundation
import RxSwift
import RxCocoa
import DependencyInjection

struct WarningLevelCellViewModel {
    var isRead: Bool
    var title: String
    var subtitle: String
    var accessedTraceId: AccessedTraceId
}

class WarningLevelsViewModel {

    @InjectStatic(\.accessedTraceIdRepo) private var accessedTraceIdRepo
    @InjectStatic(\.notificationConfigCachedDataSource) private var notificationConfigSource

    private let traceInfo: TraceInfo

    init(traceInfo: TraceInfo) {
        self.traceInfo = traceInfo
    }

    var warningLevels: Driver<[WarningLevelCellViewModel]> {
        notificationConfigSource.retrieve()
            .compactMap { $0.first }
            .asObservable()
            .flatMap { (config: NotificationConfig) in
                self.retrieveRelevantAccessedTraceIds()
                    .asObservable()
                    .flatMap { Observable.from($0) }
                    .map {
                        let msg = config.retrieveMessages(for: $0.warningLevel, healthDepartment: $0.healthDepartmentId)
                        return WarningLevelCellViewModel(
                            isRead: $0.isRead,
                            title: msg.title ?? "Unknown title",
                            subtitle: msg.shortMessage ?? "Unknown subtitle",
                            accessedTraceId: $0
                        )
                    }
                    .toArray()
            }
            .asDriver(onErrorJustReturn: [])
    }

    private func retrieveRelevantAccessedTraceIds() -> Single<[AccessedTraceId]> {
        accessedTraceIdRepo.restore().map { array in array.filter { $0.traceInfoId == self.traceInfo.identifier ?? -1 } }
    }
}
