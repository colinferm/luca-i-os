import Foundation
import RxSwift
import RxCocoa
import DependencyInjection

final class AccessedTraceIdDetailViewModel {
    private let accessedTraceId: AccessedTraceId
    @InjectStatic(\.notificationConfigCachedDataSource) private var notificationConfigSource
    @InjectStatic(\.accessedTraceIdRepo) private var accessedRepo

    init(accessedTraceId: AccessedTraceId) {
        self.accessedTraceId = accessedTraceId
    }

    var subtitle: Driver<String> {
        notificationConfigSource.retrieve()
            .compactMap { $0.first }
            .asObservable()
            .map { $0.retrieveMessages(for: self.accessedTraceId.warningLevel, healthDepartment: self.accessedTraceId.healthDepartmentId) }
            .map { $0.title ?? "" }
            .asDriver(onErrorJustReturn: "Subtitle")
    }

    var body: Driver<String> {
        notificationConfigSource.retrieve()
            .compactMap { $0.first }
            .asObservable()
            .map { $0.retrieveMessages(for: self.accessedTraceId.warningLevel, healthDepartment: self.accessedTraceId.healthDepartmentId) }
            .map { $0.message ?? "" }
            .asDriver(onErrorJustReturn: "Body")
    }

    func setAsRead() -> Completable {
        if self.accessedTraceId.isRead {
            return Completable.empty()
        }
        return Single.from {
            var temp = self.accessedTraceId
            temp.readDate = Date()
            return temp
        }
        .flatMap { self.accessedRepo.store(object: $0) }
        .asCompletable()
    }
}
