import Foundation
import RxSwift
import UIKit
import DependencyInjection

/// Service for the checkout notification which triggers to remind you to checkout from the location.
public class NotificationService: Toggleable {
    @InjectStatic(\.lucaPreferences) private var lucaPreferences

    private static let repeatingCheckoutNotification = "repeatingCheckoutNotification"
    private static let checkoutNotification = "checkoutNotification"
    private let notificationCenter = UNUserNotificationCenter.current()
    @InjectStatic(\.traceIdService) private var traceIdService: TraceIdService
    @InjectStatic(\.autoCheckoutService) private var autoCheckoutService: AutoCheckoutService

    private var disposeBag: DisposeBag?
    var isEnabled: Bool { disposeBag != nil }

    init() {
        // Trigger delegate setting
        print(NotificationPermissionHandler.shared.currentPermission)
    }

    /// Enables automatic check out and check in events listening
    func enable() {
        guard !isEnabled else {
            return
        }

        let newDisposeBag = DisposeBag()
        Observable.merge(
                traceIdService.isCurrentlyCheckedIn.asObservable(),
                traceIdService.isCurrentlyCheckedInChanges
            )
            .distinctUntilChanged()
            .flatMapLatest { isCheckedIn in
                self.autoCheckoutService.isFeatureFullyWorking
                    .flatMap { autoCheckoutRunning -> Completable in
                        if isCheckedIn && !autoCheckoutRunning {
                            return self.addNotification()
                        } else {
                            return self.removePendingNotifications()
                        }
                    }
            }
            .subscribe()
            .disposed(by: newDisposeBag)
        self.disposeBag = newDisposeBag
    }

    /// Disables automatic events listening
    func disable() {
        disposeBag = nil
    }

    private func addNotification() -> Completable {
        lucaPreferences
            .get(\.checkoutNotificationScheduled)
            .flatMapCompletable { checkoutNotificationScheduled in
                if checkoutNotificationScheduled { return Completable.empty() }
                return Completable.from {
                    let content = UNMutableNotificationContent()
                    content.title = L10n.Notification.Checkout.title
                    content.body = L10n.Notification.Checkout.description
                    content.sound = UNNotificationSound.default
                    // Send notification every two hours.
                    let repeatingTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 2 * 60 * 60, repeats: true)

                    // Send notification after five minutes.
                    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 60 * 5, repeats: false)

                    let repeatingNotificationRequest = UNNotificationRequest(identifier: Self.repeatingCheckoutNotification, content: content, trigger: repeatingTrigger)
                    let notificationRequest = UNNotificationRequest(identifier: Self.checkoutNotification, content: content, trigger: trigger)

                    self.notificationCenter.add(repeatingNotificationRequest)
                    self.notificationCenter.add(notificationRequest)
                }
                .andThen(self.lucaPreferences.set(\.checkoutNotificationScheduled, value: true))
            }
    }

    func removePendingNotifications() -> Completable {
        Completable.from {
            self.notificationCenter.removePendingNotificationRequests(
                withIdentifiers: [Self.repeatingCheckoutNotification, Self.checkoutNotification]
            )
            self.notificationCenter.removeDeliveredNotifications(
                withIdentifiers: [Self.repeatingCheckoutNotification, Self.checkoutNotification ]
            )
        }
        .andThen(lucaPreferences.set(\.checkoutNotificationScheduled, value: false))
    }

    func removePendingNotificationsIfNotCheckedIn() -> Completable {
        traceIdService.isCurrentlyCheckedIn
            .observe(on: MainScheduler.instance)
            .flatMapCompletable { checkedIn in
                if !checkedIn { return self.removePendingNotifications() }
                return Completable.empty()
            }
    }

}
