import Foundation
import DependencyInjection

class BackendAddressV4: BackendAddressV3 {
    init() {
        super.init(version: "v4")
    }
}

class BackendAccessDataV4 {
    @InjectStatic(\.backendAddressV4) private var backendAddress

    func activeChunk() -> AsyncDataOperation<BackendError<FetchAccessedTracesErrorV4>, AccessedTracesDataChunk> {
        FetchAccessedTracesV4AsyncDataOperation(backendAddress: backendAddress)
    }
    func archivedChunk(chunkId: Data) -> AsyncDataOperation<BackendError<FetchAccessedTracesArchivedChunkError>, AccessedTracesDataChunk> {
        FetchAccessedTracesArchivedChunkAsyncDataOperation(backendAddress: backendAddress, chunkId: chunkId)
    }
    func fetchNotificationConfig() -> AsyncDataOperation<BackendError<FetchNotificationConfigError>, NotificationConfig> {
        FetchNotificationConfigBinaryAsyncOperation(backendAddress: backendAddress)
    }
}
