import Foundation

enum UserTransferError: RequestError {
    case invalidInput
    case invalidSignature
    case notFound
    case unableToBuildUserTransferData(error: Error)
}

extension UserTransferError {
    var errorDescription: String? {
        return "\(self)"
    }
    var localizedTitle: String {
        return L10n.Navigation.Basic.error
    }
}

class UserTransferAsyncOperationV3: MappedBackendAsyncDataOperation<UserTransferDataV3, String, UserTransferError> {

    private var build: ()throws->UserTransferDataV3

    init(backendAddress: BackendAddressV3, userTransferBuilder: UserTransferBuilderV3, userId: UUID, numberOfDays: Int) {

        build = { try userTransferBuilder.build(userId: userId, numberOfDays: numberOfDays) }

        let fullUrl = backendAddress.apiUrl
            .appendingPathComponent("userTransfers")

        super.init(url: fullUrl,
                   method: .post,
                   errorMappings: [400: .invalidInput,
                                   403: .invalidSignature,
                                   404: .notFound])
    }

    override func execute(completion: @escaping (String) -> Void, failure: @escaping (BackendError<UserTransferError>) -> Void) -> (() -> Void) {

        do {
            parameters = try build()
        } catch let error {
            failure(BackendError(backendError: .unableToBuildUserTransferData(error: error)))
            return {}
        }
        return super.execute(completion: completion, failure: failure)
    }

    override func map(dict: [String: Any]) throws -> String {
        if let tan = dict["tan"] as? String {
            return tan
        }
        throw NetworkError.invalidResponsePayload
    }
}
