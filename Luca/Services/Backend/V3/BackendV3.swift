import Foundation
import Alamofire
import DependencyInjection

class BackendAddressV3: BackendAddress {

    var host: URL
    var dataHost: URL
    let apiUrl: URL
    let dataUrl: URL

    var privacyPolicyUrl: URL? = URL(string: "https://www.luca-app.de/app-privacy-policy/")

    init(version: String) {

#if DEVELOPMENT
    host = URL(string: "https://app-dev.luca-app.de")!
#elseif QA
    host = URL(string: "https://app-qs.luca-app.de")!
#elseif PENTEST
    host = URL(string: "https://app-pentest.luca-app.de")!
#elseif PREPROD
    host = URL(string: "https://app-preprod.luca-app.de")!
#elseif RELEASE
    host = URL(string: "https://app-release.luca-app.de")!
#elseif HOTFIX
    host = URL(string: "https://app-hotfix.luca-app.de")!
#elseif AQS
    host = URL(string: "https://app-aqs.luca-app.de")!
#elseif P1
    host = URL(string: "https://app-p1.luca-app.de")!
#elseif P2
    host = URL(string: "https://app-p2.luca-app.de")!
#elseif P3
    host = URL(string: "https://app-p3.luca-app.de")!
#elseif DEMO
    host = URL(string: "https://app-demo.luca-app.de")!
#else
    host = URL(string: "https://app.luca-app.de")!
#endif

#if DEVELOPMENT
    dataHost = URL(string: "https://data-dev.luca-app.de")!
#elseif QA
    dataHost = URL(string: "https://data-qs.luca-app.de")!
#elseif PENTEST
    dataHost = URL(string: "https://data-pentest.luca-app.de")!
#elseif PREPROD
    dataHost = URL(string: "https://data-preprod.luca-app.de")!
#elseif RELEASE
    dataHost = URL(string: "https://data-release.luca-app.de")!
#elseif HOTFIX
    dataHost = URL(string: "https://data-hotfix.luca-app.de")!
#elseif AQS
    dataHost = URL(string: "https://data-aqs.luca-app.de")!
#elseif P1
    dataHost = URL(string: "https://data-p1.luca-app.de")!
#elseif P2
    dataHost = URL(string: "https://data-p2.luca-app.de")!
#elseif P3
    dataHost = URL(string: "https://data-p3.luca-app.de")!
#elseif DEMO
    dataHost = URL(string: "https://data-demo.luca-app.de")!
#else
    dataHost = URL(string: "https://data.luca-app.de")!
#endif

        apiUrl = host.appendingPathComponent("api").appendingPathComponent(version)
        dataUrl = dataHost.appendingPathComponent("api").appendingPathComponent(version)
    }

    convenience init() {
        self.init(version: "v3")
    }
}

class DefaultBackendDailyKeyV3: BackendDailyKeyV3 {

    @InjectStatic(\.backendAddressV3) private var backendAddress: BackendAddressV3

    func retrieveDailyPubKey() -> AsyncDataOperation<BackendError<RetrieveDailyKeyError>, PublicKeyFetchResultV3> {
        RetrieveDailyKeyAsyncOperationV3(backendAddress: backendAddress)
    }

    func retrievePubKey(keyId: Int) -> AsyncDataOperation<BackendError<RetrieveDailyKeyError>, PublicKeyFetchResultV3> {
        RetrieveKeyAsyncOperationV3(backendAddress: backendAddress, keyId: keyId)
    }

    func retrieveIssuerKeys(issuerId: String) -> AsyncDataOperation<BackendError<RetrieveDailyKeyError>, IssuerKeysFetchResultV3> {
        RetrieveIssuerKeysAsyncOperationV3(backendAddress: backendAddress, issuerId: issuerId)
    }
}

class BackendUserV3: BackendUserV3Protocol {

    @InjectStatic(\.backendAddressV3) private var backendAddress: BackendAddressV3
    @InjectStatic(\.userDataPackageBuilderV3) private var userDataBuilder: UserDataPackageBuilderV3
    @InjectStatic(\.userTransferBuilderV3) private var userTransferBuilder: UserTransferBuilderV3

    func create(userData: UserRegistrationData) -> AsyncDataOperation<BackendError<CreateUserError>, UUID> {
        CreateUserAsyncOperationV3(backendAddress: backendAddress, builder: userDataBuilder, data: userData)
    }

    func update(userId: UUID, userData: UserRegistrationData) -> AsyncOperation<BackendError<UpdateUserError>> {
        UpdateUserAsyncOperationV3(backendAddress: backendAddress, builder: userDataBuilder, data: userData, userId: userId)
    }

    func userTransfer(userId: UUID, numberOfDays: Int) -> AsyncDataOperation<BackendError<UserTransferError>, String> {
        UserTransferAsyncOperationV3(backendAddress: backendAddress, userTransferBuilder: userTransferBuilder, userId: userId, numberOfDays: numberOfDays)
    }

    func delete(userId: UUID) -> AsyncOperation<BackendError<DeleteUserError>> {
        DeleteUserAsyncOperationV3(backendAddress: backendAddress, userId: userId, builder: userDataBuilder)
    }

}

class BackendTraceIdV3: BackendTraceIdV3Protocol {

    @InjectStatic(\.backendAddressV3) private var backendAddress: BackendAddressV3
    @InjectStatic(\.checkInPayloadBuilderV3) private var checkInBuilder: CheckInPayloadBuilderV3
    @InjectStatic(\.checkOutPayloadBuilderV3) private var checkOutBuilder: CheckOutPayloadBuilderV3
    @InjectStatic(\.traceIdAdditionalBuilderV3) private var additionalDataBuilder: TraceIdAdditionalDataBuilderV3

    func checkIn(qrCode: QRCodePayloadV3, venuePubKey: KeySource, scannerId: String, anonymous: Bool) -> AsyncOperation<BackendError<CheckInError>> {
        CheckInAsyncOperationV3(
            backendAddress: backendAddress,
            checkInBuilder: checkInBuilder,
            qrCodePayload: qrCode,
            venuePubKey: venuePubKey,
            scannerId: scannerId,
            anonymous: anonymous)
    }

    func fetchInfo(traceId: TraceId) -> AsyncDataOperation<BackendError<FetchTraceInfoError>, TraceInfo> {
        FetchTraceInfoRequestAsyncOperation(backendAddress: backendAddress, traceId: traceId)
    }

    func fetchInfo(traceIds: [TraceId]) -> AsyncDataOperation<BackendError<FetchTraceInfoError>, [TraceInfo]> {
        FetchTraceInfosRequestAsyncOperation(backendAddress: backendAddress, traceIds: traceIds)
    }

    func checkOut(traceId: TraceId, timestamp: Date) -> AsyncOperation<BackendError<CheckOutError>> {
        CheckOutAsyncOperationV3(backendAddress: backendAddress, checkOutBuilder: checkOutBuilder, traceId: traceId, timestamp: timestamp)
    }

    func updateAdditionalData<T>(traceId: TraceId, scannerId: String, venuePubKey: KeySource, additionalData: T) -> AsyncOperation<BackendError<UploadAdditionalDataError>> where T: Encodable {

        UploadAdditionalDataRequestAsyncOperationV3(
            backendAddress: backendAddress,
            additionalDataBuilder: additionalDataBuilder,
            traceId: traceId,
            scannerId: scannerId,
            venuePubKey: venuePubKey,
            additionalData: additionalData)
    }
}

class BackendLocationV3: BackendLocation {

    @InjectStatic(\.backendAddressV3) private var backendAddress: BackendAddressV3

    func fetchLocation(locationId: UUID) -> AsyncDataOperation<BackendError<FetchLocationError>, Location> {
        FetchLocationAsyncOperation(backendAddress: backendAddress, locationId: locationId)
    }

    func createPrivateMeeting(publicKey: SecKey) -> AsyncDataOperation<BackendError<CreatePrivateMeetingError>, PrivateMeetingIds> {
        CreatePrivateMeetingAsyncOperation(backendAddress: backendAddress, publicKey: publicKey)
    }

    func fetchLocationGuests(accessId: String) -> AsyncDataOperation<BackendError<FetchLocationGuestsError>, [PrivateMeetingGuest]> {
        FetchGuestListAsyncOperation(backendAddress: backendAddress, accessId: accessId)
    }

    func deletePrivateMeeting(accessId: String) -> AsyncOperation<BackendError<DeletePrivateMeetingError>> {
        DeletePrivateMeetingAsyncOperation(backendAddress: backendAddress, accessId: accessId)
    }
}
