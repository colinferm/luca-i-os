import Foundation
import RxSwift
import DependencyInjection

class AccessDataChunkHandler {
    @InjectStatic(\.backendAccessDataV4) private var backendAccessData
    @InjectStatic(\.keyValueRepo) private var keyValueRepo: KeyValueRepoProtocol
    @InjectStatic(\.accessedTracesDataChunkCachedDataSource) private var accessDataSource
    private let cacheKey = "AccessDataChunkCache"
    typealias DataCache = [String: Data]

    func fetch() -> Single<[AccessedTracesDataChunk]> {
        accessDataSource.retrieve()
            .asObservable()
            .compactMap { $0.first }
            .flatMap { activeChunk -> Observable<AccessedTracesDataChunk> in
                Observable.merge(
                    Observable.just(activeChunk),
                    self.fetchRecurrency(startWith: activeChunk.previousHash)
                )
            }
            .toArray()
    }

    private func fetchRecurrency(startWith chunkId: Data) -> Observable<AccessedTracesDataChunk> {
        getOrFetchChunk(withId: chunkId)
            .asObservable()
            .catch { error in

                // If it's notFound, it reached the end of the buffer. Otherwise its a legit error which shouldn't be suppressed.
                if let backendError = error as? BackendError<FetchAccessedTracesArchivedChunkError>,
                   backendError.backendError == .notFound {
                    return Observable.empty()
                }

                // Do not break the stream only because one chunk failed
                return .empty()
            }
            .flatMap { chunk -> Observable<AccessedTracesDataChunk> in
                Observable.merge(Observable.just(chunk), self.fetchRecurrency(startWith: chunk.previousHash))
            }
    }

    private func getOrFetchChunk(withId chunkId: Data) -> Single<AccessedTracesDataChunk> {
        getChunkFromCache(withId: chunkId).ifEmpty(switchTo: fetchChunk(withId: chunkId))
    }

    private func getChunkFromCache(withId chunkId: Data) -> Maybe<AccessedTracesDataChunk> {
        keyValueRepo.load(cacheKey)
            .catch { _ in Single.just([:]) }
            .asObservable()
            .flatMap { (cache: DataCache) -> Observable<AccessedTracesDataChunk> in
                if let found = cache[self.key(for: chunkId)],
                   let chunk = try? AccessedTracesDataChunk(data: found) {
                    return Observable.just(chunk)
                }
                return Observable.empty()
            }
            .asMaybe()
    }

    private func fetchChunk(withId chunkId: Data) -> Single<AccessedTracesDataChunk> {
        return backendAccessData.archivedChunk(chunkId: chunkId).asSingle()
            .flatMap { chunk in
                self.saveToCache(chunk: chunk, id: chunkId).andThen(Single.just(chunk))
            }
    }

    private func saveToCache(chunk: AccessedTracesDataChunk, id: Data) -> Completable {
        keyValueRepo.load(self.cacheKey)
            .catch { _ in Single.just([:]) }
            .flatMapCompletable { (cache: DataCache) in
                var mutableCache = cache
                mutableCache[self.key(for: id)] = chunk.originalData
                return self.keyValueRepo.store(self.cacheKey, value: mutableCache)
            }
    }

    private func key(for dataChunkId: Data) -> String {
        dataChunkId.hexString
    }
}

extension AccessDataChunkHandler: UnsafeAddress, LogUtil {}
