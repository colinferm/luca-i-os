import Foundation
import RxSwift
import DependencyInjection

// MARK: - Key path entries
struct PreferencesEntry<T> {
    var key: String
    var type: T.Type
}

struct UserRegistrationDataPreferencesEntry<T> {
    var type: T.Type
    var keyPath: KeyPath<UserRegistrationData, T>
}

struct PreferencesEntryWithDefaultValue<T> {
    var key: String
    var type: T.Type
    var defaultValue: T
}

// MARK: - Implementation
extension LucaPreferences {
    var userRegistrationData: PreferencesEntry<UserRegistrationData> {
        PreferencesEntry(key: "userRegistrationData", type: UserRegistrationData.self)
    }
    var uuid: PreferencesEntry<UUID> {
        PreferencesEntry(key: "userId", type: UUID.self)
    }
    var currentOnboardingPage: PreferencesEntryWithDefaultValue<Int> {
        PreferencesEntryWithDefaultValue(key: "currentOnboardingPage", type: Int.self, defaultValue: 0)
    }
    var onboardingComplete: PreferencesEntryWithDefaultValue<Bool> {
        PreferencesEntryWithDefaultValue(key: "onboardingComplete", type: Bool.self, defaultValue: false)
    }
    var welcomePresented: PreferencesEntryWithDefaultValue<Bool> {
        PreferencesEntryWithDefaultValue(key: "welcomePresented", type: Bool.self, defaultValue: false)
    }
    var donePresented: PreferencesEntryWithDefaultValue<Bool> {
        PreferencesEntryWithDefaultValue(key: "donePresented", type: Bool.self, defaultValue: false)
    }
    var dataPrivacyPresented: PreferencesEntryWithDefaultValue<Bool> {
        PreferencesEntryWithDefaultValue(key: "dataPrivacyPresented", type: Bool.self, defaultValue: false)
    }
    var phoneNumberVerified: PreferencesEntryWithDefaultValue<Bool> {
        PreferencesEntryWithDefaultValue(key: "phoneNumberVerified", type: Bool.self, defaultValue: false)
    }
    var termsAcceptedVersion: PreferencesEntryWithDefaultValue<Int> {
        PreferencesEntryWithDefaultValue(key: "termsAcceptedVersion", type: Int.self, defaultValue: 0)
    }
    var verificationRequests: PreferencesEntryWithDefaultValue<[PhoneNumberVerificationRequest]> {
        PreferencesEntryWithDefaultValue(key: "verificationRequests", type: [PhoneNumberVerificationRequest].self, defaultValue: [])
    }
    var checkoutNotificationScheduled: PreferencesEntryWithDefaultValue<Bool> {
        PreferencesEntryWithDefaultValue(key: "checkoutNotificationScheduled", type: Bool.self, defaultValue: false)
    }
    var appStoreReviewCheckoutCounter: PreferencesEntryWithDefaultValue<Int> {
        PreferencesEntryWithDefaultValue(key: "appStoreReviewCheckoutCounter", type: Int.self, defaultValue: 0)
    }

    fileprivate var migrated: PreferencesEntryWithDefaultValue<Bool> {
        PreferencesEntryWithDefaultValue(key: "migrated", type: Bool.self, defaultValue: false)
    }
}

class LucaPreferences {
    @InjectStatic(\.importantDataKeyValueRepo) private var keyValueRepo

    private let mainKey = "LucaPreferences."
    let publisher = PublishSubject<(keyPathHash: Int, newValue: Any?)>()

    init() {
        print("LucaPreferences.init")
    }

    deinit {
        print("LucaPreferences.deinit")
    }

    // MARK: - Methods with default values
    func set<T>(_ key: KeyPath<LucaPreferences, PreferencesEntryWithDefaultValue<T>>, value: T) -> Completable where T: Codable {
        keyValueRepo.store(mainKey + self[keyPath: key].key, value: value)
            .andThen(Completable.from {
                self.publisher.onNext((keyPathHash: key.hashValue, newValue: value))
            })
    }
    func get<T>(_ key: KeyPath<LucaPreferences, PreferencesEntryWithDefaultValue<T>>) -> Single<T> where T: Codable {
        keyValueRepo.load(mainKey + self[keyPath: key].key, defaultValue: self[keyPath: key].defaultValue)
    }
    func remove<T>(_ key: KeyPath<LucaPreferences, PreferencesEntryWithDefaultValue<T>>) -> Completable where T: Codable {
        keyValueRepo.remove(mainKey + self[keyPath: key].key)
            .andThen(Completable.from {
                self.publisher.onNext((keyPathHash: key.hashValue, newValue: self[keyPath: key].defaultValue))
            })
    }

    /// Emits next value on every update
    func changes<T>(_ key: KeyPath<LucaPreferences, PreferencesEntryWithDefaultValue<T>>) -> Observable<T> {
        let defaultValue = self[keyPath: key].defaultValue
        return publisher
            .filter { $0.keyPathHash == key.hashValue }
            .map { $0.newValue as? T }
            .map { $0 ?? defaultValue }
    }

    /// Emits next value on nevery update and the current one on subscription
    func currentAndChanges<T>(_ key: KeyPath<LucaPreferences, PreferencesEntryWithDefaultValue<T>>) -> Observable<T> where T: Codable {
        Observable.merge(get(key).asObservable(), changes(key))
    }

    // MARK: - Methods without default values
    func set<T>(_ key: KeyPath<LucaPreferences, PreferencesEntry<T>>, value: T) -> Completable where T: Codable {
        keyValueRepo.store(mainKey + self[keyPath: key].key, value: value)
            .andThen(Completable.from {
                self.publisher.onNext((keyPathHash: key.hashValue, newValue: value))
            })
    }
    func get<T>(_ key: KeyPath<LucaPreferences, PreferencesEntry<T>>) -> Single<T?> where T: Codable {
        keyValueRepo.loadOptional(mainKey + self[keyPath: key].key)
    }
    func remove<T>(_ key: KeyPath<LucaPreferences, PreferencesEntry<T>>) -> Completable where T: Codable {
        keyValueRepo.remove(mainKey + self[keyPath: key].key)
            .andThen(Completable.from {
                self.publisher.onNext((keyPathHash: key.hashValue, newValue: nil))
            })
    }

    /// Emits next value on every update
    func changes<T>(_ key: KeyPath<LucaPreferences, PreferencesEntry<T>>) -> Observable<T?> where T: Codable {
        publisher.filter { $0.keyPathHash == key.hashValue }.map { $0.newValue as? T }
    }

    /// Emits next value on nevery update and the current one on subscription
    func currentAndChanges<T>(_ key: KeyPath<LucaPreferences, PreferencesEntry<T>>) -> Observable<T?> where T: Codable {
        Observable.merge(get(key).asObservable(), changes(key))
    }
}

// MARK: - Shortcut methods for working with user registration data
extension LucaPreferences {
    private var getOrCreateUserRegistrationData: Single<UserRegistrationData> {
        self.get(\.userRegistrationData).map { $0 ?? UserRegistrationData() }
    }
    func get<T>(_ key: KeyPath<UserRegistrationData, T>) -> Single<T> where T: Codable {
        getOrCreateUserRegistrationData.map { $0[keyPath: key] }
    }
    func set<T>(_ key: WritableKeyPath<UserRegistrationData, T>, value: T) -> Completable where T: Codable {
        getOrCreateUserRegistrationData.flatMapCompletable {
            var copy = $0
            copy[keyPath: key] = value
            return self.set(\.userRegistrationData, value: copy)
        }
        .andThen(Completable.from { self.publisher.onNext((keyPathHash: key.hashValue, newValue: value)) })
    }

    /// Emits next value on every update
    func changes<T>(_ key: KeyPath<UserRegistrationData, T>) -> Observable<T> where T: Codable {
        publisher.filter { $0.keyPathHash == key.hashValue }.compactMap { $0.newValue as? T }
    }

    /// Emits next value on nevery update and the current one on subscription
    func currentAndChanges<T>(_ key: KeyPath<UserRegistrationData, T>) -> Observable<T> where T: Codable {
        Observable.merge(get(key).asObservable(), changes(key))
    }
}

extension LucaPreferences {
    func migrate(from: OldLucaPreferences) -> Completable {
        var completables: [Completable] = []
        if let userData = from.userRegistrationData {
            completables.append(set(\.userRegistrationData, value: userData).debug("Migration: Setting user data"))
        }
        if let uuid = from.uuid {
            completables.append(set(\.uuid, value: uuid).debug("Migration: Setting UUID"))
        }
        if let value = from.currentOnboardingPage {
            completables.append(set(\.currentOnboardingPage, value: value).debug("Migration: Setting currentOnboardingPage"))
        }
        completables.append(set(\.onboardingComplete, value: from.onboardingComplete).debug("Migration: Setting Onboarding complete"))

        completables.append(set(\.welcomePresented, value: from.welcomePresented).debug("Migration: Setting welcomePresented"))

        completables.append(set(\.donePresented, value: from.donePresented).debug("Migration: Setting donePresented"))

        completables.append(set(\.dataPrivacyPresented, value: from.dataPrivacyPresented).debug("Migration: Setting dataPrivacyPresented"))

        completables.append(set(\.phoneNumberVerified, value: from.phoneNumberVerified).debug("Migration: Setting phoneNumberVerified"))

        completables.append(set(\.termsAcceptedVersion, value: from.termsAcceptedVersion).debug("Migration: Setting termsAcceptedVersion"))

        completables.append(set(\.verificationRequests, value: from.verificationRequests).debug("Migration: Setting verificationRequests"))

        completables.append(set(\.checkoutNotificationScheduled, value: from.checkoutNotificationScheduled).debug("Migration: Setting checkoutNotificationScheduled"))

        completables.append(set(\.appStoreReviewCheckoutCounter, value: from.appStoreReviewCheckoutCounter).debug("Migration: Setting appStoreReviewCheckoutCounter"))

        completables.append(set(\.migrated, value: true))

        return get(\.migrated).flatMapCompletable { migrated in
            if !migrated {
                return Completable.zip(completables)
            }
            return Completable.empty()
        }
    }
}

// MARK: - Sync helpers
extension LucaPreferences {
    /// Contains blocking methods without async stuff.
    ///
    /// WARNING: Those methods can take up to 200 ms each in worst case if the cache is invalid! Use it with care and don't abuse it in interaction critical moments! If the cache is valid though, the typical retrieval should take ~1ms
    var blocking: BlockingMethods { BlockingMethods(parent: self) }
}

struct BlockingMethods {
    private var parent: LucaPreferences
    init(parent: LucaPreferences) {
        self.parent = parent
    }
    // MARK: Methods with default values
    func set<T>(_ key: KeyPath<LucaPreferences, PreferencesEntryWithDefaultValue<T>>, value: T) where T: Codable {
        try? parent.set(key, value: value).blockingWait()
    }
    func get<T>(_ key: KeyPath<LucaPreferences, PreferencesEntryWithDefaultValue<T>>) -> T where T: Codable {
        (try? parent.get(key).retrieveBlocking()) ?? parent[keyPath: key].defaultValue
    }
    func remove<T>(_ key: KeyPath<LucaPreferences, PreferencesEntryWithDefaultValue<T>>) where T: Codable {
        try? parent.remove(key).blockingWait()
    }

    // MARK: Methods without default values
    func set<T>(_ key: KeyPath<LucaPreferences, PreferencesEntry<T>>, value: T) where T: Codable {
        try? parent.set(key, value: value).blockingWait()
    }
    func get<T>(_ key: KeyPath<LucaPreferences, PreferencesEntry<T>>) -> T? where T: Codable {
        try? parent.get(key).retrieveBlocking()
    }
    func remove<T>(_ key: KeyPath<LucaPreferences, PreferencesEntry<T>>) where T: Codable {
        try? parent.remove(key).blockingWait()
    }

    // MARK: UserRegistrationData shortcuts
    func get<T>(_ key: KeyPath<UserRegistrationData, T>) -> T? where T: Codable {
        try? parent.get(key).retrieveBlocking()
    }
    func set<T>(_ key: ReferenceWritableKeyPath<UserRegistrationData, T>, value: T) where T: Codable {
        try? parent.set(key, value: value).blockingWait()
    }
}

infix operator ??? : NilCoalescingPrecedence
func ???<T>(lhs: T??, rhs: T) -> T {
    (lhs ?? rhs) ?? rhs
}
func ???<T>(lhs: T???, rhs: T) -> T {
    ((lhs ?? rhs) ?? rhs) ?? rhs
}
