import Foundation
import RxSwift

class OldLucaPreferences {

    static let shared = OldLucaPreferences()

    private let preferences: UserDataPreferences

    init() {
        preferences = UserDataPreferences(suiteName: "LucaPreferences")
    }

    var currentOnboardingPage: Int? {
        get {
            preferences.retrieve(key: "currentOnboardingPage")
        }
        set {
            preferences.store(newValue!, key: "currentOnboardingPage")
        }
    }

    var userRegistrationData: UserRegistrationData? {
        get {
            preferences.retrieve(key: "userRegistrationData", type: UserRegistrationData.self)
        }
        set {
            if let value = newValue {
                preferences.store(value, key: "userRegistrationData")
            } else {
                preferences.remove(key: "userRegistrationData")
            }
        }
    }

    var uuid: UUID? {
        get {
            preferences.retrieve(key: "uuid")
        }
        set {
            // Need to unwrap otherwise the store function for Data is used.
            if let value = newValue {
                preferences.store(value, key: "uuid")
            } else {
                preferences.remove(key: "uuid")
            }
        }
    }

    var onboardingComplete: Bool {
        get {
            preferences.retrieve(key: "onboardingComplete") ?? false
        }
        set {
            preferences.store(newValue, key: "onboardingComplete")
        }
    }

    var welcomePresented: Bool {
        get {
            preferences.retrieve(key: "welcomePresented") ?? false
        }
        set {
            preferences.store(newValue, key: "welcomePresented")
        }
    }

    var donePresented: Bool {
        get {
            preferences.retrieve(key: "donePresented") ?? false
        }
        set {
            preferences.store(newValue, key: "donePresented")
        }
    }

    var termsAcceptedVersion: Int {
        get {
            preferences.retrieve(key: "termsAcceptedVersion") ?? 0
        } set {
            preferences.store(newValue, key: "termsAcceptedVersion")
        }
    }

    var dataPrivacyPresented: Bool {
        get {
            preferences.retrieve(key: "dataPrivacyPresented") ?? false
        } set {
            preferences.store(newValue, key: "dataPrivacyPresented")
        }
    }

    var phoneNumberVerified: Bool {
        get {
            preferences.retrieve(key: "phoneNumberVerified") ?? false
        }
        set {
            preferences.store(newValue, key: "phoneNumberVerified")
        }
    }

    var autoCheckout: Bool {
        get {
            preferences.retrieve(key: "autoCheckout") ?? false
        }
        set {
            preferences.store(newValue, key: "autoCheckout")
        }
    }

    var verificationRequests: [PhoneNumberVerificationRequest] {
        get {
            preferences.retrieve(key: "phoneVerificationRequest", type: [PhoneNumberVerificationRequest].self) ?? []
        }
        set {
            preferences.store(newValue, key: "phoneVerificationRequest")
        }
    }

    var checkoutNotificationScheduled: Bool {
        get {
            preferences.retrieve(key: "checkoutNotificationScheduled") ?? false
        }
        set {
            preferences.store(newValue, key: "checkoutNotificationScheduled")
        }
    }

    var appStoreReviewCheckoutCounter: Int {
        get {
            preferences.retrieve(key: "appStoreReviewCheckoutCounter") ?? 0
        }
        set {
            preferences.store(newValue, key: "appStoreReviewCheckoutCounter")
        }
    }

}
