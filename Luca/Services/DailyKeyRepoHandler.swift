import Foundation
import RxSwift
import RxCocoa
import DependencyInjection

enum DailyKeyRepoHandlerError: LocalizedTitledError {
    case keyNotSaved(error: Error)
    case backendError(error: BackendError<RetrieveDailyKeyError>)
    case validationFailed
    case timeOutOfSyncFailed
}

extension DailyKeyRepoHandlerError {
    var errorDescription: String? {
        switch self {
        case .keyNotSaved(let error):
            return L10n.DailyKey.Fetch.FailedToSave.message(error.localizedDescription)
        case .backendError:
            return L10n.DailyKey.Fetch.FailedToDownload.message
        case .validationFailed:
            return L10n.DailyKey.Fetch.FailedToSave.message("Signature is not valid")
        case .timeOutOfSyncFailed:
            return L10n.DailyKey.Fetch.FailedToSave.message("Signature is not valid")
        }
    }

    var localizedTitle: String {
        return L10n.Navigation.Basic.error
    }
}

class DailyKeyRepoHandler: Toggleable {
    @InjectStatic(\.keyValueRepo) private var keyValueRepo: KeyValueRepoProtocol
    @InjectStatic(\.dailyKeyRepository) private var dailyKeyRepo: DailyPubKeyHistoryRepository
    @InjectStatic(\.backendDailyKeyV3) private var backend: DefaultBackendDailyKeyV3
    @InjectStatic(\.backendMiscV3) private var backendMisc: CommonBackendMisc

    private let numberOfIndicesToKeep = 35
    private let hoursBeforeUpdateNeeded: Double = 24
    private let cacheKey = "DailyKeyRepoHandlerDailyLastFetched"

    private let reachabilityService: ReachabilityServiceKind

    private var disposeBag: DisposeBag!
    private lazy var hasValidDailyPublicKey: BehaviorRelay<Bool> = {
        BehaviorRelay(value: self.verifyDailyKeyNotOutdated())
    }()
    var hasValidDailyPublicKeyDriver: Driver<Bool> {
        return hasValidDailyPublicKey.asDriver()
    }

    var isEnabled: Bool { disposeBag != nil }

    init(reachabilityService: ReachabilityServiceKind) {
        self.reachabilityService = reachabilityService
    }

    func enable() {
        disposeBag = DisposeBag()
        reachabilityService.isReachableDriver
            .asObservable()
            .filter {$0}
            .flatMap { _ in
                self.shouldUpdateKeysRx()
            }
            .filter {$0}
            .flatMapLatest { _ in
                self.fetchRx().onErrorComplete()
            }
            .subscribe()
            .disposed(by: disposeBag)
    }
    func disable() {
        disposeBag = nil
    }

    func fetchRx() -> Completable {
        shouldUpdateKeysRx()
            .asObservable()
            .filter {$0}
            .flatMap { _ in
                self.isSystemTimeGenuine().asObservable()
            }
            .filter {$0}
            .flatMapLatest { (_: Bool) -> Single<PublicKeyFetchResultV3> in
                self.retrieveDailyPubKey()
            }
            .flatMap { result -> Single<PublicKeyFetchResultV3> in
                self.processFetchResult(result: result)
                    .andThen(self.updateDailyKeyRepoRx(with: result))
                    .andThen(.just(result))
            }
            .flatMap { result -> Completable in
                print("[DailyKeyRepoHandler] Key has been successfully fetched: \(result.keyId) \(result.publicKey)")
                self.log("Key has been successfully fetched")
                self.emitDailyPublicKeyValidity(valid: self.verifyDailyKeyNotOutdated())
                return self.keyValueRepo.store(self.cacheKey, value: Date())
            }
            .ignoreElementsAsCompletable()
    }

    func emitDailyPublicKeyValidity(valid: Bool) {
        self.hasValidDailyPublicKey.accept(valid)
    }

    func retrieveDailyPubKey() -> Single<PublicKeyFetchResultV3> {
        self.backend.retrieveDailyPubKey().asSingle()
    }

    // check if we're not currently fetching and if the last update is more than 24 hours ago
    func shouldUpdateKeysRx() -> Single<Bool> {
        keyValueRepo.load(cacheKey, defaultValue: Date(timeIntervalSince1970: 0))
            .catchAndReturn(Date(timeIntervalSince1970: 0))
            .map { lastFetchDate in
                let interval = TimeUnit.hour(amount: self.hoursBeforeUpdateNeeded).timeInterval
                let outdated = lastFetchDate + interval < Date()
                return outdated
            }
    }

    func processFetchResult(result: PublicKeyFetchResultV3) -> Completable {
        self.fetchIssuerKeysRx(for: result.issuerId)
            .flatMapCompletable({ issuerResult -> Completable in
                let verification = self.verifyRx(thatSignature: result.signature,
                              matchesIssuerWithPublicHDSKP: issuerResult.publicHDSKP,
                              withKeyId: result.keyId,
                              createdAt: result.createdAt,
                              publicKey: result.publicKey)
                let validity = DailyKeyRepoHandler.keyAgeIsValidRx(for: TimeInterval(result.createdAt))

                return Completable.zip(verification, validity)
            })
    }

    func removeAll() -> Completable {
        Completable.from { self.dailyKeyRepo.removeAll() }
        .andThen(keyValueRepo.remove(cacheKey))
        .do(onCompleted: {self.log("Keys have been removed")})
        .logError(self, "Couldn't remove daily keys ")

    }

    func updateDailyKeyRepo(with result: PublicKeyFetchResultV3) throws {
        guard let key = result.parsedKey else {
            throw NSError(domain: "Invalid daily public key", code: 0, userInfo: nil)
        }

        try self.dailyKeyRepo.store(key: key, index: DailyKeyIndex(keyId: result.keyId, createdAt: Date(timeIntervalSince1970: TimeInterval(result.createdAt))))

        // Get only N newest keys and dispose rest
        let allIndices = Array(self.dailyKeyRepo.indices)
        let sortedIndices = allIndices.sorted(by: { $0.createdAt.timeIntervalSince1970 > $1.createdAt.timeIntervalSince1970 })
        let indicesToKeep = sortedIndices.prefix(numberOfIndicesToKeep)
        let indicesToRemove = allIndices.difference(from: Array(indicesToKeep))
        for index in indicesToRemove {
            dailyKeyRepo.remove(index: index)
        }
    }

    private func fetchIssuerKeys(for issuerId: String, completion: @escaping (IssuerKeysFetchResultV3) -> Void, failure: @escaping (DailyKeyRepoHandlerError) -> Void) {
        backend.retrieveIssuerKeys(issuerId: issuerId).execute(completion: { issuerResult in
            DispatchQueue.main.async {
                completion(issuerResult)
            }
        }, failure: { (issuerError) in
            self.log("The issuerKeys couldn't be retrieved: \(issuerError)", entryType: .error)
            DispatchQueue.main.async {
                failure(.backendError(error: issuerError))
            }
        })
    }

    func verify(thatSignature actualSignature: String, matchesIssuerWithPublicHDSKP publicHDSKP: String, withKeyId keyId: Int, createdAt: Int, publicKey: String) -> Bool {
        guard let publicHDSKPData = Data(base64Encoded: publicHDSKP),
              let publicHDSKPKey = try? KeyFactory.create(from: publicHDSKPData, type: .ecsecPrimeRandom, keyClass: .public),
              let publicKey = publicKey.base64ToHex(),
              let signature = Data(base64Encoded: actualSignature) else {
            return false
        }

        let keyIdData = Int32(keyId).data
        let timestampData = Int32(createdAt).data

        var signatureData = keyIdData
        signatureData.append(timestampData)
        signatureData.append(Data(hex: publicKey))

        let signatureVerifier = ECDSA(privateKeySource: nil, publicKeySource: ValueKeySource(key: publicHDSKPKey))

        do {
            let isValidSignature = try signatureVerifier.verify(data: signatureData, signature: signature)
            return isValidSignature
        } catch let error {
            print("Error in checking signature: \(error)")
            return false
        }
    }

    static func keyAgeIsValid(for createdAt: TimeInterval) -> Bool {
        let keyAge = Date().timeIntervalSince1970 - createdAt
        return keyAge <= TimeUnit.day(amount: 7).timeInterval
    }

    static func keyAgeIsValidRx(for createdAt: TimeInterval) -> Completable {
        Completable.create { observer in
            if keyAgeIsValid(for: createdAt) {
                observer(.completed)
            } else {
                observer(.error(DailyKeyRepoHandlerError.validationFailed))
            }
            return Disposables.create()
        }
    }

    func verifyDailyKeyNotOutdated() -> Bool {
        guard let dailyID = self.dailyKeyRepo.newestId else {return false}
        return DailyKeyRepoHandler.keyAgeIsValid(for: dailyID.createdAt.timeIntervalSince1970)
    }

    static func systemTimeEqualsServerTime(servertime: Timesync) -> Bool {
        let isValid = (servertime.unix - 5 * 60) ... (servertime.unix + 5 * 60) ~= Int(Date().timeIntervalSince1970)
        return isValid
    }

    private func verifyRx(thatSignature actualSignature: String, matchesIssuerWithPublicHDSKP publicHDSKP: String, withKeyId keyId: Int, createdAt: Int, publicKey: String) -> Completable {
        Completable.create { [weak self] observer -> Disposable in
            if let self = self, self.verify(thatSignature: actualSignature, matchesIssuerWithPublicHDSKP: publicHDSKP, withKeyId: keyId, createdAt: createdAt, publicKey: publicKey) {
                observer(.completed)
            } else {
                self?.log("The key signature could not be verified", entryType: .error)
                observer(.error(DailyKeyRepoHandlerError.validationFailed))
            }
            return Disposables.create()
        }
    }

    func fetchIssuerKeysRx(for issuerId: String) -> Single<IssuerKeysFetchResultV3> {
        Single.create { [weak self] observer -> Disposable in
            self?.backend.retrieveIssuerKeys(issuerId: issuerId).execute(completion: { issuerResult in
                observer(.success(issuerResult))
            }, failure: { (issuerError) in
                self?.log("The issuerKeys couldn't be retrieved: \(issuerError)", entryType: .error)
                observer(.failure(DailyKeyRepoHandlerError.backendError(error: issuerError)))
            })
            return Disposables.create()
        }
    }

    func isSystemTimeGenuine() -> Single<Bool> {
        self.fetchTimeSync()
            .map({ (time: Timesync) -> Bool in
                return DailyKeyRepoHandler.systemTimeEqualsServerTime(servertime: time)
            })
            .observe(on: MainScheduler.instance)
    }

    func fetchTimeSync() -> Single<Timesync> {
        return backendMisc.fetchTimesync()
            .asSingle()
    }

    func updateDailyKeyRepoRx(with result: PublicKeyFetchResultV3) -> Completable {
        Completable.create { [weak self] observer in
            do {
              try self?.updateDailyKeyRepo(with: result)
                observer(.completed)
            } catch {
                self?.log("The key couldn't be saved: \(error)", entryType: .error)
                observer(.error(DailyKeyRepoHandlerError.keyNotSaved(error: error)))
            }
            return Disposables.create()
        }
    }

}

extension DailyKeyRepoHandler: LogUtil, UnsafeAddress {}
