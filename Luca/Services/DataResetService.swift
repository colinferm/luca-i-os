import Foundation
import DependencyInjection
import RxSwift

class DataResetService {

    let log = GeneralPurposeLog(subsystem: "App", category: "DataResetService", subDomains: [])
    @InjectAllDynamic private var realmDatabaseUtils: [RealmDatabaseUtils]
    @InjectStatic(\.appServicesInitialiser) private var appServicesInitialiser
    @InjectStatic(\.localDBKeyRepository) private var localDBKeyRepository
    @InjectStatic(\.lucaPreferences) private var lucaPreferences
    @InjectStatic(\.userKeysBundle) private var userKeysBundle
    @InjectStatic(\.historyRepo) private var historyRepo
    @InjectStatic(\.userService) private var userService
    @InjectStatic(\.traceIdService) private var traceIdService

    func resetAll() -> Completable {
        userService.deleteUserData().debug("Delete user data")
            .catch {
                if case UserServiceError.userDeletionError(error: let error) = $0,
                   error.backendError == .alreadyDeleted || error.backendError == .userNotFound {
                    return Completable.empty()
                }
                throw $0
            }
            .andThen(traceIdService.disposeData(clearTraceHistory: true))
            .andThen(resetUserData())
            .andThen(disableServices())
            .andThen(Completable.concat(realmDatabaseUtils.map { $0.removeFile() }))
            .andThen(Completable.from {
                UserDataPreferences.removeAllSuites()
            })
            .andThen(reinitializeServices())
    }

    private func disableServices() -> Completable {
        Completable.from { self.appServicesInitialiser.invalidate() }
    }

    private func reinitializeServices() -> Completable {
        Completable.from { try self.appServicesInitialiser.initialise() }
        .logError(log, "Error in invalidating and reinitialisation of services")
    }

    private func resetUserData() -> Completable {
        Completable.from {
            self.userKeysBundle.removeKeys()
            self.localDBKeyRepository.purge()
        }
        .andThen(Completable.from { try KeyStorage.purge() }.logError(log, "Error in purging key storage"))
    }

    func resetHistory() -> Completable {
        historyRepo.removeAll()
    }

}
