import Foundation
import UIKit
import Alamofire
import DependencyInjection
import RxSwift

enum UserServiceError: LocalizedTitledError {
    case localDataIncomplete
    case unableToGenerateKeys(error: Error)
    case networkError(error: NetworkError)
    case dailyKeyRepoError(error: DailyKeyRepoHandlerError)
    case userRegistrationError(error: BackendError<CreateUserError>)
    case userTransferError(error: BackendError<UserTransferError>)
    case userUpdateError(error: BackendError<UpdateUserError>)
    case userDeletionError(error: BackendError<DeleteUserError>)
    case unknown(error: Error)
}

extension UserServiceError {
    var errorDescription: String? {
        switch self {
        case .networkError(let error):
            return error.localizedDescription
        case .dailyKeyRepoError(let error):
            return error.localizedDescription
        case .userRegistrationError(let error):
            return error.localizedDescription
        case .userTransferError(let error):
            return error.localizedDescription
        case .userUpdateError(let error):
            return error.localizedDescription
        default:
            return "\(self)"
        }
    }

    var error: Error? {
        switch self {
        case .localDataIncomplete:
            return self
        case .unableToGenerateKeys(error: let error):
            return error
        case .networkError(let error):
            return error
        case .dailyKeyRepoError(error: let error):
            return error
        case .userRegistrationError(error: let error):
            return error.backendError
        case .userTransferError(error: let error):
            return error.backendError
        case .userUpdateError(error: let error):
            return error.backendError
        case .userDeletionError(error: let error):
            return error.backendError
        case .unknown(error: let error):
            return error
        }
    }

    var localizedTitle: String {
        switch self {
        case .networkError(let error):
            return error.localizedTitle
        case .dailyKeyRepoError(let error):
            return error.localizedTitle
        case .userRegistrationError(let error):
            return error.localizedTitle
        case .userTransferError(let error):
            return error.localizedTitle
        case .userUpdateError(let error):
            return error.localizedTitle
        default:
            return L10n.Navigation.Basic.error
        }
    }
}

class UserService {

    enum Result {
        case userExists
        case userRecreated
    }

    // MARK: - dependencies
    @InjectStatic(\.lucaPreferences) private var lucaPreferences: LucaPreferences
    @InjectStatic(\.backendUserV3) private var backend: BackendUserV3
    @InjectStatic(\.userKeysBundle) private var userKeysBundle: UserKeysBundle
    @InjectStatic(\.dailyKeyRepoHandler) private var dailyKeyRepoHandler: DailyKeyRepoHandler

    // MARK: - events
    public let onUserRegistered = "UserService.onUserRegistered"
    public let onUserUpdated = "UserService.onUserUpdated"
    public let onUserDataTransfered = "UserService.onUserDataTransfered"
    public let onUserDataTransferedNumberOfDays = "UserService.onUserDataTransfered.numberOfDays"

    var isDataComplete: Single<Bool> {
        lucaPreferences.get(\.userRegistrationData).map { $0?.dataComplete ?? false }
    }

    var isPersonalDataComplete: Single<Bool> {
        lucaPreferences.get(\.userRegistrationData).map { $0?.personalDataComplete ?? false }
    }

    /// Updates data in the backend and saves the new object to the local preferences when succeeded.
    /// - Parameters:
    ///   - data: Data to upload
    ///   - completion: Will be called after data is updated and locally saved
    ///   - failure: Will be called when something went wrong
    func update(data: UserRegistrationData, completion: @escaping () -> Void, failure: @escaping (UserServiceError) -> Void) {
        guard data.dataComplete else {
            log("Upload current data: local data incomplete", entryType: .error)
            failure(.localDataIncomplete)
            return
        }

        guard let userId = lucaPreferences.blocking.get(\.uuid) else {
            log("Upload current data: no user id", entryType: .error)
            failure(.localDataIncomplete)
            return
        }
        backend.update(userId: userId, userData: data)
            .execute { [weak self] in
                guard let self = self else {
                    failure(UserServiceError.unknown(error: NSError(domain: "Self not available", code: 0)))
                    return
                }
                _ = self.lucaPreferences.set(\.userRegistrationData, value: data)
                    .do(onCompleted: {
                        NotificationCenter.default.post(Notification(name: Notification.Name(self.onUserUpdated), object: self, userInfo: nil))
                        completion()
                    })
                    .do(onError: { error in
                        if let specificError = error as? BackendError<UpdateUserError> {
                            failure(.userUpdateError(error: specificError))
                        } else {
                            failure(.unknown(error: error))
                        }
                    })
                    .subscribe()

            } failure: { [weak self] error in
                self?.log("Upload current data error: \(error)", entryType: .error)
                failure(.userUpdateError(error: error))
            }
    }

    func deleteUserData(completion: @escaping () -> Void, failure: @escaping (UserServiceError) -> Void) {
        guard let userId = retrieveUserID() else {
            log("Delete user: no user id", entryType: .error)
            failure(.localDataIncomplete)
            return
        }
        backend.delete(userId: userId).execute {
            completion()
        } failure: { [weak self] error in
            self?.log("User data deletion error: \(error)", entryType: .error)
            failure(.userDeletionError(error: error))
        }
    }

    func transferUserData(forNumberOfDays numberOfDays: Int, completion: @escaping (String) -> Void, failure: @escaping (UserServiceError) -> Void) {
        guard let userId = retrieveUserID() else {
            log("Upload current data: no user id", entryType: .error)
            failure(.localDataIncomplete)
            return
        }
        backend.userTransfer(userId: userId, numberOfDays: numberOfDays).execute { [weak self] (challengeId) in
            if let unwrappedSelf = self {
                NotificationCenter.default.post(Notification(name: Notification.Name(unwrappedSelf.onUserDataTransfered), object: unwrappedSelf, userInfo: [unwrappedSelf.onUserDataTransferedNumberOfDays: numberOfDays]))
            }
            completion(challengeId)
        } failure: { [weak self] error in
            self?.log("Transfer user data error: \(error)", entryType: .error)
            failure(.userTransferError(error: error))
        }
    }

    func registerIfNeeded() -> Single<Result> {
        lucaPreferences.get(\.userRegistrationData)
            .flatMap { optionalUserRegistrationData in
                guard let userRegistrationData = optionalUserRegistrationData,
                      userRegistrationData.dataComplete else {
                    throw UserServiceError.localDataIncomplete
                }
                return self.lucaPreferences.get(\.uuid)
                    .flatMap { optionalUUID -> Single<Result> in
                        if let uuid = optionalUUID {
                            return .just(.userExists)
                        }
                        return self.registerUser(userData: userRegistrationData)
                            .andThen(.just(.userRecreated))
                    }
            }
    }

    private func registerUser(userData: UserRegistrationData) -> Completable {
        Completable.from {
            self.log("Registering user: regenerating keys")
            try self.userKeysBundle.generateKeys(forceRefresh: true)
            self.log("Registering user: keys regenerated")
        }
        // Delete all daily keys to make sure there are no leftovers from other backend.
        // It should solve the problem with keys mismatch when switching between production and staging
        .andThen(dailyKeyRepoHandler.removeAll())
        .andThen(dailyKeyRepoHandler.fetchRx())
        .andThen(backend.create(userData: userData).asSingle())
        .flatMapCompletable { uuid in
            self.lucaPreferences.set(\.uuid, value: uuid)
                .andThen(self.lucaPreferences.set(\.onboardingComplete, value: true))
        }
        .andThen(Completable.from {
            NotificationCenter.default.post(
                Notification(name: Notification.Name(self.onUserRegistered),
                             object: self,
                             userInfo: nil)
            )
        })
    }

    private func retrieveUserID() -> UUID? {
        return lucaPreferences.blocking.get(\.uuid)
    }

    func userDescription() -> String {
        let userData = lucaPreferences.blocking.get(\.userRegistrationData)
        let name = "\(userData?.firstName ?? "") \(userData?.lastName ?? "")"
        let address = "\(userData?.street ?? "") \(userData?.houseNumber ?? ""), \(userData?.postCode ?? "") \(userData?.city ?? "")"
        let phoneVerified = lucaPreferences.blocking.get(\.phoneNumberVerified) ? "(\(L10n.Verification.PhoneNumber.verified))" : ""
        let phone = "\(userData?.phoneNumber ?? "") \(phoneVerified)"
        let mail = userData?.email ?? "-"

        return L10n.DataReport.ContactData.userData(name, address, phone, mail)
    }
}

extension UserService: LogUtil, UnsafeAddress {}
