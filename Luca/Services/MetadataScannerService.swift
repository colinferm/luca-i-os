import RxSwift
import Foundation
import AVFoundation

enum MetadataScannerServiceError: Error {
    case failedToInitializeCaptureDevice
    case cannotAddVideoInput
    case cannotAddVideoOutput
}

enum MetadataScannerServiceLocalizedError: LocalizedTitledError {
    case captureDeviceNotAuthorized
}

extension MetadataScannerServiceLocalizedError {
    var localizedTitle: String {
        L10n.Camera.Access.title
    }

    var errorDescription: String? {
        switch self {
        case .captureDeviceNotAuthorized:
            return L10n.Camera.Access.description
        }
    }
}

class MetadataScannerService: NSObject {

    private static let sharedDelegate = MetadataScannerService()
    private static let delegateOutputs = PublishSubject<(output: AVCaptureMetadataOutput, didOutput: [AVMetadataObject], from: AVCaptureConnection)>()

    static func createCaptureSession(for metadataObjectTypes: [AVMetadataObject.ObjectType]) -> Single<AVCaptureSession> {
        Single.from {
            guard AVCaptureDevice.authorizationStatus(for: .video) == .authorized else {
                throw MetadataScannerServiceLocalizedError.captureDeviceNotAuthorized
            }
            let captureSession = AVCaptureSession()

            guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else {
                throw MetadataScannerServiceError.failedToInitializeCaptureDevice
            }

            let videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)

            if captureSession.canAddInput(videoInput) {
                captureSession.addInput(videoInput)
            } else {
                throw MetadataScannerServiceError.cannotAddVideoInput
            }

            let metadataOutput = AVCaptureMetadataOutput()

            if captureSession.canAddOutput(metadataOutput) {
                captureSession.addOutput(metadataOutput)
                metadataOutput.setMetadataObjectsDelegate(Self.sharedDelegate, queue: DispatchQueue.main)
                metadataOutput.metadataObjectTypes = metadataObjectTypes
            } else {
                throw MetadataScannerServiceError.cannotAddVideoOutput
            }

            return captureSession
        }
        .subscribe(on: MainScheduler.asyncInstance)
    }

    static func scan(with captureSession: AVCaptureSession) -> Observable<[AVMetadataObject]> {
        Completable.from {
            guard AVCaptureDevice.authorizationStatus(for: .video) == .authorized else {
                throw MetadataScannerServiceLocalizedError.captureDeviceNotAuthorized
            }
        }
        .andThen(delegateOutputs)
        .asObservable()
        .filter { call in captureSession.outputs.contains { call.output == $0 } }
        .map { $0.didOutput }
        .do(onSubscribe: {
            captureSession.startRunning()
        })
        .do(onDispose: {
            captureSession.stopRunning()
        })
    }

}

extension MetadataScannerService: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        Self.delegateOutputs.onNext((output: output, didOutput: metadataObjects, from: connection))
    }
}
