import Foundation
import RxSwift
import DependencyInjection

class UpdateService {

    @InjectStatic(\.keyValueRepo) private var keyValueRepo

    private let screensCompletedKey = "screensCompletedKey"
    private weak var parentViewController: UIViewController?

    init(parent: UIViewController) {
        self.parentViewController = parent
    }

    func handleUpdateInfoScreens() -> Completable {
        return keyValueRepo.load(screensCompletedKey)
            .catch({ _ in Single.just(false) })
            .flatMapCompletable { completed in
                !completed ? self.launchUpdatePageController() : Completable.empty()
            }
    }

    private func launchUpdatePageController() -> Completable {
        Completable.create { observer -> Disposable in
            let updatePageViewController = ViewControllerFactory.Main.createUpdateContainerViewController()
            updatePageViewController.modalTransitionStyle = .crossDissolve
            updatePageViewController.modalPresentationStyle = .overCurrentContext

            updatePageViewController.onCompleted = {
                self.keyValueRepo.store(self.screensCompletedKey,
                                        value: true,
                                        completion: { observer(.completed) },
                                        failure: { _ in observer(.completed) })
            }

            updatePageViewController.onCancelled = {
                observer(.completed)
            }

            self.parentViewController?.present(updatePageViewController, animated: true)

            return Disposables.create {
                DispatchQueue.main.async { updatePageViewController.dismiss(animated: true, completion: nil) }
            }
        }.subscribe(on: MainScheduler.instance)
    }

}
