import Foundation
import Security
import CryptoSwift
import SwiftDGC

public enum KeyClass {
    case `public`
    case `private`
    case symmetric
}

extension KeyClass {
    var typeRawValue: CFString {
        switch self {
        case .private:
            return kSecAttrKeyClassPrivate
        case .public:
            return kSecAttrKeyClassPublic
        case .symmetric:
            return kSecAttrKeyClassSymmetric
        }
    }
}

public enum KeyType {
    case rsa
    case ec
    case ecsecPrimeRandom
}

extension KeyType {
    var typeRawValue: CFString {
        switch self {
        case .ec:
            return kSecAttrKeyTypeEC
        case .ecsecPrimeRandom:
            return kSecAttrKeyTypeECSECPrimeRandom
        case .rsa:
            return kSecAttrKeyTypeRSA
        }
    }
}

public class KeyFactory {

    public static func create(from data: Data, type: KeyType, keyClass: KeyClass, sizeInBits: UInt) throws -> SecKey {
        let attributes: [String: Any] = [kSecAttrKeyType as String: type.typeRawValue,
                                         kSecAttrKeyClass as String: keyClass.typeRawValue,
                                         kSecAttrKeySizeInBits as String: sizeInBits]
        var error: Unmanaged<CFError>?
        guard let key = SecKeyCreateWithData(data as CFData, attributes as CFDictionary, &error) as SecKey? else {
            if let error = error?.takeRetainedValue() {
                throw error
            }
            throw NSError(domain: "Couldn't create key from data!", code: 0, userInfo: nil)
        }
        return key
    }

    public static func create(from data: Data, type: KeyType, keyClass: KeyClass) throws -> SecKey {
        let attributes: [String: Any] = [kSecAttrKeyType as String: type.typeRawValue,
                                         kSecAttrKeyClass as String: keyClass.typeRawValue]
        var error: Unmanaged<CFError>?
        guard let key = SecKeyCreateWithData(data as CFData, attributes as CFDictionary, &error) as SecKey? else {
            if let error = error?.takeRetainedValue() {
                throw error
            }
            throw NSError(domain: "Couldn't create key from data!", code: 0, userInfo: nil)
        }
        return key
    }

    public static func createPrivate(tag: String, type: KeyType, sizeInBits: UInt, persistedInKeychain: Bool = true) throws -> SecKey {
        let attributes: [String: Any] = [kSecAttrKeyType as String: type.typeRawValue,
                                         kSecAttrKeySizeInBits as String: sizeInBits,
                                         kSecPrivateKeyAttrs as String: [
                                            kSecAttrIsPermanent as String: persistedInKeychain,
                                            kSecAttrApplicationTag as String: tag]]
        var error: Unmanaged<CFError>?
        guard let key = SecKeyCreateRandomKey(attributes as CFDictionary, &error) else {
            if let error = error?.takeRetainedValue() {
                throw error
            }
            throw NSError(domain: "There was an error when creating a key", code: 0, userInfo: nil)
        }
        return key
    }

    public static func derivePublic(from key: SecKey) -> SecKey? {
        return SecKeyCopyPublicKey(key)
    }

    public static func createPrivateEC(x: Data, y: Data, d: Data, sizeInBits: UInt = 256) throws -> SecKey {
        let data = NSMutableData(bytes: [0x04], length: 1)
        data.append(x)
        data.append(y)
        data.append(d)

        let attributes: [String: Any] = [kSecAttrKeyType as String: kSecAttrKeyTypeEC,
                                         kSecAttrKeyClass as String: kSecAttrKeyClassPrivate,
                                         kSecAttrKeySizeInBits as String: sizeInBits,
                                         kSecAttrIsPermanent as String: false]

        var error: Unmanaged<CFError>?
        guard let key = SecKeyCreateWithData(data as CFData, attributes as CFDictionary, &error) else {
            if let error = error?.takeRetainedValue() {
                throw error
            }
            throw NSError(domain: "There was an error when creating private EC Key", code: 0, userInfo: nil)
        }
        return key
    }

    public static func createPublicEC(x: Data, y: Data, sizeInBits: UInt = 256) throws -> SecKey {
        let data = NSMutableData(bytes: [0x04], length: 1)
        data.append(x)
        data.append(y)

        let attributes: [String: Any] = [kSecAttrKeyType as String: kSecAttrKeyTypeEC,
                                         kSecAttrKeyClass as String: kSecAttrKeyClassPublic,
                                         kSecAttrKeySizeInBits as String: sizeInBits,
                                         kSecAttrIsPermanent as String: false]

        var error: Unmanaged<CFError>?
        guard let key = SecKeyCreateWithData(data as CFData, attributes as CFDictionary, &error) else {
            if let error = error?.takeRetainedValue() {
                throw error
            }
            throw NSError(domain: "There was an error when creating a public EC Key", code: 0, userInfo: nil)
        }
        return key
    }

    public static func compressPublicEC(key: SecKey) throws -> Data {
        var keyData = try key.toData()

        if keyData.bytes[0] != 0x04 {
            throw NSError(domain: "Error in compressing public ec key: key in an unknown format", code: 0, userInfo: nil)
        }

        if keyData.count != 65 {
            throw NSError(domain: "Error in compressing public ec key: key length invalid", code: 0, userInfo: nil)
        }

        let y = keyData.suffix(32)
        keyData.removeLast(32)
        keyData.removeFirst(1)

        let firstByte: UInt8 = (y.bytes[31] % 2) == 0 ? 0x02 : 0x03
        var data = Data()
        data.append(firstByte)
        data.append(keyData)

        return data
    }

    public static func exchangeKeys(privateKey: SecKey, publicKey: SecKey, algorithm: SecKeyAlgorithm = .ecdhKeyExchangeStandard, requestedKeySize: UInt = 32) -> Data? {
        var error: Unmanaged<CFError>?
        let dict: [String: Any] = [SecKeyKeyExchangeParameter.requestedSize.rawValue as String: requestedKeySize]

        guard let result = SecKeyCopyKeyExchangeResult(privateKey,
                                                        algorithm,
                                                        publicKey,
                                                        dict as CFDictionary,
                                                        &error) as Data? else {
//            logger.log("exchangeKeys: Failed: \(error!.takeRetainedValue() as Error)", entryType: .error)
            return nil
        }
        return result
    }

    public static func randomBytes(size: Int) -> Data? {
        var bytes = [Int8](repeating: 0, count: size)
        let status = SecRandomCopyBytes(kSecRandomDefault, bytes.count, &bytes)

        if status == errSecSuccess {
            return Data(bytes: &bytes, count: size)
        }
        return nil
    }
}

extension SecKey {

    public func toData() throws -> Data {
        var error: Unmanaged<CFError>?
        guard let data = SecKeyCopyExternalRepresentation(self, &error) as Data? else {
            throw error!.takeRetainedValue() as Error
        }
        return data
    }
}
