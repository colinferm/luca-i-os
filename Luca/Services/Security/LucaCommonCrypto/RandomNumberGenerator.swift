import Foundation

protocol RandomNumberGenerator {
    func generate(bytesCount: UInt) throws -> Data
}

enum StandardRandomNumberGeneratorError: Error {
    case unableToGenerateData
}

class StandardRandomNumberGenerator: RandomNumberGenerator {
    func generate(bytesCount: UInt) throws -> Data {
        if let data = KeyFactory.randomBytes(size: Int(bytesCount)) {
            return data
        }
        throw StandardRandomNumberGeneratorError.unableToGenerateData
    }
}
