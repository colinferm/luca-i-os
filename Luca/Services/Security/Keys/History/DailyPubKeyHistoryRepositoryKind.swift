import Foundation
import Security

struct DailyKeyIndex: Codable, Hashable {
    var keyId: Int
    var createdAt: Date
}

protocol DailyPubKeyHistoryRepositoryKind: SecKeyHistoryRepository<DailyKeyIndex> {
    var newestId: DailyKeyIndex? {get}
    func newest(withId: Int) -> DailyKeyIndex?
}
