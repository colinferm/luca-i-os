import RxSwift
import DependencyInjection

class UserOwnershipValidator: DocumentValidator {
    @InjectStatic(\.lucaPreferences) private var preferences

    func validate(document: Document) -> Completable {
        DocumentOwnershipValidator(
            firstNameSource: preferences.get(\.firstName).map { $0 ?? "" },
            lastNameSource: preferences.get(\.lastName).map { $0 ?? "" }
        )
            .validate(document: document)
    }
}
