import Foundation
import RxSwift

class CoronaTestIsNotInFutureValidator: DocumentValidator {

    func validate(document: Document) -> Completable {
        Maybe<CoronaTest>.from { document as? CoronaTest }
            .map { $0.issuedAt }
            .flatMap { issuedAt -> Maybe<Never> in
                if issuedAt < Date() {
                    return Maybe.empty()
                }
                return Maybe.error(CoronaTestProcessingError.testInFuture)
            }
            .asObservable()
            .ignoreElementsAsCompletable()
    }

}
