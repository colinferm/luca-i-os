import Foundation
import SwiftJWT
import DependencyInjection

class DefaultJWTParser: JWTCoronaTestParser<JWTTestClaims> {

    init() {
        super.init { (jwt, originalCode, provider) -> Document in
            return JWTTestPayload(claims: jwt.claims, originalCode: originalCode, provider: provider)
        }
    }
}
