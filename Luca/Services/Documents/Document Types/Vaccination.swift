import Foundation

protocol Vaccination: Document, AssociableToIdentity, ContainsDateOfBirth {

    /// Encoded QR code
    var originalCode: String { get set }

    /// user date of birth
    var dateOfBirth: Date { get }

    /// test type e.g. PCR
    var vaccineType: String { get }

    var doseNumber: Int { get }

    var dosesTotalNumber: Int { get }

    /// testing laboratory
    var laboratory: String { get }
}

extension Vaccination {
    var identifier: Int {
        guard let payloadData = originalCode.data(using: .utf8) else {
            return -1
        }
        return Int(payloadData.crc32)
    }

    var vaccinatedSinceDays: Int {
        return Calendar.current.dateComponents([.day], from: self.issuedAt, to: Date()).day ?? Int.max
    }

    var validDate: Date? {
        return Calendar.current.date(byAdding: .day, value: 15, to: issuedAt)
    }

    var fullyVaccinatedInDays: Int {
        return 15 - vaccinatedSinceDays
    }

    var hasAllDosesReceived: Bool {
        return doseNumber == dosesTotalNumber
    }

    var isAdditionalDose: Bool {
        return doseNumber > dosesTotalNumber
    }

    func isComplete() -> Bool {
        let dateIsValid = vaccinatedSinceDays > 14
        return isAdditionalDose || (hasAllDosesReceived && dateIsValid)
    }

    var expiresAt: Date {
        Calendar.current.date(byAdding: .year, value: 1, to: issuedAt) ?? issuedAt
    }
}
