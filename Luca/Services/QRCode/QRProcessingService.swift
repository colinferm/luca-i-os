import Foundation
import RxSwift
import JGProgressHUD
import DependencyInjection

enum QRProcessingServiceSilentError: SilentError {
    case userNotAgreedOnDifferentQRType
    case userNotAgreedOnPrivacyConsent
    case userNotAgreedOnCheckIn
}

class QRProcessingService {
    @InjectStatic(\.documentProcessingService) private var documentProcessingService
    @InjectStatic(\.documentRepoService) private var documentRepoService
    @InjectStatic(\.traceIdService) private var traceIdService
    @InjectStatic(\.lucaPreferences) private var preferences
    @InjectStatic(\.personRepo) private var personRepo

    private let qrParser: QRParser

    init() {
        self.qrParser = QRParser()
    }

    public func processQRCode(qr: String, expectedType: QRType, presenter: UIViewController) -> Single<QRType> {
        qrParser.processQRType(qr: qr)
            .flatMap {
                self.showAlertIfWrongType(
                    viewController: presenter,
                    expectedType: expectedType,
                    detectedType: $0
                )
                .andThen(Single.just($0))
            }
            .flatMap { type -> Single<QRType> in
                let selectedCompletable: Completable
                switch type {
                case .checkin: selectedCompletable = self.checkin(qr: qr, presenter: presenter)
                case .document: selectedCompletable = self.processDocument(url: qr, presenter: presenter)
                case .url: selectedCompletable = self.openURL(qr: qr)
                }

                return selectedCompletable.andThen(Single.just(type))
            }
    }

    private func checkin(qr: String, presenter: UIViewController) -> Completable {
        CheckInInteractor(presenter: presenter, qrString: qr).interact()
    }

    private func processDocument(url: String, presenter: UIViewController) -> Completable {
        showTestPrivacyConsent(viewController: presenter)
            .andThen(initAdditionalValidators(presenter: presenter))
            .flatMapCompletable { additionalValidators in
                self.documentProcessingService
                    .parseQRCode(qr: url, additionalValidators: additionalValidators)
            }
    }

    private func initAdditionalValidators(presenter: UIViewController) -> Single<[DocumentValidator]> {
        preferences.get(\.userRegistrationData)
            .map { userRegistrationData in
                [VaccinationBirthDateValidator(
                    presenter: presenter,
                    userFirstName: userRegistrationData?.firstName ??? "",
                    userLastName: userRegistrationData?.lastName ??? "",
                    documentSource: self.documentRepoService.currentAndNewTests,
                    personsSource: self.personRepo.restore())]
            }
    }

    private func openURL(qr: String) -> Completable {
        Completable.from {
            guard let url = URL(string: qr) else {
                throw QRProcessingError.parsingFailed
            }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
        .subscribe(on: MainScheduler.instance)
    }

    // MARK: Alerts
    /// Shows an alert if expected type and detected type mismatch. If User doesn't allow to procees, an Silent Error will be emitted.
    private func showAlertIfWrongType(viewController: UIViewController, expectedType: QRType, detectedType: QRType) -> Completable {

        return Completable.create { observer -> Disposable in

            let showAlert = expectedType != detectedType && detectedType != .url

            let title = expectedType == .checkin ? L10n.Camera.Warning.Checkin.title : L10n.Camera.Warning.Document.title

            let message = expectedType == .checkin ? L10n.Camera.Warning.Checkin.description : L10n.Camera.Warning.Document.description

            let alert = UIAlertController
                .actionAndCancelAlert(title: title, message: message, actionTitle: L10n.Navigation.Basic.continue, action: {
                    observer(.completed)
                }, cancelAction: {
                    observer(.error(QRProcessingServiceSilentError.userNotAgreedOnDifferentQRType))
                })
            showAlert ? viewController.present(alert, animated: true, completion: nil) : observer(.completed)

            return Disposables.create { alert.dismiss(animated: true, completion: nil) }
        }
        .subscribe(on: MainScheduler.instance)
    }

    /// An alert with privacy consent will be shown. If user disagrees, a silent error will be emitted.
    private func showTestPrivacyConsent(viewController: UIViewController) -> Completable {
        return Completable.create { observer -> Disposable in
            let alert = ViewControllerFactory.Alert.createTestPrivacyConsent(confirmAction: {
                observer(.completed)
            }, cancelAction: {
                observer(.error(QRProcessingServiceSilentError.userNotAgreedOnPrivacyConsent))
            })
            alert.modalTransitionStyle = .crossDissolve
            alert.modalPresentationStyle = .overCurrentContext
            viewController.present(alert, animated: true, completion: nil)

            return Disposables.create { alert.dismiss(animated: true, completion: nil) }
        }
        .subscribe(on: MainScheduler.instance)
    }

}
