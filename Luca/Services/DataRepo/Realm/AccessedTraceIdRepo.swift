import Foundation
import RealmSwift

struct AccessedTraceId {
    var healthDepartmentId: UUID
    var traceInfoId: Int
    var warningLevel: UInt8
    var traceId: String

    /// Date when this data set has been created on the backend.
    var createdAt: Date

    /// Date when this data set has been sighted (downloaded).
    var sightDate: Date

    /// Date when this data set has been read by the user
    var readDate: Date?
}

extension AccessedTraceId: DataRepoModel {
    var identifier: Int? {
        get {
            var checksum = Data()
            checksum.append(healthDepartmentId.data)
            checksum.append(traceId.data(using: .utf8) ?? Data())
            checksum.append(warningLevel.data)
            return Int(checksum.crc32)
        }
        set {}
    }
}

class AccessedTraceIdRealmModel: RealmSaveModel<AccessedTraceId> {
    @objc dynamic var healthDepartmentId: String = ""
    @objc dynamic var traceInfoId: Int = 0
    @objc dynamic var warningLevel: Int = 0
    @objc dynamic var traceId: String = ""

    /// Date when this data set has been created on the backend.
    @objc dynamic var createdAt: Date = Date()

    /// Date when this data set has been sighted (downloaded).
    @objc dynamic var sightDate: Date = Date()

    /// Date when this dataset has been notified to user per local notification.
    @objc dynamic var readDate: Date?

    override func create() -> AccessedTraceId {
        return AccessedTraceId(healthDepartmentId: UUID(), traceInfoId: 0, warningLevel: 0, traceId: "", createdAt: Date(), sightDate: Date(), readDate: nil)
    }

    override func populate(from: AccessedTraceId) {
        super.populate(from: from)
        healthDepartmentId = from.healthDepartmentId.uuidString
        traceInfoId = from.traceInfoId
        warningLevel = Int(from.warningLevel)
        createdAt = from.createdAt
        sightDate = from.sightDate
        readDate = from.readDate
        traceId = from.traceId
    }

    override var model: AccessedTraceId {
        var m = super.model
        m.healthDepartmentId = UUID(uuidString: healthDepartmentId) ?? UUID()
        m.traceInfoId = traceInfoId
        m.warningLevel = UInt8(warningLevel)
        m.createdAt = createdAt
        m.sightDate = sightDate
        m.readDate = readDate
        m.traceId = traceId
        return m
    }
}

class AccessedTraceIdRepo: RealmDataRepo<AccessedTraceIdRealmModel, AccessedTraceId> {
    override func createSaveModel() -> AccessedTraceIdRealmModel {
        return AccessedTraceIdRealmModel()
    }

    init(key: Data) {
        super.init(filenameSalt: "YetAnotherAccessedTraceIdRepo", schemaVersion: 0, encryptionKey: key)
    }
}
