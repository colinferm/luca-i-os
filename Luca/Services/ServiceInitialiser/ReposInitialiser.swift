import Foundation
import DependencyInjection
import RxSwift

private var _localDBKeyRepository: BaseInjectionProvider<DataKeyRepository> = EmptyProvider()

private var _traceInfoRepo: BaseInjectionProvider<DataRepo<TraceInfo>> = EmptyProvider()
private var _traceIdCoreRepo: BaseInjectionProvider<DataRepo<TraceIdCore>> = EmptyProvider()
private var _locationRepo: BaseInjectionProvider<DataRepo<Location>> = EmptyProvider()
private var _keyValueRepo: BaseInjectionProvider<KeyValueRepoProtocol> = EmptyProvider()
private var _importantDataKeyValueRepo: BaseInjectionProvider<KeyValueRepoProtocol> = EmptyProvider()
private var _healthDepartmentRepo: BaseInjectionProvider<DataRepo<HealthDepartment>> = EmptyProvider()
private var _historyRepo: BaseInjectionProvider<DataRepo<HistoryEntry>> = EmptyProvider()
private var _documentRepo: BaseInjectionProvider<DataRepo<DocumentPayload>> = EmptyProvider()
private var _testProviderKeyRepo: BaseInjectionProvider<DataRepo<TestProviderKey>> = EmptyProvider()
private var _personRepo: BaseInjectionProvider<DataRepo<Person>> = EmptyProvider()
private var _notificationHealthDepartmentRepo: BaseInjectionProvider<DataRepo<NotificationHealthDepartment>> = EmptyProvider()
private var _accessedTraceIdRepo: BaseInjectionProvider<DataRepo<AccessedTraceId>> = EmptyProvider()

private var _lucaPreferences: BaseInjectionProvider<LucaPreferences> = EmptyProvider()

extension DependencyContext {
    var localDBKeyRepository: BaseInjectionProvider<DataKeyRepository> {
        get { _localDBKeyRepository }
        set { _localDBKeyRepository = newValue }
    }

    var traceInfoRepo: BaseInjectionProvider<DataRepo<TraceInfo>> {
        get { _traceInfoRepo }
        set { _traceInfoRepo = newValue }
    }

    var lucaPreferences: BaseInjectionProvider<LucaPreferences> {
        get { _lucaPreferences }
        set { _lucaPreferences = newValue }
    }

    // Those will be prepared step by step in following PRs.
    var traceIdCoreRepo: BaseInjectionProvider<DataRepo<TraceIdCore>> {
        get { _traceIdCoreRepo }
        set { _traceIdCoreRepo = newValue }
    }
    var locationRepo: BaseInjectionProvider<DataRepo<Location>> {
        get { _locationRepo }
        set { _locationRepo = newValue }
    }

    var keyValueRepo: BaseInjectionProvider<KeyValueRepoProtocol> {
        get { _keyValueRepo }
        set { _keyValueRepo = newValue }
    }

    /// Reserved for data, that cannot be removed
    var importantDataKeyValueRepo: BaseInjectionProvider<KeyValueRepoProtocol> {
        get { _importantDataKeyValueRepo }
        set { _importantDataKeyValueRepo = newValue }
    }

    var healthDepartmentRepo: BaseInjectionProvider<DataRepo<HealthDepartment>> {
        get { _healthDepartmentRepo }
        set { _healthDepartmentRepo = newValue }
    }
    var historyRepo: BaseInjectionProvider<DataRepo<HistoryEntry>> {
        get { _historyRepo }
        set { _historyRepo = newValue }
    }
    var documentRepo: BaseInjectionProvider<DataRepo<DocumentPayload>> {
        get { _documentRepo }
        set { _documentRepo = newValue }
    }
    var testProviderKeyRepo: BaseInjectionProvider<DataRepo<TestProviderKey>> {
        get { _testProviderKeyRepo }
        set { _testProviderKeyRepo = newValue }
    }
    var personRepo: BaseInjectionProvider<DataRepo<Person>> {
        get { _personRepo }
        set { _personRepo = newValue }
    }
    var notificationHealthDepartmentRepo: BaseInjectionProvider<DataRepo<NotificationHealthDepartment>> {
        get { _notificationHealthDepartmentRepo }
        set { _notificationHealthDepartmentRepo = newValue }
    }
    var accessedTraceIdRepo: BaseInjectionProvider<DataRepo<AccessedTraceId>> {
        get { _accessedTraceIdRepo }
        set { _accessedTraceIdRepo = newValue }
    }
}

class ReposInitialiser: ServiceInitialiser {

    var realmDatabaseUtils: [RealmDatabaseUtils] = []

    func initialise() throws {

        let localDBKeyRepository = DataKeyRepository(tag: "LocalDBKey")

        DependencyContext[\.localDBKeyRepository] = SharedProvider(value: localDBKeyRepository)

        var currentKey: Data! = localDBKeyRepository.retrieveKey()
        let keyWasAvailable = currentKey != nil
        if !keyWasAvailable {
            self.log("No DB Key found, generating one...")
            guard let bytes = KeyFactory.randomBytes(size: 64) else {
                throw NSError(domain: "Couldn't generate random bytes for local DB key", code: 0, userInfo: nil)
            }
            if !localDBKeyRepository.store(key: bytes, removeIfExists: true) {
                throw NSError(domain: "Couldn't store local DB key", code: 0, userInfo: nil)
            }
            currentKey = bytes
            self.log("DB Key generated and stored succesfully.")
        }

        DependencyContext[\.traceInfoRepo] = SharedProvider(value: TraceInfoRepo(key: currentKey))
        DependencyContext[\.traceIdCoreRepo] = SharedProvider(value: TraceIdCoreRepo(key: currentKey))
        DependencyContext[\.locationRepo] = SharedProvider(value: LocationRepo(key: currentKey))
        DependencyContext[\.keyValueRepo] = SharedProvider(value: RealmKeyValueRepo(key: currentKey, filenameSalt: "RealmKeyValueUnderlyingRepo"))
        DependencyContext[\.importantDataKeyValueRepo] = SharedProvider(value: RealmKeyValueRepo(key: currentKey, filenameSalt: "RealmKeyValueUnderlyingRepoForImportantData"))
        DependencyContext[\.healthDepartmentRepo] = SharedProvider(value: HealthDepartmentRepo(key: currentKey))
        DependencyContext[\.historyRepo] = SharedProvider(value: HistoryRepo(key: currentKey))
        DependencyContext[\.documentRepo] = SharedProvider(value: DocumentRepo(key: currentKey))
        DependencyContext[\.testProviderKeyRepo] = SharedProvider(value: TestProviderKeyRepo(key: currentKey))
        DependencyContext[\.personRepo] = SharedProvider(value: PersonRepo(key: currentKey))
        DependencyContext[\.notificationHealthDepartmentRepo] = SharedProvider(value: NotificationHealthDepartmentRepo(key: currentKey))
        DependencyContext[\.accessedTraceIdRepo] = SharedProvider(value: AccessedTraceIdRepo(key: currentKey))

        // Force cast is OK here as it is known that all of those Repos conform to RealmDatabaseUtils. Besides, crash is required in the initialisation if something goes wrong.
        // swiftlint:disable force_cast
        try realmDatabaseUtils = [
            DependencyContext[\.traceInfoRepo].provide() as! RealmDatabaseUtils,
            DependencyContext[\.traceIdCoreRepo].provide() as! RealmDatabaseUtils,
            DependencyContext[\.locationRepo].provide() as! RealmDatabaseUtils,
            DependencyContext[\.keyValueRepo].provide() as! RealmDatabaseUtils,
            DependencyContext[\.importantDataKeyValueRepo].provide() as! RealmDatabaseUtils,
            DependencyContext[\.healthDepartmentRepo].provide() as! RealmDatabaseUtils,
            DependencyContext[\.historyRepo].provide() as! RealmDatabaseUtils,
            DependencyContext[\.documentRepo].provide() as! RealmDatabaseUtils,
            DependencyContext[\.testProviderKeyRepo].provide() as! RealmDatabaseUtils,
            DependencyContext[\.personRepo].provide() as! RealmDatabaseUtils,
            DependencyContext[\.notificationHealthDepartmentRepo].provide() as! RealmDatabaseUtils,
            DependencyContext[\.accessedTraceIdRepo].provide() as! RealmDatabaseUtils
        ]
        // swiftlint:enable force_cast

        if !keyWasAvailable {
            self.log("Applying new key to the repos")

            let changeEncryptionCompletables = self.realmDatabaseUtils
                .map { $0.changeEncryptionSettings(oldKey: nil, newKey: currentKey)
                    .logError(self, "\(String(describing: $0.self)): Changing encryption")
                }

            try Completable.concat(changeEncryptionCompletables)
            .debug("KT TOTAL")
            .do(onError: { error in
                fatalError("failed to change encryption settings. Error: \(error)")
            })
            .blockingWait(nil) // This blocking is crucial here. I want to block the app until the settings are done.

            self.log("New keys applied successfully")
        }

        for util in realmDatabaseUtils.enumerated() {
            try DependencyContext.register(SharedProvider(value: util.element), keyIdentifier: "RealmUtils\(util.offset)")
        }

        let lucaPreferences = LucaPreferences()
        DependencyContext[\.lucaPreferences] = SharedProvider(value: lucaPreferences)
        try lucaPreferences
            .migrate(from: OldLucaPreferences.shared)
            .debug("Preferenes migration")
            .blockingWait(nil)
    }

    func invalidate() {
        DependencyContext[\.lucaPreferences] = EmptyProvider()
        DependencyContext[\.traceInfoRepo] = EmptyProvider()
        DependencyContext[\.traceIdCoreRepo] = EmptyProvider()
        DependencyContext[\.locationRepo] = EmptyProvider()
        DependencyContext[\.keyValueRepo] = EmptyProvider()
        DependencyContext[\.importantDataKeyValueRepo] = EmptyProvider()
        DependencyContext[\.healthDepartmentRepo] = EmptyProvider()
        DependencyContext[\.historyRepo] = EmptyProvider()
        DependencyContext[\.documentRepo] = EmptyProvider()
        DependencyContext[\.testProviderKeyRepo] = EmptyProvider()
        DependencyContext[\.personRepo] = EmptyProvider()
        DependencyContext[\.notificationHealthDepartmentRepo] = EmptyProvider()
        DependencyContext[\.accessedTraceIdRepo] = EmptyProvider()

        for util in realmDatabaseUtils.enumerated() {
            DependencyContext.remove(for: "RealmUtils\(util.offset)")
        }

        realmDatabaseUtils = []
    }
}

extension ReposInitialiser: UnsafeAddress, LogUtil {}
