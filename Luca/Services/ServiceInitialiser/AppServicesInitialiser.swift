import Foundation
import DependencyInjection

private var dataResetServiceProvider: BaseInjectionProvider<DataResetService> = EmptyProvider<DataResetService>()

private var appServiceInitialiserProvider = SharedProvider<AppServicesInitialiser>(value: AppServicesInitialiser())

extension DependencyContext {
    var appServicesInitialiser: SharedProvider<AppServicesInitialiser> {
        get { appServiceInitialiserProvider }
        set { appServiceInitialiserProvider = newValue }
    }
    var dataResetService: BaseInjectionProvider<DataResetService> {
        get { dataResetServiceProvider }
        set { dataResetServiceProvider = newValue }
    }
}

class AppServicesInitialiser: ServiceInitialiser, Toggleable {

    private var alreadyInitialised = false
    private(set) var isEnabled: Bool = false

    @InjectAllDynamic private var toggleables: [Toggleable]

    private let initialisers: [ServiceInitialiser] = [
        CryptoInitialiser(),
        BackendInitialiser(),
        DataBuildersInitialiser(),
        ReposInitialiser(),
        MiscServicesInitialiser(),
        DocumentsServicesInitialiser()
    ]

    func initialise() throws {
        if alreadyInitialised {
            return
        }

        for initialiser in initialisers {
            try initialiser.initialise()
        }
        DependencyContext[\.dataResetService] = SharedProvider(value: DataResetService())

        // Enable all toggleable services
        enable()

        alreadyInitialised = true
    }

    func invalidate() {

        // Disable all toggleable services
        disable()

        DependencyContext[\.dataResetService] = EmptyProvider()

        for initialiser in initialisers.reversed() {
            initialiser.invalidate()
        }

        alreadyInitialised = false
    }

    func enable() {
        if isEnabled {
            return
        }
        for t in toggleables {

            // Skip LocationUpdater for the start
            if t is LocationUpdater {
                continue
            }
            t.enable()
        }
        isEnabled = true
    }

    func disable() {
        for t in toggleables {
            t.disable()
        }
        isEnabled = false
    }
}
