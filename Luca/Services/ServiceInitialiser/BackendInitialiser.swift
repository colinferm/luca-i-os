import DependencyInjection

private var backendAddressV3Provider: BaseInjectionProvider<BackendAddressV3> = EmptyProvider()
private var backendAddressV4Provider: BaseInjectionProvider<BackendAddressV4> = EmptyProvider()
private var backendUserV3Provider: BaseInjectionProvider<BackendUserV3> = EmptyProvider()
private var backendTraceIdV3Provider: BaseInjectionProvider<BackendTraceIdV3> = EmptyProvider()
private var backendMiscV3Provider: BaseInjectionProvider<CommonBackendMisc> = EmptyProvider()
private var backendSMSV3Provider: BaseInjectionProvider<BaseBackendSMSVerification> = EmptyProvider()
private var backendLocationV3Provider: BaseInjectionProvider<BackendLocationV3> = EmptyProvider()
private var backendDailyKeyV3Provider: BaseInjectionProvider<DefaultBackendDailyKeyV3> = EmptyProvider()
private var backendAccessDataV4Provider: BaseInjectionProvider<BackendAccessDataV4> = EmptyProvider()
private var notificationConfigCachedDataSourceProvider: BaseInjectionProvider<CachedDataSource<NotificationConfig>> = EmptyProvider()
private var accessedTracesDataChunkCachedDataSourceProvider: BaseInjectionProvider<CachedDataSource<AccessedTracesDataChunk>> = EmptyProvider()

extension DependencyContext {

    var backendAddressV3: BaseInjectionProvider<BackendAddressV3> {
        get { backendAddressV3Provider }
        set { backendAddressV3Provider = newValue }
    }
    var backendAddressV4: BaseInjectionProvider<BackendAddressV4> {
        get { backendAddressV4Provider }
        set { backendAddressV4Provider = newValue }
    }
    var backendUserV3: BaseInjectionProvider<BackendUserV3> {
        get { backendUserV3Provider }
        set { backendUserV3Provider = newValue }
    }
    var backendTraceIdV3: BaseInjectionProvider<BackendTraceIdV3> {
        get { backendTraceIdV3Provider }
        set { backendTraceIdV3Provider = newValue }
    }
    var backendMiscV3: BaseInjectionProvider<CommonBackendMisc> {
        get { backendMiscV3Provider }
        set { backendMiscV3Provider = newValue }
    }
    var backendSMSV3: BaseInjectionProvider<BaseBackendSMSVerification> {
        get { backendSMSV3Provider }
        set { backendSMSV3Provider = newValue }
    }
    var backendLocationV3: BaseInjectionProvider<BackendLocationV3> {
        get { backendLocationV3Provider }
        set { backendLocationV3Provider = newValue }
    }
    var backendDailyKeyV3: BaseInjectionProvider<DefaultBackendDailyKeyV3> {
        get { backendDailyKeyV3Provider }
        set { backendDailyKeyV3Provider = newValue }
    }
    var backendAccessDataV4: BaseInjectionProvider<BackendAccessDataV4> {
        get { backendAccessDataV4Provider }
        set { backendAccessDataV4Provider = newValue }
    }
    var notificationConfigCachedDataSource: BaseInjectionProvider<CachedDataSource<NotificationConfig>> {
        get { notificationConfigCachedDataSourceProvider }
        set { notificationConfigCachedDataSourceProvider = newValue }
    }
    var accessedTracesDataChunkCachedDataSource: BaseInjectionProvider<CachedDataSource<AccessedTracesDataChunk>> {
        get { accessedTracesDataChunkCachedDataSourceProvider }
        set { accessedTracesDataChunkCachedDataSourceProvider = newValue }
    }
}

class BackendInitialiser: ServiceInitialiser {

    func initialise() throws {

        DependencyContext[\.backendAddressV3] = SharedProvider(value: BackendAddressV3())
        DependencyContext[\.backendAddressV4] = SharedProvider(value: BackendAddressV4())

        let backendAccessDataV4 = BackendAccessDataV4()
        #if PRODUCTION
        let notificationCacheValidity = CacheValidity.until(unit: .hour, count: 6)
        #else
        let notificationCacheValidity = CacheValidity.onceInRuntime
        #endif
        let notificationConfigCachedDataSource = BaseCachedDataSource(
            dataSource: backendAccessDataV4.fetchNotificationConfig(),
            cacheDataRepo: KeyValueRepoCacheWrapper(uniqueCacheKey: "NotificationCache"),
            cacheValidity: notificationCacheValidity,
            uniqueCacheIdentifier: "NotificationConfigCache"
        )

        #if PRODUCTION
        let cacheUnit = Calendar.Component.minute
        #else
        let cacheUnit = Calendar.Component.second
        #endif

        let accessedTracesDataChunkCachedDataSource = BaseCachedDataSource(
            dataSource: backendAccessDataV4.activeChunk(),
            cacheDataRepo: KeyValueRepoCacheWrapper(uniqueCacheKey: "ActiveChunk"),
            cacheValidity: .until(unit: cacheUnit, count: 30),
            uniqueCacheIdentifier: "ActiveChunkCache"
        )

        DependencyContext[\.backendSMSV3] = SharedProvider(value: BaseBackendSMSVerification())
        DependencyContext[\.backendLocationV3] = SharedProvider(value: BackendLocationV3())
        DependencyContext[\.backendUserV3] = SharedProvider(value: BackendUserV3())
        DependencyContext[\.backendTraceIdV3] = SharedProvider(value: BackendTraceIdV3())
        DependencyContext[\.backendMiscV3] = SharedProvider(value: CommonBackendMisc())
        DependencyContext[\.backendAccessDataV4] = SharedProvider(value: backendAccessDataV4)
        DependencyContext[\.backendDailyKeyV3] = SharedProvider(value: DefaultBackendDailyKeyV3())
        DependencyContext[\.notificationConfigCachedDataSource] = SharedProvider(value: notificationConfigCachedDataSource)
        DependencyContext[\.accessedTracesDataChunkCachedDataSource] = SharedProvider(value: accessedTracesDataChunkCachedDataSource)
    }

    func invalidate() {
        DependencyContext[\.backendAddressV3] = EmptyProvider()
        DependencyContext[\.backendAddressV4] = EmptyProvider()
        DependencyContext[\.backendSMSV3] = EmptyProvider()
        DependencyContext[\.backendLocationV3] = EmptyProvider()
        DependencyContext[\.backendUserV3] = EmptyProvider()
        DependencyContext[\.backendTraceIdV3] = EmptyProvider()
        DependencyContext[\.backendMiscV3] = EmptyProvider()
        DependencyContext[\.backendAccessDataV4] = EmptyProvider()
        DependencyContext[\.backendDailyKeyV3] = EmptyProvider()
        DependencyContext[\.notificationConfigCachedDataSource] = EmptyProvider()
        DependencyContext[\.accessedTracesDataChunkCachedDataSource] = EmptyProvider()
    }

}
