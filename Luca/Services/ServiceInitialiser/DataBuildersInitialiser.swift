import DependencyInjection

private var dailyKeyRepositoryProvider: BaseInjectionProvider<DailyPubKeyHistoryRepository> = EmptyProvider()
private var ePubKeyHistoryRepositoryProvider: BaseInjectionProvider<EphemeralPublicKeyHistoryRepository> = EmptyProvider()
private var ePrivKeyHistoryRepositoryProvider: BaseInjectionProvider<EphemeralPrivateKeyHistoryRepository> = EmptyProvider()
private var locationPrivateKeyHistoryRepositoryProvider: BaseInjectionProvider<LocationPrivateKeyHistoryRepository> = EmptyProvider()
private var userKeysBundleProvider: BaseInjectionProvider<UserKeysBundle> = EmptyProvider()
private var userDataPackageBuilderV3Provider: BaseInjectionProvider<UserDataPackageBuilderV3> = EmptyProvider()
private var qrCodePayloadBuilderV3Provider: BaseInjectionProvider<QRCodePayloadBuilderV3> = EmptyProvider()
private var checkOutPayloadBuilderV3Provider: BaseInjectionProvider<CheckOutPayloadBuilderV3> = EmptyProvider()
private var checkInPayloadBuilderV3Provider: BaseInjectionProvider<CheckInPayloadBuilderV3> = EmptyProvider()
private var traceIdAdditionalBuilderV3Provider: BaseInjectionProvider<TraceIdAdditionalDataBuilderV3> = EmptyProvider()
private var privateMeetingQRCodeBuilderV3Provider: BaseInjectionProvider<PrivateMeetingQRCodeBuilderV3> = EmptyProvider()
private var userTransferBuilderV3Provider: BaseInjectionProvider<UserTransferBuilderV3> = EmptyProvider()

extension DependencyContext {
    var dailyKeyRepository: BaseInjectionProvider<DailyPubKeyHistoryRepository> {
        get { dailyKeyRepositoryProvider }
        set { dailyKeyRepositoryProvider = newValue }
    }
    var ePubKeyHistoryRepository: BaseInjectionProvider<EphemeralPublicKeyHistoryRepository> {
        get { ePubKeyHistoryRepositoryProvider }
        set { ePubKeyHistoryRepositoryProvider = newValue }
    }
    var ePrivKeyHistoryRepository: BaseInjectionProvider<EphemeralPrivateKeyHistoryRepository> {
        get { ePrivKeyHistoryRepositoryProvider }
        set { ePrivKeyHistoryRepositoryProvider = newValue }
    }
    var locationPrivateKeyHistoryRepository: BaseInjectionProvider<LocationPrivateKeyHistoryRepository> {
        get { locationPrivateKeyHistoryRepositoryProvider }
        set { locationPrivateKeyHistoryRepositoryProvider = newValue }
    }
    var userKeysBundle: BaseInjectionProvider<UserKeysBundle> {
        get { userKeysBundleProvider }
        set { userKeysBundleProvider = newValue }
    }
    var userDataPackageBuilderV3: BaseInjectionProvider<UserDataPackageBuilderV3> {
        get { userDataPackageBuilderV3Provider }
        set { userDataPackageBuilderV3Provider = newValue }
    }
    var qrCodePayloadBuilderV3: BaseInjectionProvider<QRCodePayloadBuilderV3> {
        get { qrCodePayloadBuilderV3Provider }
        set { qrCodePayloadBuilderV3Provider = newValue }
    }
    var checkOutPayloadBuilderV3: BaseInjectionProvider<CheckOutPayloadBuilderV3> {
        get { checkOutPayloadBuilderV3Provider }
        set { checkOutPayloadBuilderV3Provider = newValue }
    }
    var checkInPayloadBuilderV3: BaseInjectionProvider<CheckInPayloadBuilderV3> {
        get { checkInPayloadBuilderV3Provider }
        set { checkInPayloadBuilderV3Provider = newValue }
    }
    var traceIdAdditionalBuilderV3: BaseInjectionProvider<TraceIdAdditionalDataBuilderV3> {
        get { traceIdAdditionalBuilderV3Provider }
        set { traceIdAdditionalBuilderV3Provider = newValue }
    }
    var privateMeetingQRCodeBuilderV3: BaseInjectionProvider<PrivateMeetingQRCodeBuilderV3> {
        get { privateMeetingQRCodeBuilderV3Provider }
        set { privateMeetingQRCodeBuilderV3Provider = newValue }
    }
    var userTransferBuilderV3: BaseInjectionProvider<UserTransferBuilderV3> {
        get { userTransferBuilderV3Provider }
        set { userTransferBuilderV3Provider = newValue }
    }
}

class DataBuildersInitialiser: ServiceInitialiser {

    func initialise() throws {
        DependencyContext[\.dailyKeyRepository] = SharedProvider(value: DailyPubKeyHistoryRepository())
        DependencyContext[\.ePubKeyHistoryRepository] = SharedProvider(value: EphemeralPublicKeyHistoryRepository())
        DependencyContext[\.ePrivKeyHistoryRepository] = SharedProvider(value: EphemeralPrivateKeyHistoryRepository())
        DependencyContext[\.locationPrivateKeyHistoryRepository] = SharedProvider(value: LocationPrivateKeyHistoryRepository())
        let userKeysBundle = UserKeysBundle()
        DependencyContext[\.userKeysBundle] = SharedProvider(value: userKeysBundle)
        DependencyContext[\.userDataPackageBuilderV3] = SharedProvider(value: UserDataPackageBuilderV3())
        DependencyContext[\.qrCodePayloadBuilderV3] = SharedProvider(value: QRCodePayloadBuilderV3())
        DependencyContext[\.checkOutPayloadBuilderV3] = SharedProvider(value: CheckOutPayloadBuilderV3())
        DependencyContext[\.checkInPayloadBuilderV3] = SharedProvider(value: CheckInPayloadBuilderV3())
        DependencyContext[\.traceIdAdditionalBuilderV3] = SharedProvider(value: TraceIdAdditionalDataBuilderV3())
        DependencyContext[\.privateMeetingQRCodeBuilderV3] = SharedProvider(value: PrivateMeetingQRCodeBuilderV3())
        DependencyContext[\.userTransferBuilderV3] = SharedProvider(value: UserTransferBuilderV3())

        try userKeysBundle.generateKeys()
        userKeysBundle.removeUnusedKeys()
    }

    func invalidate() {
        DependencyContext[\.dailyKeyRepository] = EmptyProvider()
        DependencyContext[\.ePubKeyHistoryRepository] = EmptyProvider()
        DependencyContext[\.ePrivKeyHistoryRepository] = EmptyProvider()
        DependencyContext[\.locationPrivateKeyHistoryRepository] = EmptyProvider()
        DependencyContext[\.userKeysBundle] = EmptyProvider()
        DependencyContext[\.userDataPackageBuilderV3] = EmptyProvider()
        DependencyContext[\.qrCodePayloadBuilderV3] = EmptyProvider()
        DependencyContext[\.checkOutPayloadBuilderV3] = EmptyProvider()
        DependencyContext[\.checkInPayloadBuilderV3] = EmptyProvider()
        DependencyContext[\.traceIdAdditionalBuilderV3] = EmptyProvider()
        DependencyContext[\.privateMeetingQRCodeBuilderV3] = EmptyProvider()
        DependencyContext[\.userTransferBuilderV3] = EmptyProvider()
    }
}
