import Foundation
import DependencyInjection

private var dailyKeyRepoHandlerProvider: BaseInjectionProvider<DailyKeyRepoHandler> = EmptyProvider()
private var userServiceProvider: BaseInjectionProvider<UserService> = EmptyProvider()
private var locationUpdaterProvider: BaseInjectionProvider<LocationUpdater> = EmptyProvider()
private var autoCheckoutServiceProvider: BaseInjectionProvider<AutoCheckoutService> = EmptyProvider()
private var traceIdServiceProvider: BaseInjectionProvider<TraceIdService> = EmptyProvider()
private var historyProvider: BaseInjectionProvider<HistoryService> = EmptyProvider()
private var historyListenerProvider: BaseInjectionProvider<HistoryEventListener> = EmptyProvider()
private var selfCheckinProvider: BaseInjectionProvider<SelfCheckinService> = EmptyProvider()
private var privateMeetingServiceProvider: BaseInjectionProvider<PrivateMeetingService> = EmptyProvider()
private var userSecrectsConsistencyCheckerProvider: BaseInjectionProvider<UserSecretsConsistencyChecker> = EmptyProvider()
private var accessDataChunkHandlerProvider: BaseInjectionProvider<AccessDataChunkHandler> = EmptyProvider()
private var accessTraceIdCheckerProvider: BaseInjectionProvider<AccessedTraceIdChecker> = EmptyProvider()
private var accessTraceIdNotifierProvider: BaseInjectionProvider<AccessedTraceIdNotifier> = EmptyProvider()
private var personServiceProvider: BaseInjectionProvider<PersonService> = EmptyProvider()
private var baerCodeKeyServiceProvider: BaseInjectionProvider<BaerCodeKeyService> = EmptyProvider()
private var notificationServiceProvider: BaseInjectionProvider<NotificationService> = EmptyProvider()
private var contactDataReporterProvider: BaseInjectionProvider<ContentReporterService> = EmptyProvider()
private var documentsReporterProvider: BaseInjectionProvider<ContentReporterService> = EmptyProvider()
private var checkinPollingProvider: BaseInjectionProvider<CheckinStatusPollingService> = EmptyProvider()
private var reachabilityServiceProvider: BaseInjectionProvider<ReachabilityService> = EmptyProvider()
private var lucaLocationPermissionWorkflowProvider: BaseInjectionProvider<LucaLocationPermissionWorkflow> = EmptyProvider()

extension DependencyContext {
    var dailyKeyRepoHandler: BaseInjectionProvider<DailyKeyRepoHandler> {
        get { dailyKeyRepoHandlerProvider }
        set { dailyKeyRepoHandlerProvider = newValue }
    }
    var userService: BaseInjectionProvider<UserService> {
        get { userServiceProvider }
        set { userServiceProvider = newValue }
    }
    var locationUpdater: BaseInjectionProvider<LocationUpdater> {
        get { locationUpdaterProvider }
        set { locationUpdaterProvider = newValue }
    }
    var autoCheckoutService: BaseInjectionProvider<AutoCheckoutService> {
        get { autoCheckoutServiceProvider }
        set { autoCheckoutServiceProvider = newValue }
    }
    var traceIdService: BaseInjectionProvider<TraceIdService> {
        get { traceIdServiceProvider }
        set { traceIdServiceProvider = newValue }
    }
    var history: BaseInjectionProvider<HistoryService> {
        get { historyProvider }
        set { historyProvider = newValue }
    }
    var historyListener: BaseInjectionProvider<HistoryEventListener> {
        get { historyListenerProvider }
        set { historyListenerProvider = newValue }
    }
    var selfCheckin: BaseInjectionProvider<SelfCheckinService> {
        get { selfCheckinProvider }
        set { selfCheckinProvider = newValue }
    }
    var privateMeetingService: BaseInjectionProvider<PrivateMeetingService> {
        get { privateMeetingServiceProvider }
        set { privateMeetingServiceProvider = newValue }
    }
    var userSecrectsConsistencyChecker: BaseInjectionProvider<UserSecretsConsistencyChecker> {
        get { userSecrectsConsistencyCheckerProvider }
        set { userSecrectsConsistencyCheckerProvider = newValue }
    }
    var accessDataChunkHandler: BaseInjectionProvider<AccessDataChunkHandler> {
        get { accessDataChunkHandlerProvider }
        set { accessDataChunkHandlerProvider = newValue }
    }
    var accessTraceIdChecker: BaseInjectionProvider<AccessedTraceIdChecker> {
        get { accessTraceIdCheckerProvider }
        set { accessTraceIdCheckerProvider = newValue }
    }
    var accessTraceIdNotifier: BaseInjectionProvider<AccessedTraceIdNotifier> {
        get { accessTraceIdNotifierProvider }
        set { accessTraceIdNotifierProvider = newValue }
    }
    var personService: BaseInjectionProvider<PersonService> {
        get { personServiceProvider }
        set { personServiceProvider = newValue }
    }
    var baerCodeKeyService: BaseInjectionProvider<BaerCodeKeyService> {
        get { baerCodeKeyServiceProvider }
        set { baerCodeKeyServiceProvider = newValue }
    }
    var notificationService: BaseInjectionProvider<NotificationService> {
        get { notificationServiceProvider }
        set { notificationServiceProvider = newValue }
    }
    var contactDataReporter: BaseInjectionProvider<ContentReporterService> {
        get { contactDataReporterProvider }
        set { contactDataReporterProvider = newValue }
    }
    var documentsReporter: BaseInjectionProvider<ContentReporterService> {
        get { documentsReporterProvider }
        set { documentsReporterProvider = newValue }
    }
    var checkinPolling: BaseInjectionProvider<CheckinStatusPollingService> {
        get { checkinPollingProvider }
        set { checkinPollingProvider = newValue }
    }
    var reachabilityService: BaseInjectionProvider<ReachabilityService> {
        get { reachabilityServiceProvider }
        set { reachabilityServiceProvider = newValue }
    }
    var lucaLocationPermissionWorkflow: BaseInjectionProvider<LucaLocationPermissionWorkflow> {
        get { lucaLocationPermissionWorkflowProvider }
        set { lucaLocationPermissionWorkflowProvider = newValue }
    }
}

class MiscServicesInitialiser: ServiceInitialiser {

    @InjectAllDynamic private var toggleables: [Toggleable]

    func initialise() throws {
        let reachabilityService = ReachabilityService()
        DependencyContext[\.reachabilityService] = SharedProvider(value: reachabilityService)
        try DependencyContext.register(SharedProvider(value: reachabilityService as Toggleable), keyIdentifier: "ReachabilityServiceToggleable")

        let locationUpdater = LocationUpdater()
        DependencyContext[\.locationUpdater] = SharedProvider(value: locationUpdater)
        try DependencyContext.register(SharedProvider(value: locationUpdater as Toggleable), keyIdentifier: "LocationUpdaterToggleable")

        DependencyContext[\.privateMeetingService] = SharedProvider(value: PrivateMeetingService(preferences: UserDataPreferences(suiteName: "locations")))

        let dailyKeyRepoHandler = DailyKeyRepoHandler(reachabilityService: reachabilityService)
        DependencyContext[\.dailyKeyRepoHandler] = SharedProvider(value: dailyKeyRepoHandler)
        try DependencyContext.register(SharedProvider(value: dailyKeyRepoHandler as Toggleable), keyIdentifier: "DailyKeyRepoHandlerToggleable")

        DependencyContext[\.userService] = SharedProvider(value: UserService())

        DependencyContext[\.traceIdService] = SharedProvider(value: TraceIdService(
            preferences: UserDataPreferences(suiteName: "traceIdService")))

        let autoCheckoutService = AutoCheckoutService()
        autoCheckoutService.register(regionDetector: RegionMonitoringDetector())
//        autoCheckoutService.register(regionDetector: LocationUpdatesRegionDetector(locationUpdater: locationUpdater, allowedAppStates: [.active]))
        autoCheckoutService.register(regionDetector: SingleLocationRequestRegionDetector(allowedAppStates: [.active]))
        DependencyContext[\.autoCheckoutService] = SharedProvider(value: autoCheckoutService)
        try DependencyContext.register(SharedProvider(value: autoCheckoutService as Toggleable), keyIdentifier: "AutoCheckoutServiceToggleable")

        DependencyContext[\.history] = SharedProvider(value: HistoryService())

        let historyListener = HistoryEventListener()
        DependencyContext[\.historyListener] = SharedProvider(value: historyListener)
        try DependencyContext.register(SharedProvider(value: historyListener as Toggleable), keyIdentifier: "HistoryListenerToggleable")

        DependencyContext[\.selfCheckin] = SharedProvider(value: SelfCheckinService())

        let userSecrectsConsistencyChecker = UserSecretsConsistencyChecker()
        DependencyContext[\.userSecrectsConsistencyChecker] = SharedProvider(value: userSecrectsConsistencyChecker)
        try DependencyContext.register(SharedProvider(value: userSecrectsConsistencyChecker as Toggleable), keyIdentifier: "UserSecretsCheckerToggleable")

        DependencyContext[\.accessDataChunkHandler] = SharedProvider(value: AccessDataChunkHandler())

        DependencyContext[\.accessTraceIdChecker] = SharedProvider(value: AccessedTraceIdChecker())

        let accessTraceIdNotifier = AccessedTraceIdNotifier(notificationScheduler: NotificationScheduler.shared)
        DependencyContext[\.accessTraceIdNotifier] = SharedProvider(value: accessTraceIdNotifier)
        try DependencyContext.register(SharedProvider(value: accessTraceIdNotifier as Toggleable), keyIdentifier: "AccessTraceIdNotifierToggleable")

        DependencyContext[\.baerCodeKeyService] = SharedProvider(value: BaerCodeKeyService())
        let notificationService = NotificationService()
        DependencyContext[\.notificationService] = SharedProvider(value: notificationService)
        try DependencyContext.register(SharedProvider(value: notificationService as Toggleable), keyIdentifier: "NotificationServiceToggleable")

        DependencyContext[\.personService] = SharedProvider(value: PersonService())

        let checkinPolling = CheckinStatusPollingService()
        DependencyContext[\.checkinPolling] = SharedProvider(value: checkinPolling)
        try DependencyContext.register(SharedProvider(value: checkinPolling as Toggleable), keyIdentifier: "CheckinPollingToggleable")

        let contactDataReporter = ContentReporterService()
        contactDataReporter.register(contentReporter: StaticStringReporter(text: L10n.DataReport.ContactData.header))
        contactDataReporter.register(contentReporter: UserDataReporter())
        contactDataReporter.register(contentReporter: HistoryReporter())
        contactDataReporter.register(contentReporter: TraceInfoReporter())
        contactDataReporter.register(contentReporter: StaticStringReporter(text: L10n.DataReport.footer))

        let documentsReporter = ContentReporterService()
        documentsReporter.register(contentReporter: StaticStringReporter(text: L10n.DataReport.Document.header))
        documentsReporter.register(contentReporter: DocumentReporter())
        documentsReporter.register(contentReporter: StaticStringReporter(text: L10n.DataReport.footer))

        DependencyContext[\.contactDataReporter] = SharedProvider(value: contactDataReporter)
        DependencyContext[\.documentsReporter] = SharedProvider(value: documentsReporter)

        DependencyContext[\.lucaLocationPermissionWorkflow] = SharedProvider(value: LucaLocationPermissionWorkflow())
    }

    func invalidate() {
        DependencyContext[\.reachabilityService] = EmptyProvider()
        DependencyContext.remove(for: "ReachabilityServiceToggleable")

        DependencyContext[\.locationUpdater] = EmptyProvider()
        DependencyContext.remove(for: "LocationUpdaterToggleable")

        DependencyContext[\.privateMeetingService] = EmptyProvider()

        DependencyContext[\.dailyKeyRepoHandler] = EmptyProvider()
        DependencyContext.remove(for: "DailyKeyRepoHandlerToggleable")

        DependencyContext[\.userService] = EmptyProvider()

        DependencyContext[\.traceIdService] = EmptyProvider()

        DependencyContext[\.autoCheckoutService] = EmptyProvider()
        DependencyContext.remove(for: "AutoCheckoutServiceToggleable")

        DependencyContext[\.history] = EmptyProvider()

        DependencyContext[\.historyListener] = EmptyProvider()
        DependencyContext.remove(for: "HistoryListenerToggleable")

        DependencyContext[\.selfCheckin] = EmptyProvider()

        DependencyContext[\.userSecrectsConsistencyChecker] = EmptyProvider()
        DependencyContext.remove(for: "UserSecretsCheckerToggleable")

        DependencyContext[\.accessDataChunkHandler] = EmptyProvider()

        DependencyContext[\.accessTraceIdChecker] = EmptyProvider()

        DependencyContext[\.accessTraceIdNotifier] = EmptyProvider()
        DependencyContext.remove(for: "AccessTraceIdNotifierToggleable")

        DependencyContext[\.baerCodeKeyService] = EmptyProvider()

        DependencyContext[\.notificationService] = EmptyProvider()
        DependencyContext.remove(for: "NotificationServiceToggleable")

        DependencyContext[\.personService] = EmptyProvider()

        DependencyContext[\.checkinPolling] = EmptyProvider()
        DependencyContext.remove(for: "CheckinPollingToggleable")

        DependencyContext[\.contactDataReporter] = EmptyProvider()
        DependencyContext[\.documentsReporter] = EmptyProvider()

        DependencyContext[\.lucaLocationPermissionWorkflow] = EmptyProvider()
    }
}
