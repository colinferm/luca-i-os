import Foundation
import DependencyInjection
import RxSwift

private var _accessedTraceIdRepo: BaseInjectionProvider<DataRepo<AccessedTraceId>> = EmptyProvider()
private var _documentKeyProviderProvider: BaseInjectionProvider<DocumentKeyProvider> = EmptyProvider()
private var _documentFactoryProvider: BaseInjectionProvider<DocumentFactory> = EmptyProvider()
private var _documentRepoServiceProvider: BaseInjectionProvider<DocumentRepoService> = EmptyProvider()
private var _documentProcessingServiceProvider: BaseInjectionProvider<DocumentProcessingService> = EmptyProvider()
private var _documentUniquenessCheckerProvider: BaseInjectionProvider<DocumentUniquenessChecker> = EmptyProvider()
private var _qrProcessingServiceProvider: BaseInjectionProvider<QRProcessingService> = EmptyProvider()

extension DependencyContext {
    var documentKeyProvider: BaseInjectionProvider<DocumentKeyProvider> {
        get { _documentKeyProviderProvider }
        set { _documentKeyProviderProvider = newValue }
    }
    var documentFactory: BaseInjectionProvider<DocumentFactory> {
        get { _documentFactoryProvider }
        set { _documentFactoryProvider = newValue }
    }
    var documentRepoService: BaseInjectionProvider<DocumentRepoService> {
        get { _documentRepoServiceProvider }
        set { _documentRepoServiceProvider = newValue }
    }
    var documentProcessingService: BaseInjectionProvider<DocumentProcessingService> {
        get { _documentProcessingServiceProvider }
        set { _documentProcessingServiceProvider = newValue }
    }
    var documentUniquenessChecker: BaseInjectionProvider<DocumentUniquenessChecker> {
        get { _documentUniquenessCheckerProvider }
        set { _documentUniquenessCheckerProvider = newValue }
    }
    var qrProcessingService: BaseInjectionProvider<QRProcessingService> {
        get { _qrProcessingServiceProvider }
        set { _qrProcessingServiceProvider = newValue }
    }
}

class DocumentsServicesInitialiser: ServiceInitialiser {

    func initialise() throws {
        DependencyContext[\.documentKeyProvider] = SharedProvider(value: DocumentKeyProvider())

        var documentFactory = DocumentFactory()

        documentFactory.register(parser: DGCParser())
        documentFactory.register(parser: AppointmentParser())
        documentFactory.register(parser: DefaultJWTParser())
        documentFactory.register(parser: JWTParserWithOptionalDoctor())
        documentFactory.register(parser: BaerCodeParser())

        DependencyContext[\.documentFactory] = SharedProvider(value: documentFactory)

        DependencyContext[\.documentRepoService] = SharedProvider(value: DocumentRepoService())
        DependencyContext[\.documentUniquenessChecker] = SharedProvider(value: DocumentUniquenessChecker())
        var documentProcessingService = DocumentProcessingService()

        // Document validators
        documentProcessingService.register(validator: CoronaTestIsNotInFutureValidator())
        documentProcessingService.register(validator: CoronaTestIsNegativeValidator())
        documentProcessingService.register(validator: DGCIssuerValidator())

        #if !DEVELOPMENT
        documentProcessingService.register(validator: UserOrChildValidator(userIdentityValidator: UserOwnershipValidator()))
        #endif

        #if PREPROD || PRODUCTION
        documentProcessingService.register(validator: CoronaTestValidityValidator())
        documentProcessingService.register(validator: RecoveryValidityValidator())
        documentProcessingService.register(validator: AppointmentValidityValidator())
        #endif

        DependencyContext[\.documentProcessingService] = SharedProvider(value: documentProcessingService)

        DependencyContext[\.qrProcessingService] = SharedProvider(value: QRProcessingService())
    }

    func invalidate() {
        DependencyContext[\.documentKeyProvider] = EmptyProvider()
        DependencyContext[\.documentFactory] = EmptyProvider()
        DependencyContext[\.documentRepoService] = EmptyProvider()
        DependencyContext[\.documentUniquenessChecker] = EmptyProvider()
        DependencyContext[\.documentProcessingService] = EmptyProvider()
        DependencyContext[\.qrProcessingService] = EmptyProvider()
    }
}

extension DocumentsServicesInitialiser: UnsafeAddress, LogUtil {}
