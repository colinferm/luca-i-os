import Foundation

protocol ServiceInitialiser {
    /// Used to init components, load initial data, register services in DI
    func initialise() throws

    /// Used to close those components and to unregister services in DI
    func invalidate()
}
