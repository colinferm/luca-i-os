import Foundation
import DependencyInjection

private var randomNumberGeneratorProvider: BaseInjectionProvider<RandomNumberGenerator> = EmptyProvider()
private var commonCryptoProvider: BaseInjectionProvider<CommonCrypto> = EmptyProvider()
private var keyGenProvider: BaseInjectionProvider<KeyGen> = EmptyProvider()

extension DependencyContext {
    var randomNumberGenerator: BaseInjectionProvider<RandomNumberGenerator> {
        get { randomNumberGeneratorProvider }
        set { randomNumberGeneratorProvider = newValue }
    }
    var commonCrypto: BaseInjectionProvider<CommonCrypto> {
        get { commonCryptoProvider }
        set { commonCryptoProvider = newValue }
    }
    var keyGen: BaseInjectionProvider<KeyGen> {
        get { keyGenProvider }
        set { keyGenProvider = newValue }
    }
}

class CryptoInitialiser: ServiceInitialiser {
    func initialise() throws {
        DependencyContext[\.randomNumberGenerator] = SharedProvider(value: StandardRandomNumberGenerator())
        DependencyContext[\.commonCrypto] = SharedProvider(value: LucaCommonCrypto())
        DependencyContext[\.keyGen] = SharedProvider(value: LucaKeyGen())
    }

    func invalidate() {
        DependencyContext[\.randomNumberGenerator] = EmptyProvider()
        DependencyContext[\.commonCrypto] = EmptyProvider()
        DependencyContext[\.keyGen] = EmptyProvider()
    }

}
