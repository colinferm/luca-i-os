import Foundation
import UIKit

@IBDesignable
public class LightStandardButton: RoundedLucaButton {}

@IBDesignable
public class DarkStandardButton: RoundedLucaButton, Themable {
    public var theme: AppearanceTheme = .dark
    public var supportedThemes: [AppearanceTheme] = [.dark, .light]

	internal override func borderWidth() -> CGFloat {return 1}

    public func apply(theme: AppearanceTheme) {
        guard supports(theme: theme) else { return }
        self.theme = theme
        switch theme {
        case .dark:
            titleLabelColor = UIColor.white
            backgroundColor = UIColor.clear
            borderColor = UIColor.white
        case .light:
            titleLabelColor = UIColor.black
            backgroundColor = UIColor.white
            borderColor = UIColor.black
        }
    }
}

public class SelfSizingLabel: UILabel {

    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

	required init?(coder: NSCoder) {
		super.init(coder: coder)
		setup()
	}

    private func setup() {
        self.adjustsFontForContentSizeCategory = true
        if #available(iOS 15.0, *) {
            self.maximumContentSizeCategory = .accessibilityLarge
        }
	}
}

public class Luca14PtBlackLabel: SelfSizingLabel {}
public class Luca14PtBoldBlackLabel: SelfSizingLabel {}
public class Luca14PtBoldLabel: SelfSizingLabel {}
public class Luca16PtLabel: SelfSizingLabel {}
public class Luca16PtBoldLabel: SelfSizingLabel {}
public class Luca16PtBoldBlackLabel: SelfSizingLabel {}
public class Luca20PtBoldLabel: SelfSizingLabel {}
public class TANLabel: SelfSizingLabel {}
public class Luca60PtLabel: SelfSizingLabel {}
public class Luca12PtLabel: SelfSizingLabel {}
public class PrivateMeetingTimerLabel: SelfSizingLabel {}

@IBDesignable
public class WhiteButton: RoundedLucaButton {}

@IBDesignable
public class BlueButton: DesignableButton {}

public class Luca14PtLabel: SelfSizingLabel, Themable {

    public var theme: AppearanceTheme = .dark
    public var supportedThemes: [AppearanceTheme] {
        [.dark, .light]
    }

    public func apply(theme: AppearanceTheme) {
        if !supports(theme: theme) {
            return
        }
        switch theme {
        case .dark:
            textColor = .white
        case .light:
            textColor = .black
        }
        self.theme = theme
    }
}
